﻿using Volo.Abp.BackgroundWorkers.Quartz;
using Volo.Abp.Modularity;
namespace Septa.PerformanceEvaluation.Job
{

    [DependsOn(
    typeof(AbpBackgroundWorkersQuartzModule),
    typeof(PerformanceEvaluationApplicationContractsModule)
    )]
    public class PerformanceEvaluationQuartzModule : AbpModule
    {

    }
}


