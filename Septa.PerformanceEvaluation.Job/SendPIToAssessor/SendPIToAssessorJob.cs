﻿using Quartz;
using Septa.PerformanceEvaluation.Interface;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.BackgroundWorkers.Quartz;

namespace Septa.PerformanceEvaluation.Job.SendPIToAssessor
{
    [DisallowConcurrentExecution]
    public class SendPiToAssessorJob : QuartzBackgroundWorkerBase
    {
        private readonly ISendPIToAssessorAppService _sendPIToAssessorAppService;

        public SendPiToAssessorJob(ISendPIToAssessorAppService sendPIToAssessorAppService)
        {
            JobDetail = JobBuilder.Create<SendPiToAssessorJob>().WithIdentity("SendPiToAssessorJob", "job").Build();
            Trigger = TriggerBuilder.Create().WithIdentity("SendPiToAssessorTrigger", "job")
            .WithSchedule(CronScheduleBuilder.DailyAtHourAndMinute(01, 00))
            .Build();
         // .StartNow()
         // .WithSimpleSchedule(x => x
         //     .WithIntervalInSeconds(10)
         //     .RepeatForever()
         //     .WithMisfireHandlingInstructionIgnoreMisfires())
         // .Build();
            _sendPIToAssessorAppService = sendPIToAssessorAppService;
        }

        public override async Task Execute(IJobExecutionContext context)
        {
            await _sendPIToAssessorAppService.ExecutePA(DateTime.Now);
        }
    }
}
