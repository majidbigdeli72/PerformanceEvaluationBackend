﻿using Septa.PerformanceEvaluation.EntityFrameworkCore;
using Volo.Abp.Modularity;

namespace Septa.PerformanceEvaluation
{
    [DependsOn(
        typeof(PerformanceEvaluationEntityFrameworkCoreTestModule)
        )]
    public class PerformanceEvaluationDomainTestModule : AbpModule
    {

    }
}