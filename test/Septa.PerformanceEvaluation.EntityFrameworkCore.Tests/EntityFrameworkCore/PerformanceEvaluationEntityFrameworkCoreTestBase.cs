﻿using Volo.Abp;

namespace Septa.PerformanceEvaluation.EntityFrameworkCore
{
    public abstract class PerformanceEvaluationEntityFrameworkCoreTestBase : PerformanceEvaluationTestBase<PerformanceEvaluationEntityFrameworkCoreTestModule> 
    {

    }
}
