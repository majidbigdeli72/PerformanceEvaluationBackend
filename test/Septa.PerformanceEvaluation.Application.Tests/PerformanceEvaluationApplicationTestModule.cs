﻿using Volo.Abp.Modularity;

namespace Septa.PerformanceEvaluation
{
    [DependsOn(
        typeof(PerformanceEvaluationApplicationModule),
        typeof(PerformanceEvaluationDomainTestModule)
        )]
    public class PerformanceEvaluationApplicationTestModule : AbpModule
    {

    }
}