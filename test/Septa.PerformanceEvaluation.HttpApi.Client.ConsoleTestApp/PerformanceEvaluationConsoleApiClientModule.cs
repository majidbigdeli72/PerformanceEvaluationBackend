﻿using Volo.Abp.Http.Client.IdentityModel;
using Volo.Abp.Modularity;

namespace Septa.PerformanceEvaluation.HttpApi.Client.ConsoleTestApp
{
    [DependsOn(
        typeof(PerformanceEvaluationHttpApiClientModule),
        typeof(AbpHttpClientIdentityModelModule)
        )]
    public class PerformanceEvaluationConsoleApiClientModule : AbpModule
    {
        
    }
}
