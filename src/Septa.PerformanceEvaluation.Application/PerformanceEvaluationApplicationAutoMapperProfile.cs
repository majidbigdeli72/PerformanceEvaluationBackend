﻿using AutoMapper;
using Septa.PerformanceEvaluation.DataBaseView;
using Septa.PerformanceEvaluation.Dto;
using Septa.PerformanceEvaluation.Entities;
using Septa.PerformanceEvaluation.LinqView;
using Volo.Abp.Identity;

namespace Septa.PerformanceEvaluation
{
    public class PerformanceEvaluationApplicationAutoMapperProfile : Profile
    {
        public PerformanceEvaluationApplicationAutoMapperProfile()
        {
            /* You can configure your AutoMapper mapping configuration here.
             * Alternatively, you can split your mapping configurations
             * into multiple profile classes for a better organization. */

            //Create Questionnaire
            CreateMap<QuestionnaireCreateDto, Questionnaire>().ReverseMap();
            CreateMap<QuestionCreateDto, Question>().ReverseMap();
            CreateMap<QuestionItemCreateDto, QuestionItem>().ReverseMap();


            //Get Questionnaire
            CreateMap<QuestionnaireGetDto, Questionnaire>().ReverseMap();
            CreateMap<QuestionGetDto, Question>().ReverseMap();
            CreateMap<QuestionItemGetDto, QuestionItem>().ReverseMap();

            //Update Questionnaire

            CreateMap<QuestionnaireUpdateDto, Questionnaire>().ReverseMap();
            CreateMap<QuestionUpdateDto, Question>().ReverseMap();
            CreateMap<QuestionItemUpdateDto, QuestionItem>().ReverseMap();

            //PA
            CreateMap<PACreateDto, PA>().ReverseMap();
            CreateMap<PAUpdateDto, PA>().ReverseMap();
            CreateMap<PAGetDto, PA>().ReverseMap();

            //Create Position
            CreateMap<PositionCreateDto, Position>().ReverseMap();
            CreateMap<OrganizationEntryCreateDto, OrganizationEntry>().ReverseMap();

            //Update Position
            CreateMap<PositionGetDto, Position>().ReverseMap();
            CreateMap<OrganizationEntryGetDto, OrganizationEntry>().ReverseMap();

            //Get Position
            CreateMap<PositionUpdateDto, Position>().ReverseMap();
            CreateMap<OrganizationEntryUpdateDto, OrganizationEntry>().ReverseMap();

            //Department
            CreateMap<DepartmentCreateDto, Department>().ReverseMap();
            CreateMap<DepartmentGetDto, Department>().ReverseMap();
            CreateMap<DepartmentUpdateDto, Department>().ReverseMap();

            //OrganizationEntr Department
            CreateMap<OrganizationEntryDepartmentCreateDto, OrganizationEntry>().ReverseMap();
            CreateMap<OrganizationEntryDepartmentGetDto, OrganizationEntry>().ReverseMap();
            CreateMap<OrganizationEntryDepartmentUpdateDto, OrganizationEntry>().ReverseMap();

            //OrganizationEntr Position
            CreateMap<OrganizationEntryPositionCreateDto, OrganizationEntry>().ReverseMap();
            CreateMap<OrganizationEntryPositionGetDto, OrganizationEntry>().ReverseMap();
            CreateMap<OrganizationEntryPositionUpdateDto, OrganizationEntry>().ReverseMap();

            //PAOrganizationEntry
            CreateMap<PAOrganizationEntryCreateDto, PAOrganizationEntry>().ReverseMap();
            CreateMap<PAOrganizationEntryGetDto, PAOrganizationEntry>().ReverseMap();
            CreateMap<PAOrganizationEntryUpdateDto, PAOrganizationEntry>().ReverseMap();

            CreateMap<PositionOrganizationEntryGetDto, Position>().ReverseMap();


            CreateMap<OperationEvaluationCategoryGetDto, OperationEvaluationCategory>().ReverseMap();

            CreateMap<PAOrganizationEntryCategoryCreateDto, PAOrganizationEntryCategory>().ReverseMap();
            CreateMap<PAOrganizationEntryCategoryGetDto, PAOrganizationEntryCategory>().ReverseMap();
            CreateMap<PAOrganizationEntryCategoryUpdateDto, PAOrganizationEntryCategory>().ReverseMap();

            CreateMap<PAOrganizationEntryCategoryPICreateDto, PAOrganizationEntryCategoryPI>().ReverseMap();
            CreateMap<PAOrganizationEntryCategoryPIGetDto, PAOrganizationEntryCategoryPI>().ReverseMap();
            CreateMap<PAOrganizationEntryCategoryPIUpdateDto, PAOrganizationEntryCategoryPI>().ReverseMap();

            CreateMap<PerformanceIndicatorCreateDto, PerformanceIndicator>().ReverseMap();
            CreateMap<PerformanceIndicatorGetDto, PerformanceIndicator>().ReverseMap();
            CreateMap<PerformanceIndicatorUpdateDto, PerformanceIndicator>().ReverseMap();

            CreateMap<PAOrganizationEntryCategoryPIAssessorCreateDto, PAOrganizationEntryCategoryPIAssessor>().ReverseMap();
            CreateMap<PAOrganizationEntryCategoryPIAssessorGetDto, PAOrganizationEntryCategoryPIAssessor>().ReverseMap();
            CreateMap<PAOrganizationEntryCategoryPIAssessorUpdateDto, PAOrganizationEntryCategoryPIAssessor>().ReverseMap();

            CreateMap<EmployeeCreateDto, Employee>().ReverseMap();
            CreateMap<EmployeeGetDto, Employee>().ReverseMap();
            CreateMap<EmployeeUpdateDto, Employee>().ReverseMap();
            CreateMap<OrganizationEntryEmployeeCreateDto, OrganizationEntry>().ReverseMap();
            CreateMap<OrganizationEntryEmployeeGetDto, OrganizationEntry>().ReverseMap();
            CreateMap<OrganizationEntryEmployeeUpdateDto, OrganizationEntry>().ReverseMap();
            CreateMap<PAResultPIGetDto, PAResultPI>().ReverseMap();
            CreateMap<PAResultPIAssessorGetDto, PAResultPIAssessor>().ReverseMap();
            CreateMap<PAResultPIAssessorGetDto, LVwPAResultPIAssesor>().ReverseMap();
            CreateMap<QuestionnaireResultGetDto, QuestionnaireResult>().ReverseMap();
            CreateMap<VwDepartmentDto, VwDepartment>().ReverseMap();
            CreateMap<VwPositionDto, VwPosition>().ReverseMap();
            CreateMap<IdentityUserDto, IdentityUser>().ReverseMap();
            CreateMap<IdentityRoleDto, IdentityRole>().ReverseMap();
            CreateMap<PALogGetDto, LVwPaLog>().ReverseMap();
            CreateMap<QuestionResultGetDto, LVwQuestionResult>().ReverseMap();







        }
    }
}
