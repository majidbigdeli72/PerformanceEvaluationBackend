﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Septa.PerformanceEvaluation.Localization;
using Volo.Abp.Application.Services;
using Volo.Abp.Linq;

namespace Septa.PerformanceEvaluation
{
    /* Inherit your application services from this class.
     */
    public abstract class PerformanceEvaluationAppService : ApplicationService
    {
        public IAsyncQueryableExecuter AsyncQueryableExecuter { get; set; }

        protected PerformanceEvaluationAppService()
        {
            LocalizationResource = typeof(PerformanceEvaluationResource);
            AsyncQueryableExecuter = DefaultAsyncQueryableExecuter.Instance;
        }
    }

}
