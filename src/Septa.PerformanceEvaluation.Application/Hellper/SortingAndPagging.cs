﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Domain.Entities;

namespace Septa.PerformanceEvaluation.Hellper
{
    public static class SortingAndPagging
    {
        public static IQueryable<IEntity<Tkey>> ApplySorting<Tkey>(this IQueryable<IEntity<Tkey>> query, IPagedAndSortedResultRequest input)
        {
            //Try to sort query if available
            if (input is ISortedResultRequest sortInput)
            {
                if (!sortInput.Sorting.IsNullOrWhiteSpace())
                {
                    return query.OrderBy(sortInput.Sorting);
                }
            }

            //IQueryable.Task requires sorting, so we should sort if Take will be used.
            if (input is ILimitedResultRequest)
            {
                return query.OrderByDescending(e => e.Id);
            }

            //No sorting
            return query;
        }

        public static IQueryable<IEntity<Tkey>> ApplyPaging<Tkey>(this IQueryable<IEntity<Tkey>> query, IPagedAndSortedResultRequest input)
        {
            //Try to use paging if available
            if (input is IPagedResultRequest pagedInput)
            {
                try
                {

                    return query.PageBy(pagedInput);
                }
                catch (Exception ex)
                {

                    throw ex;
                }
            }

            //Try to limit query result if available
            if (input is ILimitedResultRequest limitedInput)
            {
                return query.Take(limitedInput.MaxResultCount);
            }

            //No paging
            return query;
        }
    }

    public static class HttpClientHelper
    {
        public static async Task<TResult> PostFormUrlEncoded<TResult>(string url, IEnumerable<KeyValuePair<string, string>> postData)
        {
            using (var httpClient = new HttpClient())
            {
                using (var content = new FormUrlEncodedContent(postData))
                {
                    content.Headers.Clear();
                    content.Headers.Add("Content-Type", "application/x-www-form-urlencoded");

                    try
                    {

                        HttpResponseMessage response = await httpClient.PostAsync(url, content);
                        return await response.Content.ReadAsAsync<TResult>();


                    }
                    catch (Exception ex)
                    {

                        throw ex;
                    }

                }
            }
        }

    }

    public static class HttpContentExtensions
    {
        public static async Task<T> ReadAsAsync<T>(this HttpContent content) =>
            await JsonSerializer.DeserializeAsync<T>(await content.ReadAsStreamAsync());
    }
}
