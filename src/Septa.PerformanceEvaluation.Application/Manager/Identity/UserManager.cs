﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Septa.PerformanceEvaluation.Entities;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Domain.Services;
using Volo.Abp.Identity;

namespace Septa.PerformanceEvaluation.Manager.Identity
{
    public class UserManager : UserManager<Volo.Abp.Identity.IdentityUser>, IDomainService, ITransientDependency
    {
        public UserManager(IUserStore<Volo.Abp.Identity.IdentityUser> store, IOptions<IdentityOptions> optionsAccessor, IPasswordHasher<Volo.Abp.Identity.IdentityUser> passwordHasher, IEnumerable<IUserValidator<Volo.Abp.Identity.IdentityUser>> userValidators, IEnumerable<IPasswordValidator<Volo.Abp.Identity.IdentityUser>> passwordValidators, ILookupNormalizer keyNormalizer, IdentityErrorDescriber errors, IServiceProvider services, ILogger<UserManager<Volo.Abp.Identity.IdentityUser>> logger) : base(store, optionsAccessor, passwordHasher, userValidators, passwordValidators, keyNormalizer, errors, services, logger)
        {
        }
    }
}
