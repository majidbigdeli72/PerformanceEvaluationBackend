﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Septa.PerformanceEvaluation.Entities;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Domain.Services;
using Volo.Abp.Identity;

namespace Septa.PerformanceEvaluation.Manager.Identity
{
    public class SignInManager : SignInManager<Volo.Abp.Identity.IdentityUser>, IDomainService, ITransientDependency
    {
        public SignInManager(UserManager<Volo.Abp.Identity.IdentityUser> userManager, IHttpContextAccessor contextAccessor, IUserClaimsPrincipalFactory<Volo.Abp.Identity.IdentityUser> claimsFactory, IOptions<IdentityOptions> optionsAccessor, ILogger<SignInManager<Volo.Abp.Identity.IdentityUser>> logger, IAuthenticationSchemeProvider schemes, IUserConfirmation<Volo.Abp.Identity.IdentityUser> confirmation) : base(userManager, contextAccessor, claimsFactory, optionsAccessor, logger, schemes, confirmation)
        {
        }
    }

}
