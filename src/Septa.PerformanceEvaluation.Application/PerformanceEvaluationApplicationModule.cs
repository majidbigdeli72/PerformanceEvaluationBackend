﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using Septa.PerformanceEvaluation.Entities;
using System.Net;
using Volo.Abp;
using Volo.Abp.Account;
using Volo.Abp.AutoMapper;
using Volo.Abp.FeatureManagement;
using Volo.Abp.Identity;
using Volo.Abp.Modularity;
using Volo.Abp.PermissionManagement;
using Volo.Abp.TenantManagement;

namespace Septa.PerformanceEvaluation
{
    [DependsOn(
        typeof(PerformanceEvaluationDomainModule),
        typeof(AbpAccountApplicationModule),
        typeof(PerformanceEvaluationApplicationContractsModule),
        typeof(AbpIdentityApplicationModule),
        typeof(AbpPermissionManagementApplicationModule),
        typeof(AbpTenantManagementApplicationModule),
        typeof(AbpFeatureManagementApplicationModule)
        )]
    public class PerformanceEvaluationApplicationModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {

            Configure<AbpAutoMapperOptions>(options =>
            {
                options.AddMaps<PerformanceEvaluationApplicationModule>();
            });
        }
    }
}
