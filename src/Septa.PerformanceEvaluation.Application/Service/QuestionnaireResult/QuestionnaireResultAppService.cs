﻿using Microsoft.AspNetCore.Authorization;
using Septa.PerformanceEvaluation.Dto;
using Septa.PerformanceEvaluation.Enums;
using Septa.PerformanceEvaluation.Interface;
using Septa.PerformanceEvaluation.IRepository;
using Septa.PerformanceEvaluation.LinqView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp;

namespace Septa.PerformanceEvaluation.Service.QuestionnaireResult
{
    [Authorize]
    public class QuestionnaireResultAppService : PerformanceEvaluationAppService, IQuestionnaireResultAppService
    {
        private readonly IQuestionnaireResultRepository _questionnaireResultRepository;
        private readonly IPAResultPIAssessorRepository _pAResultPIAssessorRepository;
        private readonly IPAResultPIRepository _pAResultPIRepository;
        private readonly IQuestionResultRepository _questionResultRepository;
        private readonly IEmployeeRepository _employeeRepository;

        public QuestionnaireResultAppService(
            IQuestionnaireResultRepository questionnaireResults,
            IPAResultPIAssessorRepository pAResultPIAssessorRepository,
            IPAResultPIRepository pAResultPIRepository,
            IQuestionResultRepository questionResultRepository,
            IEmployeeRepository employeeRepository)
        {
            _questionnaireResultRepository = questionnaireResults;
            _pAResultPIAssessorRepository = pAResultPIAssessorRepository;
            _pAResultPIRepository = pAResultPIRepository;
            _questionResultRepository = questionResultRepository;
            _employeeRepository = employeeRepository;
        }

        public async Task<QuestionnaireResultGetDto> CreateQuestionnaireResult(List<QuestionnaireResultCreateDto> questionnaireResult)
        {
            var qr = questionnaireResult.FirstOrDefault();
            var minAndmaxList = await _questionResultRepository.GetMinMaxQuestionItemsAsync(qr.QuestionnaireId);
            decimal totalWeight = questionnaireResult.Sum(x => x.Weight);
            Entities.QuestionnaireResult entityQuestionnaireResult = new Entities.QuestionnaireResult();
            decimal finalValue = 0;
            switch (qr.CalculationTypeIndex)
            {
                case Gp_CalculationType.Additive:
                    finalValue = NormalizeAdditive(questionnaireResult, minAndmaxList, totalWeight);
                    entityQuestionnaireResult = await SaveQuestionnaireResult(qr.QuestionnaireId, finalValue);
                    await SaveQustionResult(questionnaireResult, entityQuestionnaireResult.Id);
                    await UpdatePAResultPIAssessor(finalValue, qr.PAResultAssessorId, entityQuestionnaireResult.Id);
                    return ObjectMapper.Map<Entities.QuestionnaireResult, QuestionnaireResultGetDto>(entityQuestionnaireResult);
                case Gp_CalculationType.Decrease:
                    finalValue = NormalizeDecrease(questionnaireResult, minAndmaxList, totalWeight);
                    entityQuestionnaireResult = await SaveQuestionnaireResult(qr.QuestionId, finalValue);
                    await SaveQustionResult(questionnaireResult, entityQuestionnaireResult.Id);
                    await UpdatePAResultPIAssessor(finalValue, qr.PAResultAssessorId, entityQuestionnaireResult.Id);
                    return ObjectMapper.Map<Entities.QuestionnaireResult, QuestionnaireResultGetDto>(entityQuestionnaireResult);
            }
            return ObjectMapper.Map<Entities.QuestionnaireResult, QuestionnaireResultGetDto>(entityQuestionnaireResult);
        }

        private decimal NormalizeAdditive(List<QuestionnaireResultCreateDto> questionnaireResult, List<LVwMinMaxQuestionItem> minAndmaxList, decimal totalWeight)
        {
            decimal questionItemResult = 0;
            foreach (var item in questionnaireResult)
            {
                var minAndmax = minAndmaxList.FirstOrDefault(x => x.QuestionId == item.QuestionId);
                questionItemResult += (((item.Value - minAndmax.Min) / (minAndmax.Max - minAndmax.Min)) * item.Weight);
            }
            decimal finalValue = (questionItemResult / totalWeight);
            return finalValue;
        }

        private decimal NormalizeDecrease(List<QuestionnaireResultCreateDto> questionnaireResult, List<LVwMinMaxQuestionItem> minAndmaxList, decimal totalWeight)
        {
            decimal questionItemResult = 0;
            foreach (var item in questionnaireResult)
            {
                var minAndmax = minAndmaxList.FirstOrDefault(x => x.QuestionId == item.QuestionId);
                questionItemResult += (((minAndmax.Min - item.Value) / (minAndmax.Min - minAndmax.Max)) * item.Weight);
            }
            decimal finalValue = (questionItemResult / totalWeight);
            return finalValue;
        }

        private async Task<Entities.QuestionnaireResult> SaveQuestionnaireResult(int questionnaireId, decimal finalValue)
        {

            var userName = CurrentUser.UserName;
            var assessorId = (await _employeeRepository.GetEmployeeByUserNameAsync(userName))?.Id;

            if (!assessorId.HasValue)
            {
                throw new UserFriendlyException(L["EmployeeNotFound", userName]);
            }

            var questionnaireResult = await _questionnaireResultRepository.InsertAsync(new Entities.QuestionnaireResult(questionnaireId, DateTime.Now, assessorId.Value, finalValue), true);

            return questionnaireResult;

        }

        private async Task SaveQustionResult(List<QuestionnaireResultCreateDto> questionnaireResultDto, int entityQuestionnaireResultId)
        {
            foreach (var item in questionnaireResultDto)
            {
                await _questionResultRepository.InsertAsync(new Entities.QuestionResult(item.QuestionId, item.SelectedItemId, entityQuestionnaireResultId));
            }
        }

        private async Task UpdatePAResultPIAssessor(decimal finalValue, int pAResultPIAssessorId, int QuestionnaireResultId)
        {
            var pAResultPIAssessor = await _pAResultPIAssessorRepository.FindAsync(pAResultPIAssessorId, true);

            if (pAResultPIAssessor.PAResultPI.IsGroupingEvaluation)
            {
                var pAResultPIs = _pAResultPIRepository.WithDetails().Where(x => x.ParentId == pAResultPIAssessor.PAResultPI.Id || x.Id == pAResultPIAssessor.PAResultPI.Id).ToList();

                foreach (var pAResultPI in pAResultPIs)
                {
                    foreach (var item in pAResultPI.PAResultPIAssessors.Where(x => x.AssessedById == pAResultPIAssessor.AssessedById).ToList())
                    {

                        var paResultPiAssessor = item.UpdateEntity(item, item.PAResultPIId, item.AssessedById, QuestionnaireResultId, Gp_AssessStatus.Evaluated, finalValue, finalValue, item.AssessDate ?? DateTime.Now, DateTime.Now);
                        await _pAResultPIAssessorRepository.UpdateAsync(paResultPiAssessor, true);
                    }
                }
            }
            else
            {
                var paResultPiAssessor = pAResultPIAssessor.UpdateEntity(pAResultPIAssessor, pAResultPIAssessor.PAResultPIId, pAResultPIAssessor.AssessedById, QuestionnaireResultId, Gp_AssessStatus.Evaluated, finalValue, finalValue, pAResultPIAssessor.AssessDate ?? DateTime.Now, DateTime.Now);
                await _pAResultPIAssessorRepository.UpdateAsync(paResultPiAssessor, true);
            }
        }

    }
}
