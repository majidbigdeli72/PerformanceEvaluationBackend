﻿using Microsoft.AspNetCore.Authorization;
using Septa.PerformanceEvaluation.Const;
using Septa.PerformanceEvaluation.Dto;
using Septa.PerformanceEvaluation.Interface;
using Septa.PerformanceEvaluation.IRepository;

namespace Septa.PerformanceEvaluation.Service
{
    [Authorize(Roles = Roles.PaAdmin)]
    public class PerformanceIndicatorAppService : PerformanceEvaluationCrudAppService<Entities.PerformanceIndicator, PerformanceIndicatorGetDto, int, PagedAndSortedResultDto, PerformanceIndicatorCreateDto, PerformanceIndicatorUpdateDto>, IPerformanceIndicatorAppService
    {
        public PerformanceIndicatorAppService(IPerformanceIndicatorRepository repository) : base(repository)
        {
        }
    }
}
