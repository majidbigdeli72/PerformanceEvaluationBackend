﻿using Microsoft.AspNetCore.Authorization;
using Septa.PerformanceEvaluation.Const;
using Septa.PerformanceEvaluation.Dto;
using Septa.PerformanceEvaluation.Interface;
using Septa.PerformanceEvaluation.IRepository;
using Volo.Abp;

namespace Septa.PerformanceEvaluation.Service
{
    [Authorize(Roles = Roles.PaAdmin)]
    public class PAAppService : PerformanceEvaluationCrudAppService<Entities.PA, PAGetDto, int, PagedAndSortedResultDto, PACreateDto, PAUpdateDto>, IPAAppService
    {
        public PAAppService(IPARepository repository) : base(repository)
        {
        }
    }
}
