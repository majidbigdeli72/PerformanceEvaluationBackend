﻿using Microsoft.AspNetCore.Authorization;
using Septa.PerformanceEvaluation.Const;
using Septa.PerformanceEvaluation.DataBaseView;
using Septa.PerformanceEvaluation.Dto;
using Septa.PerformanceEvaluation.Entities;
using Septa.PerformanceEvaluation.Interface;
using Septa.PerformanceEvaluation.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Septa.PerformanceEvaluation.Service
{
    [Authorize(Roles = Roles.PaAdmin)]
    public class PositionAppService : PerformanceEvaluationCrudAppService<Entities.OrganizationEntry, OrganizationEntryPositionGetDto, Guid, PagedAndSortedResultDto, OrganizationEntryPositionCreateDto, OrganizationEntryPositionUpdateDto>, IPositionAppService
    {
        private readonly IPositionRepository _positionRepository;

        public PositionAppService(IPositionRepository positionRepository) : base(positionRepository)
        {
            _positionRepository = positionRepository;
        }

        public async Task<List<VwPositionDto>> GetPositionPathAsync()
        {
            var vwPositionDto = await _positionRepository.GetPositionAsync();
            return ObjectMapper.Map<List<VwPosition>, List<VwPositionDto>>(vwPositionDto);
        }

        protected override IQueryable<OrganizationEntry> CreateFilteredQuery(PagedAndSortedResultDto input)
        {
            return base.Repository.WithDetails().Where(x => x.Position != null);
        }
    }
}
