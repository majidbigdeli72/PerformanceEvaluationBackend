﻿using Microsoft.AspNetCore.Authorization;
using Septa.PerformanceEvaluation.Const;
using Septa.PerformanceEvaluation.Dto;
using Septa.PerformanceEvaluation.Interface;
using Septa.PerformanceEvaluation.IRepository;
using Volo.Abp;

namespace Septa.PerformanceEvaluation.Service
{
    [Authorize(Roles = Roles.PaAdmin)]
    public class PAOrganizationEntryAppService : PerformanceEvaluationCrudAppService<Entities.PAOrganizationEntry, PAOrganizationEntryGetDto, int, PagedAndSortedResultDto, PAOrganizationEntryCreateDto, PAOrganizationEntryUpdateDto>, IPAOrganizationEntryAppService
    {
        public PAOrganizationEntryAppService(IPAOrganizationEntryRepository repository) : base(repository)
        {
        }
    }
}
