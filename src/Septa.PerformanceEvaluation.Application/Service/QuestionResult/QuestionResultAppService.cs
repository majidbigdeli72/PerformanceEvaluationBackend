﻿using Microsoft.AspNetCore.Authorization;
using Septa.PerformanceEvaluation.Const;
using Septa.PerformanceEvaluation.Dto;
using Septa.PerformanceEvaluation.Interface;
using Septa.PerformanceEvaluation.IRepository;
using Septa.PerformanceEvaluation.LinqView;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Septa.PerformanceEvaluation.Service.QuestionResult
{
    [Authorize]
    public class QuestionResultAppService : PerformanceEvaluationAppService, IQuestionResultAppService
    {
        private readonly IQuestionResultRepository _questionResultsRepository;

        public QuestionResultAppService(IQuestionResultRepository questionResultsRepository)
        {
            _questionResultsRepository = questionResultsRepository;
        }
        public async Task<List<QuestionResultGetDto>> GetQuestionResultAsync(int questionnaireResultId)
        {
            var qs = await _questionResultsRepository.GetQuestionResultAsync(questionnaireResultId);
            return ObjectMapper.Map<List<LVwQuestionResult>, List<QuestionResultGetDto>>(qs);
        }
    }
}
