﻿using Microsoft.AspNetCore.Authorization;
using Septa.PerformanceEvaluation.Const;
using Septa.PerformanceEvaluation.Dto;
using Septa.PerformanceEvaluation.Interface;
using Septa.PerformanceEvaluation.IRepository;
using System.Linq;
using System.Threading.Tasks;
using Volo.Abp;
using Volo.Abp.Application.Dtos;

namespace Septa.PerformanceEvaluation.Service
{
    public class QuestionnaireAppService : PerformanceEvaluationCrudAppService<Entities.Questionnaire, QuestionnaireGetDto, int, PagedAndSortedResultDto, QuestionnaireCreateDto, QuestionnaireUpdateDto>, IQuestionnaireAppService
    {
        private readonly IQuestionnaireRepository _questionnairesRepo;

        public QuestionnaireAppService(IQuestionnaireRepository questionnairesRepo) : base(questionnairesRepo)
        {
            _questionnairesRepo = questionnairesRepo;
        }

        [Authorize(Roles = Roles.PaAdmin)]
        public override Task<QuestionnaireGetDto> CreateAsync(QuestionnaireCreateDto input)
        {
            return base.CreateAsync(input);
        }

        [Authorize(Roles = Roles.PaAdmin)]
        public override Task DeleteAsync(int id)
        {
            return base.DeleteAsync(id);
        }

        [Authorize]
        public override Task<QuestionnaireGetDto> GetAsync(int id)
        {
            return base.GetAsync(id);
        }

        [Authorize(Roles = Roles.PaAdmin)]
        public override Task<PagedResultDto<QuestionnaireGetDto>> GetListAsync(PagedAndSortedResultDto input)
        {
            return base.GetListAsync(input);
        }

        [Authorize(Roles = Roles.PaAdmin)]
        public override async Task<QuestionnaireGetDto> UpdateAsync(int id, QuestionnaireUpdateDto questionnaireUpdateDto)
        {
            // return base.UpdateAsync(id, input);

            var questionnaire = await _questionnairesRepo.FindQuestionnaireAsync(id);

            if (questionnaire == null)
            {
                throw new UserFriendlyException(L["NotFound"], "404");
            }
            try
            {

                var model = new Entities.Questionnaire(id)
                {
                    Description = questionnaireUpdateDto.Description,
                    Name = questionnaireUpdateDto.Name,
                    Questions = questionnaireUpdateDto.Questions.Select(z => new Entities.Question(z.Id)
                    {
                        Content = z.Content,
                        Weight = z.Weight,
                        QuestionnaireId = z.QuestionnaireId,
                        QuestionItems = z.QuestionItems.Select(s => new Entities.QuestionItem(s.Id)
                        {
                            Name = s.Name,
                            QuestionId = s.QuestionId,
                            Value = s.Value
                        }).ToList()

                    }).ToList()
                };
                //update Questionnaire;
                questionnaire.Name = model.Name;
                questionnaire.Description = model.Description;

                //Delete Question if not exist in model;
                foreach (var question in questionnaire.Questions.ToList())
                {
                    if (!model.Questions.Any(c => c.Id == question.Id))
                    {
                        questionnaire.Questions.Remove(question);
                    }
                }

                // Update and Insert Question
                foreach (var questionModel in model.Questions.ToList())
                {

                    if (questionModel.Id == default(int))
                    {
                        questionnaire.Questions.Add(questionModel);

                    }
                    else
                    {
                        var checkQuestion = questionnaire.Questions.FirstOrDefault(x => x.Id == questionModel.Id);

                        if (checkQuestion == null)
                        {
                            //Insert Question
                            questionnaire.Questions.Add(questionModel);
                        }
                        else
                        {
                            //Update Question
                            checkQuestion.Content = questionModel.Content;
                            checkQuestion.Weight = questionModel.Weight;

                            //Delete QuestionItem if not exist in model
                            foreach (var item in checkQuestion.QuestionItems.ToList())
                            {
                                if (!questionModel.QuestionItems.Any(x => x.Id == item.Id))
                                {
                                    checkQuestion.QuestionItems.Remove(item);
                                }
                            }

                            //update or insert questionItem
                            foreach (var questionItemsModel in questionModel.QuestionItems.ToList())
                            {

                                if (questionItemsModel.Id == default(int))
                                {
                                    checkQuestion.QuestionItems.Add(questionItemsModel);
                                }
                                else
                                {

                                    var checkQustionItem = checkQuestion.QuestionItems.FirstOrDefault(x => x.Id == questionItemsModel.Id);
                                    //insert questionItem
                                    if (checkQustionItem == null)
                                    {
                                        checkQuestion.QuestionItems.Add(questionItemsModel);
                                    }
                                    else
                                    {
                                        //update question Item 
                                        checkQustionItem.Name = questionItemsModel.Name;
                                        checkQustionItem.Value = questionItemsModel.Value;
                                    }
                                }

                            }
                        }
                    }



                }

                await _questionnairesRepo.UpdateQuestionnaireAsync(questionnaire);

                return ObjectMapper.Map<Entities.Questionnaire, QuestionnaireGetDto>(questionnaire);


            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(ex.Message, "500");
            }

        }


        protected override void MapToEntity(QuestionnaireUpdateDto input, Entities.Questionnaire questionnaire)
        {
            var model = ObjectMapper.Map<QuestionnaireUpdateDto, Entities.Questionnaire>(input);
            questionnaire.UpdateEntity(model);
        }
    }
}
