﻿using Microsoft.AspNetCore.Authorization;
using Septa.PerformanceEvaluation.Const;
using Septa.PerformanceEvaluation.Dto;
using Septa.PerformanceEvaluation.Interface;
using Septa.PerformanceEvaluation.IRepository;
using System.Linq;
using Volo.Abp;

namespace Septa.PerformanceEvaluation.Service
{
    [Authorize(Roles = Roles.PaAdmin)]
    public class PAOrganizationEntryCategoryAppService : PerformanceEvaluationCrudAppService<Entities.PAOrganizationEntryCategory, PAOrganizationEntryCategoryGetDto, int, PagedAndSortedResultDto, PAOrganizationEntryCategoryCreateDto, PAOrganizationEntryCategoryUpdateDto>, IPAOrganizationEntryCategoryAppService
    {
        public PAOrganizationEntryCategoryAppService(IPAOrganizationEntryCategoryRepository repository) : base(repository)
        {
        }
        protected override IQueryable<Entities.PAOrganizationEntryCategory> CreateFilteredQuery(PagedAndSortedResultDto input)
        {
            return base.Repository.WithDetails();
        }

    }
}
