﻿using Microsoft.AspNetCore.Authorization;
using Septa.PerformanceEvaluation.Const;
using Septa.PerformanceEvaluation.Dto;
using Septa.PerformanceEvaluation.Interface;
using Septa.PerformanceEvaluation.IRepository;
using Septa.PerformanceEvaluation.LinqView;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;

namespace Septa.PerformanceEvaluation.Service
{
    [Authorize(Roles = Roles.PaAdmin)]
    public class PALogAppService : PerformanceEvaluationAppService, IPALogAppService
    {
        private readonly IPALogRepository _paLogRepository;

        public PALogAppService(IPALogRepository paLogRepository)
        {
            _paLogRepository = paLogRepository;
        }
        public async Task<PagedResultDto<PALogGetDto>> GetPALogs()
        {
           var pALogs = await _paLogRepository.GetPALogsAsync();
            var result = ObjectMapper.Map<List<LVwPaLog>, List<PALogGetDto>>(pALogs);
            return new PagedResultDto<PALogGetDto>()
            {
                Items = result,
                TotalCount = result.Count
            };
        }
    }
}
