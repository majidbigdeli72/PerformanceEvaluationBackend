﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Septa.PerformanceEvaluation.Const;
using Septa.PerformanceEvaluation.Dto.Account;
using Septa.PerformanceEvaluation.Hellper;
using Septa.PerformanceEvaluation.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Identity;
using Volo.Abp.IdentityServer.Grants;

namespace Septa.PerformanceEvaluation.Service
{

    [Authorize]
    public class UserAppService : IdentityUserAppService, IUserAppService
    {
        private readonly IdentityUserManager _userManager;
        private readonly IIdentityUserRepository _userRepository;
        private readonly IConfiguration _configuration;
        private readonly IPersistentGrantRepository _persistentGrantRepository;

        public UserAppService(IdentityUserManager userManager, IIdentityUserRepository userRepository, IConfiguration configuration, IPersistentGrantRepository persistentGrantRepository) : base(userManager, userRepository)
        {
            _userManager = userManager;
            _userRepository = userRepository;
            _configuration = configuration;
            _persistentGrantRepository = persistentGrantRepository;
        }

        public override async Task<IdentityUserDto> CreateAsync(IdentityUserCreateDto input)
        {
            var user = new Volo.Abp.Identity.IdentityUser(GuidGenerator.Create(), input.UserName, input.Email, CurrentTenant.Id);

            (await _userManager.CreateAsync(user, input.Password)).CheckErrors();

            await UpdateUserByInput(user, input);

            await CurrentUnitOfWork.SaveChangesAsync();

            return ObjectMapper.Map<Volo.Abp.Identity.IdentityUser, IdentityUserDto>(user);
        }

        public override async Task DeleteAsync(Guid id)
        {
            if (CurrentUser.Id == id)
            {
                throw new BusinessException(code: IdentityErrorCodes.UserSelfDeletion);
            }

            var user = await _userManager.FindByIdAsync(id.ToString());
            if (user == null)
            {
                return;
            }

            (await _userManager.DeleteAsync(user)).CheckErrors();
        }

        public override async Task<IdentityUserDto> FindByUsernameAsync(string username)
        {
            return ObjectMapper.Map<Volo.Abp.Identity.IdentityUser, IdentityUserDto>(
                await _userManager.FindByNameAsync(username)
            );
        }

        public override async Task<IdentityUserDto> GetAsync(Guid id)
        {
            return ObjectMapper.Map<Volo.Abp.Identity.IdentityUser, IdentityUserDto>(
                await _userManager.GetByIdAsync(id)
            );
        }

        public override async Task<PagedResultDto<IdentityUserDto>> GetListAsync(GetIdentityUsersInput input)
        {
            var count = await _userRepository.GetCountAsync(input.Filter);
            var list = await _userRepository.GetListAsync(input.Sorting, input.MaxResultCount, input.SkipCount, input.Filter);

            return new PagedResultDto<IdentityUserDto>(
                count,
                ObjectMapper.Map<List<Volo.Abp.Identity.IdentityUser>, List<IdentityUserDto>>(list)
            );
        }

        public override async Task<ListResultDto<IdentityRoleDto>> GetRolesAsync(Guid id)
        {
            var roles = await _userRepository.GetRolesAsync(id);
            return new ListResultDto<IdentityRoleDto>(
                ObjectMapper.Map<List<Volo.Abp.Identity.IdentityRole>, List<IdentityRoleDto>>(roles)
            );
        }

        public override async Task<IdentityUserDto> UpdateAsync(Guid id, IdentityUserUpdateDto input)
        {
            var user = await _userManager.GetByIdAsync(id);
            user.ConcurrencyStamp = input.ConcurrencyStamp;

            (await _userManager.SetUserNameAsync(user, input.UserName)).CheckErrors();
            await UpdateUserByInput(user, input);
            (await _userManager.UpdateAsync(user)).CheckErrors();

            if (!input.Password.IsNullOrEmpty())
            {
                (await _userManager.RemovePasswordAsync(user)).CheckErrors();
                (await _userManager.AddPasswordAsync(user, input.Password)).CheckErrors();
            }

            await CurrentUnitOfWork.SaveChangesAsync();

            return ObjectMapper.Map<Volo.Abp.Identity.IdentityUser, IdentityUserDto>(user);
        }

        public override async Task UpdateRolesAsync(Guid id, IdentityUserUpdateRolesDto input)
        {
            var user = await _userManager.GetByIdAsync(id);
            (await _userManager.SetRolesAsync(user, input.RoleNames)).CheckErrors();
            await _userRepository.UpdateAsync(user);
        }

        public async Task ChangeUserPasswordAsync(string userName, string newPassword)
        {
            var currentUser = await _userManager.FindByNameAsync(userName);
            (await _userManager.RemovePasswordAsync(currentUser)).CheckErrors();
            (await _userManager.AddPasswordAsync(currentUser, newPassword)).CheckErrors();
        }

        public async Task<bool> HasRoleAsync(string roleName)
        {
           // return CurrentUser.Roles.Contains(roleName.ToLower());
               var user = await _userManager.FindByNameAsync(CurrentUser.UserName);
               return await _userManager.IsInRoleAsync(user, roleName.ToLower());
        }

        public async Task<List<string>> GetRolesForUserAsync(string username)
        {
            if (CurrentUser.Roles.Contains(Roles.HRAdmin) || CurrentUser.Roles.Contains(Roles.HROperator))
            {
                var user = await _userManager.FindByNameAsync(username);
                return (await _userManager.GetRolesAsync(user)).ToList();
            }
            throw new UserFriendlyException("User Does Not Access See GetRolesForUser");
        }

        public async Task<bool> Delete(string username)
        {
            if (CurrentUser.Roles.Contains(Roles.HRAdmin) || CurrentUser.Roles.Contains(Roles.HROperator))
            {
                var user = await _userManager.FindByNameAsync(username);
                return (await _userManager.DeleteAsync(user)).Succeeded;
            }
            throw new UserFriendlyException("User Does Not Access See GetRolesForUser");
        }

        public async Task<IdentityUserDto> GetUser(string username)
        {
            var user = await _userManager.FindByNameAsync(username);
            return ObjectMapper.Map<Volo.Abp.Identity.IdentityUser, IdentityUserDto>(user);
        }

        public async Task<bool> ExpireToken()
        {
            var subId = CurrentUser.Id;
            var expireToken = (await _persistentGrantRepository.GetListBySubjectIdAsync(subId.ToString())).Where(x => x.Expiration.Value > DateTime.Now).OrderByDescending(x => x.CreationTime).FirstOrDefault(x => x.Expiration.Value > DateTime.Now).Key;

            IEnumerable<KeyValuePair<string, string>> keyValuePairs = new Dictionary<string, string> {
                            {"grant_type","refresh_token" },{"client_id","PerformanceEvaluation_App"},{"client_secret","1q2w3e*"},{"scope","PerformanceEvaluation"},{"refresh_token",expireToken}
                             };
            var baseUrl = _configuration["AuthServer:Authority"];
            baseUrl = baseUrl + "/connect/token";
            var url = new Uri(baseUrl);
            AccesToken accesToken = await HttpClientHelper.PostFormUrlEncoded<AccesToken>(url.ToString(), keyValuePairs);

            if (!string.IsNullOrWhiteSpace(accesToken.access_token))
            {
                return true;
            }

            return false;

        }

        protected override async Task UpdateUserByInput(Volo.Abp.Identity.IdentityUser user, IdentityUserCreateOrUpdateDtoBase input)
        {
            if (!string.Equals(user.Email, input.Email, StringComparison.InvariantCultureIgnoreCase))
            {
                (await _userManager.SetEmailAsync(user, input.Email)).CheckErrors();
            }

            if (!string.Equals(user.PhoneNumber, input.PhoneNumber, StringComparison.InvariantCultureIgnoreCase))
            {
                (await _userManager.SetPhoneNumberAsync(user, input.PhoneNumber)).CheckErrors();
            }

            (await _userManager.SetTwoFactorEnabledAsync(user, input.TwoFactorEnabled)).CheckErrors();
            (await _userManager.SetLockoutEnabledAsync(user, input.LockoutEnabled)).CheckErrors();

            user.Name = input.Name;
            user.Surname = input.Surname;

            if (input.RoleNames != null)
            {
                (await _userManager.SetRolesAsync(user, input.RoleNames)).CheckErrors();
            }
        }

    }
}
