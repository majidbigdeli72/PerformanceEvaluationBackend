﻿using Septa.PerformanceEvaluation.Dto;
using Septa.PerformanceEvaluation.Entities;
using Septa.PerformanceEvaluation.Enums;
using Septa.PerformanceEvaluation.Interface;
using Septa.PerformanceEvaluation.IRepository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Septa.PerformanceEvaluation.Service.LogCollector
{

    public class LogCollector : ILogCollector
    {
        private readonly List<LogCollectorDto> logCollectors;
        private readonly IPALogRepository _pALogsRepository;

        public LogCollector(IPALogRepository pALogsRepository)
        {
             logCollectors = new List<LogCollectorDto>();
            _pALogsRepository = pALogsRepository;
        }


        public void AddToPALog(int pAId, int? pAOrganizationEntryId, int? pAOrganizationEntryCategoryId, int? pAOrganizationEntryCategoryPIId, int? pAResultPIId, Gp_PAErrorType pAErrorTypeIndex, string exception, DateTime relatedDate)
        {

            logCollectors.Add(new LogCollectorDto()
            {
                Exception = exception,
                PAErrorTypeIndex = pAErrorTypeIndex,
                PAId = pAId,
                PAOrganizationEntryCategoryId = pAOrganizationEntryCategoryId,
                PAOrganizationEntryCategoryPIId = pAOrganizationEntryCategoryPIId,
                PAOrganizationEntryId = pAOrganizationEntryId,
                PAResultPIId = pAResultPIId,
                RelatedDate = relatedDate
            });
        }

        public async Task PersistPALogAsync()
        {

            foreach (var item in logCollectors)
            {
                await _pALogsRepository.InsertAsync(new PALog(item.PAId, item.PAOrganizationEntryId, item.PAOrganizationEntryCategoryId, item.PAOrganizationEntryCategoryPIId, item.PAResultPIId, item.PAErrorTypeIndex, item.Exception, item.RelatedDate), true); ;
            }

        }

        public void ClearPALog()
        {
            logCollectors.Clear();
        }
    }

}
