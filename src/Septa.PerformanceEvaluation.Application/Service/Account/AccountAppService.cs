﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Septa.PerformanceEvaluation.Dto;
using Septa.PerformanceEvaluation.Dto.Account;
using Septa.PerformanceEvaluation.Entities;
using Septa.PerformanceEvaluation.Enums;
using Septa.PerformanceEvaluation.Hellper;
using Septa.PerformanceEvaluation.Interface.Account;
using Septa.PerformanceEvaluation.Manager.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp;
using Volo.Abp.Account.Settings;
using Volo.Abp.Data;
using Volo.Abp.Domain.Repositories;
using Volo.Abp.Identity;
using Volo.Abp.Settings;
using Volo.Abp.Users;

namespace Septa.PerformanceEvaluation.Service.Account
{
    public class AccountAppService : PerformanceEvaluationAppService, IAccountAppService
    {
        private readonly SignInManager _signInManager;
        private readonly IRepository<AppUser, Guid> _appUsers;
        private readonly UserManager _userManager;
        private readonly ISettingProvider _settingProvider;
        private readonly IConfiguration _configuration;

        public AccountAppService(IRepository<AppUser, Guid> appUsers, UserManager userManager, SignInManager signInManager, ISettingProvider settingProvider, IConfiguration configuration)
        {
            _appUsers = appUsers;
            _userManager = userManager;
            _signInManager = signInManager;
            _settingProvider = settingProvider;
            _configuration = configuration;
        }
        public async Task<LoginResult> Login(UserLoginDto login)
        {
            await CheckLocalLoginAsync();

            ValidateLoginInfo(login);

            var identityUser = await _userManager.FindByNameAsync(login.UserName);

            if (identityUser == null)
            {
                return new LoginResult(Gp_LoginResultType.InvalidUserNameOrPassword);
            }
          
            var user = await _appUsers.FindAsync(identityUser.Id);


            var result = GetLoginResult(await _signInManager.CheckPasswordSignInAsync(identityUser, login.Password, true));

            if (result.Result == Gp_LoginResultType.Success)
            {
                if (user.ExpirePasswordDate.HasValue)
                {
                    if (user.ExpirePasswordDate.Value.Date <= DateTime.Today)
                    {
                        return new LoginResult(Gp_LoginResultType.PasswordExpierd);
                    }
                    else
                    {
                        return result;
                    }

                }
                else
                {
                    return new LoginResult(Gp_LoginResultType.PasswordExpierd);
                }
            }
            else
            {
                return result;
            }



        }


        public async Task<AccesToken> Connect(UserLoginDto login)
        {

            IEnumerable<KeyValuePair<string, string>> keyValuePairs = new Dictionary<string, string> {
                            {"grant_type","password" },{"client_id","PerformanceEvaluation_App"},{"client_secret","1q2w3e*"},{"username",login.UserName},{"password",login.Password}
                             };
            var baseUrl = _configuration["AuthServer:Authority"];
            baseUrl = baseUrl + "/connect/token";
            var url = new Uri(baseUrl);
            AccesToken accesToken = await HttpClientHelper.PostFormUrlEncoded<AccesToken>(url.ToString(), keyValuePairs);

            return accesToken;

            

        }


        private void ValidateLoginInfo(UserLoginDto login)
        {
            if (login == null)
            {
                throw new ArgumentException(nameof(login));
            }

            if (login.UserName.IsNullOrEmpty())
            {
                throw new ArgumentNullException(nameof(login.UserName));
            }

            if (login.Password.IsNullOrEmpty())
            {
                throw new ArgumentNullException(nameof(login.Password));
            }
        }
        private async Task CheckLocalLoginAsync()
        {
            if (!await _settingProvider.IsTrueAsync(AccountSettingNames.EnableLocalLogin))
            {
                throw new UserFriendlyException(L["LocalLoginDisabledMessage"]);
            }
        }

        private static LoginResult GetLoginResult(SignInResult result)
        {
            if (result.IsLockedOut)
            {
                return new LoginResult(Gp_LoginResultType.LockedOut);
            }

            if (result.RequiresTwoFactor)
            {
                return new LoginResult(Gp_LoginResultType.RequiresTwoFactor);
            }

            if (result.IsNotAllowed)
            {
                return new LoginResult(Gp_LoginResultType.NotAllowed);
            }

            if (!result.Succeeded)
            {
                return new LoginResult(Gp_LoginResultType.InvalidUserNameOrPassword);
            }

            return new LoginResult(Gp_LoginResultType.Success);
        }

        public virtual async Task ChangePasswordAsync(ChangePasswordDto input)
        {

            try
            {
                var identityUser = await _userManager.FindByNameAsync(input.UserName);

                if (identityUser == null)
                {
                    throw new Exception("نام کاربری اشتباه است");
                }

                (await _userManager.ChangePasswordAsync(identityUser, input.CurrentPassword, input.NewPassword)).CheckErrors();

                var user = _appUsers.FirstOrDefault(x => x.UserName == input.UserName);

                user.ExpirePasswordDate = DateTime.Today.AddMonths(3);
                user.LockoutEnd = null;

                await _appUsers.UpdateAsync(user);

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }


        }

    }
}
