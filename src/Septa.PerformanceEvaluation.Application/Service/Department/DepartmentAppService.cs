﻿using Microsoft.AspNetCore.Authorization;
using Septa.PerformanceEvaluation.Const;
using Septa.PerformanceEvaluation.DataBaseView;
using Septa.PerformanceEvaluation.Dto;
using Septa.PerformanceEvaluation.Entities;
using Septa.PerformanceEvaluation.Interface;
using Septa.PerformanceEvaluation.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Septa.PerformanceEvaluation.Service
{
    [Authorize(Roles = Roles.PaAdmin)]
    public class DepartmentAppService : PerformanceEvaluationCrudAppService<Entities.OrganizationEntry, OrganizationEntryDepartmentGetDto, Guid, PagedAndSortedResultDto, OrganizationEntryDepartmentCreateDto, OrganizationEntryDepartmentUpdateDto>, IDepartmentAppService
    {
        private readonly IDepartmentRepository _departmentRepository;

        public DepartmentAppService(IDepartmentRepository departmentRepository) : base(departmentRepository)
        {
            _departmentRepository = departmentRepository;
        }

        public async Task<List<VwDepartmentDto>> GetDepartmentPathAsync()
        {
            var vwDepartment = await _departmentRepository.GetDepartmentPathAsync();
            return ObjectMapper.Map<List<VwDepartment>, List<VwDepartmentDto>>(vwDepartment);
        }

        protected override IQueryable<OrganizationEntry> CreateFilteredQuery(PagedAndSortedResultDto input)
        {
            return base.Repository.WithDetails().Where(x => x.Department != null);
        }

    }
}