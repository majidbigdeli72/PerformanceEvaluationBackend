﻿using Septa.PerformanceEvaluation.Interface;
using SeptaNSF.Helper.Persian.PersianDateTime;
using System;
using System.Threading.Tasks;

namespace Septa.PerformanceEvaluation.Service
{
    public class PersianCalendarManager : IPersianCalendarManager
    {
        public DateTime GetWeekLastDay(DateTime dateTime)
        {
            PersianDate pd = new PersianDate(dateTime);
            int dayOfWeek = (int)pd.DayOfWeek;
            int lastWeekOfDay = (int)PersianDayOfWeek.Friday;
            int diff = lastWeekOfDay - dayOfWeek;
            var calendar = new PersianCalendar();
            return calendar.AddDays(pd, diff).Date;

        }
        public DateTime GetMonthFirstDay(DateTime dateTime)
        {
            PersianDate pd = new PersianDate(dateTime);
            return ((DateTime)new PersianDate(pd.Year, pd.Month, 1)).Date;
        }

        public DateTime GetMonthLastDay(DateTime dateTime)
        {
            var firstDay = GetMonthFirstDay(dateTime);
            var calendar = new PersianCalendar();
            return calendar.AddMonths(firstDay, 1).AddDays(-1).Date;
        }

        public DateTime GetQuarterLastDay(DateTime dateTime)
        {
            PersianDate pd = new PersianDate(dateTime);
            int month = pd.Month / 3;
            if (pd.Month % 3 != 0)
            {
                month++; 
            }
            var firstDayofQuarter = new PersianDate(pd.Year, (3 * month), 1);
            var calendar = new PersianCalendar();
            return calendar.AddMonths(firstDayofQuarter, 1).AddDays(-1).Date;
        }


        public DateTime GetHalflyLastDay(DateTime dateTime)
        {
            PersianDate pd = new PersianDate(dateTime);
            var firstDayOfHalfly = new PersianDate(pd.Year, 1, 1);
            var calendar = new PersianCalendar();

            if (pd.Month < 7)
            {
                return calendar.AddMonths(firstDayOfHalfly, 6).AddDays(-1).Date;
            }
            else
            {
                return calendar.AddMonths(firstDayOfHalfly, 12).AddDays(-1).Date;
            }
        }

        public DateTime GetYearLastDay(DateTime dateTime)
        {
            PersianDate pd = new PersianDate(dateTime);
            var firstDayOfYear = new PersianDate(pd.Year, 1, 1);
            var calendar = new PersianCalendar();
            return calendar.AddYears(firstDayOfYear, 1).AddDays(-1).Date;
        }
    }
}
