﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using Septa.PerformanceEvaluation.Interface;
using System.Threading.Tasks;

namespace Septa.PerformanceEvaluation.Service.Token
{
    public class TokenAppService : PerformanceEvaluationAppService, ITokenAppService
    {
        public string ValidateToken()
        {
            return CurrentUser.UserName;
        }
    }
}
