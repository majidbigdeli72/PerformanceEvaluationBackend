﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Septa.PerformanceEvaluation.Entities;
using Septa.PerformanceEvaluation.Enums;
using Septa.PerformanceEvaluation.Interface;
using Septa.PerformanceEvaluation.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Volo.Abp;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Uow;

namespace Septa.PerformanceEvaluation.Service
{
    [RemoteService(false)]
    public class SendPIToAssessorAppService : PerformanceEvaluationAppService, ISendPIToAssessorAppService
    {
        private readonly IPARepository _pARepository;
        private readonly IPositionRepository _positionRepository;
        private readonly IPAResultPIRepository _pAResultPIRepository;
        private readonly IPersianCalendarManager _persianCalendarManager;
        private readonly IPAResultPIAssessorRepository _pAResultPIAssessors;
        private readonly IEmployeeContractRepository _employeeContractsRepository;
        private readonly IEmployeeContractAuthorizeRepository _employeeContractAuthorizesRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly ILogCollector _logCollector;

        public SendPIToAssessorAppService(
            IPARepository pARepository,
            IPositionRepository positionRepository,
            IPAResultPIRepository pAResultPIRepository,
            IPersianCalendarManager persianCalendarManager,
            IPAResultPIAssessorRepository pAResultPIAssessors,
            IEmployeeContractRepository employeeContractsRepository,
            IEmployeeContractAuthorizeRepository employeeContractAuthorizesRepository,
            IUnitOfWorkManager unitOfWorkManager,
            ILogCollector logCollector
            )
        {
            _pARepository = pARepository;
            _positionRepository = positionRepository;
            _pAResultPIRepository = pAResultPIRepository;
            _persianCalendarManager = persianCalendarManager;
            _pAResultPIAssessors = pAResultPIAssessors;
            _employeeContractsRepository = employeeContractsRepository;
            _employeeContractAuthorizesRepository = employeeContractAuthorizesRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _logCollector = logCollector;
        }

        [UnitOfWork(IsDisabled = false)]
        public async Task ExecutePA(DateTime datetime)
        {
            using (var uow = _unitOfWorkManager.Begin(new AbpUnitOfWorkOptions() { IsTransactional = true }, true))
            {
                var pAs = await _pARepository.GetPAs(datetime);

                foreach (var pa in pAs)
                {
                    DateTime runDate = default;
                    try
                    {
                        runDate = pa.LastExecution.HasValue ? pa.LastExecution.Value.Date.AddDays(1) : pa.StartDate.Date;
                        while (runDate <= datetime.Date)
                        {
                            await SendPiToAssessor(pa, runDate);
                            pa.UpdateEntity(pa, GP_PaStatus.InProgress, runDate);
                            runDate = runDate.AddDays(1);
                        }
                        await _pARepository.UpdateAsync(pa, true);
                        await _logCollector.PersistPALogAsync();
                        _logCollector.ClearPALog();
                        await uow.CompleteAsync();
                    }
                    catch (Exception ex)
                    {
                        await uow.RollbackAsync();
                        Logger.LogError(ex, $"Error In Job For PAId {pa.Id}");
                        _logCollector.ClearPALog();
                        _logCollector.AddToPALog(pa.Id, null, null, null, null, Gp_PAErrorType.Error, ex.Message, runDate);
                        await _logCollector.PersistPALogAsync();
                    }
                }
            }
        }



        public async Task SendPiToAssessor(PA pa, DateTime runDate)
        {
            foreach (var paorganizationentry in pa.PAOrganizationEntrys)
            {
                List<PAResultEmployeeInfo> employeeInfos = null;
                // var o = paorganizationentry.OrganizationEntryId;
                if (paorganizationentry.OrganizationEntry.OrganizatuinEntryTypeIndex == Gp_OrganizationEntry.Department)
                {
                    employeeInfos = paorganizationentry.OrganizationEntry.Department.EmployeeContractAuthorizes
                                       .Where(x => x.EmployeeContract.StartDate <= runDate && x.EmployeeContract.EndDate >= runDate)
                                       .Select(p => new PAResultEmployeeInfo
                                       {
                                           EmployeeId = p.EmployeeContract.EmployeeId,
                                           PositionId = p.PositionId.Value
                                       })
                                       .ToList();

                }
                else
                {
                    employeeInfos = paorganizationentry.OrganizationEntry.Position.EmployeeContractAuthorizes
                                       .Where(x => x.EmployeeContract.StartDate <= runDate && x.EmployeeContract.EndDate >= runDate)
                                       .Select(p => new PAResultEmployeeInfo
                                       {
                                           EmployeeId = p.EmployeeContract.EmployeeId,
                                           PositionId = p.PositionId.Value
                                       })
                                       .ToList();

                }

                foreach (var paorganizationentrycategory in paorganizationentry.PAOrganizationEntryCategories)
                {
                    foreach (var paorganizationentrycategorypi in paorganizationentrycategory.PAOrganizationEntryCategoryPIs)
                    {
                        await PreparePAResultPI(pa, paorganizationentry, paorganizationentrycategory, paorganizationentrycategorypi, employeeInfos, runDate);
                    }
                }
            }
        }



        private async Task PreparePAResultPI(PA pA, PAOrganizationEntry pAOrganizationEntry, PAOrganizationEntryCategory pAOrganizationEntryCategory, PAOrganizationEntryCategoryPI pAOrganizationEntryCategoryPI, List<PAResultEmployeeInfo> pAResultEmployeeInfos, DateTime runDate)
        {

            var lastResultPi = _pAResultPIRepository.OrderByDescending(x => x.IterationNumber).ThenByDescending(x => x.Id).FirstOrDefault(x => x.OperationEvaluationCategoryId == pAOrganizationEntryCategoryPI.Id);
            int iterationNumber = 1;
            if (lastResultPi != null)
            {
                iterationNumber = lastResultPi.IterationNumber + 1;
            }
            //DateTime effectiveDate = 
            switch (pAOrganizationEntryCategoryPI.EvaluationPeriodIndex)
            {
                case Gp_EvaluationPeriod.Weekly:
                    DateTime dueDateWeekly = _persianCalendarManager.GetWeekLastDay(runDate);
                    if (dueDateWeekly.Date.AddDays(-pA.DaysBeforeDueDateWeekly) == runDate)
                    {
                        await SavePaResultPIs(pA, pAOrganizationEntry, pAOrganizationEntryCategory, pAOrganizationEntryCategoryPI, pAResultEmployeeInfos, iterationNumber, runDate);
                    }
                    break;
                case Gp_EvaluationPeriod.Monthly:
                    DateTime dueDateMonthly = _persianCalendarManager.GetMonthLastDay(runDate);
                    if (dueDateMonthly.Date.AddDays(-pA.DaysBeforeDueDateMonthly) == runDate)
                    {
                        await SavePaResultPIs(pA, pAOrganizationEntry, pAOrganizationEntryCategory, pAOrganizationEntryCategoryPI, pAResultEmployeeInfos, iterationNumber, runDate);
                    }
                    break;
                case Gp_EvaluationPeriod.Quarterly:
                    DateTime dueDateQuarterly = _persianCalendarManager.GetQuarterLastDay(runDate);
                    if (dueDateQuarterly.Date.AddDays(-pA.DaysBeforeDueDateQuarterly) == runDate)
                    {
                        await SavePaResultPIs(pA, pAOrganizationEntry, pAOrganizationEntryCategory, pAOrganizationEntryCategoryPI, pAResultEmployeeInfos, iterationNumber, runDate);
                    }
                    break;
                case Gp_EvaluationPeriod.Yearly:
                    DateTime dueDateYearly = _persianCalendarManager.GetYearLastDay(runDate);
                    if (dueDateYearly.Date.AddDays(-pA.DaysBeforeDueDateYearly) == runDate)
                    {
                        await SavePaResultPIs(pA, pAOrganizationEntry, pAOrganizationEntryCategory, pAOrganizationEntryCategoryPI, pAResultEmployeeInfos, iterationNumber, runDate);
                    }
                    break;
            }

        }


        private async Task SavePaResultPIs(PA pA, PAOrganizationEntry pAOrganizationEntry, PAOrganizationEntryCategory pAOrganizationEntryCategory, PAOrganizationEntryCategoryPI pAOrganizationEntryCategoryPI, List<PAResultEmployeeInfo> pAResultEmployeeInfos, int itrationNumber, DateTime runDate)
        {

            if (pAOrganizationEntryCategoryPI.IsGroupingEvaluation)
            {
                var pAResultPIParent = await _pAResultPIRepository.InsertAsync(new PAResultPI(pA.Id, null, pAOrganizationEntryCategoryPI.Id, runDate, itrationNumber, pAOrganizationEntryCategoryPI.IsGroupingEvaluation, null, pAOrganizationEntryCategory.OperationEvaluationCategoryId, pAOrganizationEntryCategory.Weight, null, runDate), true);
                var assessorSaved = false;
                if (pAOrganizationEntry.OrganizationEntry.OrganizatuinEntryTypeIndex == Gp_OrganizationEntry.Department)
                {
                    var assessorTypes = pAOrganizationEntryCategoryPI.PAOrganizationEntryCategoryPIAssessors.Where(x => x.AssessorTypeIndex == Gp_AssessorType.Manager || x.AssessorTypeIndex == Gp_AssessorType.CoWorker).Select(x => x.AssessorTypeIndex.ToString()).ToList();
                    if (assessorTypes.Count > 0)
                    {
                        _logCollector.AddToPALog(pA.Id, pAOrganizationEntry.Id, pAOrganizationEntryCategory.Id, pAOrganizationEntryCategoryPI.Id, null, Gp_PAErrorType.Warning, "ارزیابی گروهی در دسته بندی دپارتمان نمی تواند ارزیابی کننده های مدیر و همکار داشته باشد", runDate);
                    }
                    else
                    {
                        assessorSaved = await SavePAResultPIAssessor(pAResultPIParent, null, null, pAOrganizationEntry, pAOrganizationEntryCategoryPI, runDate);
                    }
                }
                else
                {
                    assessorSaved = await SavePAResultPIAssessor(pAResultPIParent, null, pAOrganizationEntry.OrganizationEntryId, pAOrganizationEntry, pAOrganizationEntryCategoryPI, runDate);
                }
                if (!assessorSaved)
                {

                    _logCollector.AddToPALog(pA.Id, pAOrganizationEntry.Id, pAOrganizationEntryCategory.Id, pAOrganizationEntryCategoryPI.Id, null, Gp_PAErrorType.Warning, $"ارزیابی کننده ای معتبری برای این شاخص یافت نشد", runDate);

                    await _pAResultPIRepository.DeleteAsync(pAResultPIParent);
                    return;
                }
                foreach (var pAResultEmployeeInfo in pAResultEmployeeInfos)
                {
                    // برای گرفتن وزن  مرتبط با هر سمت در هر کتگوری در شاخص های گروهی
                    var weight = pAOrganizationEntryCategory.Weight;
                    var emploayPositionPAOrganizationEntry = pA.PAOrganizationEntrys.FirstOrDefault(x => x.OrganizationEntryId == pAResultEmployeeInfo.PositionId);
                    if (emploayPositionPAOrganizationEntry != null)
                    {
                        var employeCategory = emploayPositionPAOrganizationEntry.PAOrganizationEntryCategories.FirstOrDefault(x => x.OperationEvaluationCategoryId == pAOrganizationEntryCategory.Id);
                        if (employeCategory != null)
                        {
                            weight = employeCategory.Weight;
                        }
                    }

                    var pAResultPI = await _pAResultPIRepository.InsertAsync(new PAResultPI(pA.Id, pAResultPIParent.Id, pAOrganizationEntryCategoryPI.Id, runDate, itrationNumber, pAOrganizationEntryCategoryPI.IsGroupingEvaluation, pAResultEmployeeInfo.EmployeeId, pAOrganizationEntryCategory.OperationEvaluationCategoryId, weight, pAResultEmployeeInfo.PositionId, runDate), true);
                    await SavePAResultPIAssessor(pAResultPI, pAResultEmployeeInfo.EmployeeId, pAResultEmployeeInfo.PositionId, pAOrganizationEntry, pAOrganizationEntryCategoryPI, runDate);
                }

            }
            else
            {
                foreach (var pAResultEmployeeInfo in pAResultEmployeeInfos)
                {
                    var pAResultPI = await _pAResultPIRepository.InsertAsync(new PAResultPI(pA.Id, null, pAOrganizationEntryCategoryPI.Id, runDate, itrationNumber, pAOrganizationEntryCategoryPI.IsGroupingEvaluation, pAResultEmployeeInfo.EmployeeId, pAOrganizationEntryCategory.OperationEvaluationCategoryId, pAOrganizationEntryCategory.Weight, pAResultEmployeeInfo.PositionId, runDate), true);
                    var assessorSaved = await SavePAResultPIAssessor(pAResultPI, pAResultEmployeeInfo.EmployeeId, pAResultEmployeeInfo.PositionId, pAOrganizationEntry, pAOrganizationEntryCategoryPI, runDate);
                    if (!assessorSaved)
                    {
                        _logCollector.AddToPALog(pA.Id, pAOrganizationEntry.Id, pAOrganizationEntryCategory.Id, pAOrganizationEntryCategoryPI.Id, null, Gp_PAErrorType.Warning, $"ارزیابی کننده ای معتبری برای این شاخص یافت نشد", runDate);

                        await _pAResultPIRepository.DeleteAsync(pAResultPI);
                    }
                }
            }
        }


        private async Task<bool> SavePAResultPIAssessor(PAResultPI pAResultPI, Guid? employeeId, Guid? employeePositionId, PAOrganizationEntry pAOrganizationEntry, PAOrganizationEntryCategoryPI pAOrganizationEntryCategoryPI, DateTime runDate)
        {
            List<Guid> assessors = new List<Guid>();
            var assessorSaved = false;
            foreach (var PAOrganizationEntryCategoryPIAssessors in pAOrganizationEntryCategoryPI.PAOrganizationEntryCategoryPIAssessors.ToList())
            {
                switch (PAOrganizationEntryCategoryPIAssessors.AssessorTypeIndex)
                {
                    case Gp_AssessorType.Manager:
                        if (employeePositionId.HasValue)
                        {
                            var managerEmployeeId = await GetCurrentPositionManager(employeePositionId.Value, runDate);
                            assessors.Add(managerEmployeeId);
                        }
                        else
                        {

                            _logCollector.AddToPALog(pAOrganizationEntry.PAId, pAOrganizationEntry.Id, pAOrganizationEntryCategoryPI.PAOrganizationEntryCategoryId, pAOrganizationEntryCategoryPI.Id, pAResultPI.Id, Gp_PAErrorType.Warning, $"نوع ارزیابی کننده مدیر نباید برای شاخص با دسته بندی دپارتمان انتخاب شود", runDate);

                        }
                        break;
                    case Gp_AssessorType.CoWorker:
                        if (employeeId.HasValue)
                        {
                            var employeeIds = pAOrganizationEntry.OrganizationEntry.Position.EmployeeContractAuthorizes.Where(x => x.EmployeeContract.EmployeeId != employeeId).Select(x => x.EmployeeContract.EmployeeId).ToList();

                            if (employeeIds.Count > 0)
                            {
                                Random rnd = new Random();
                                int r = rnd.Next(employeeIds.Count);
                                var coWorkerEmployeeId = employeeIds[r];
                                assessors.Add(coWorkerEmployeeId);
                            }
                            else
                            {
                                var employeePosition = pAOrganizationEntry.OrganizationEntry.Position.EmployeeContractAuthorizes.FirstOrDefault(x => x.EmployeeContract.EmployeeId == employeeId);
                                _logCollector.AddToPALog(pAOrganizationEntry.PAId, pAOrganizationEntry.Id, pAOrganizationEntryCategoryPI.PAOrganizationEntryCategoryId, pAOrganizationEntryCategoryPI.Id, pAResultPI.Id, Gp_PAErrorType.Warning, $"همکاری برای کارمند {employeePosition?.EmployeeContract.Employee.FirstName + " " + employeePosition?.EmployeeContract.Employee.LastName} در سمت  {employeePosition?.Position.OrganizationEntry.Name} یافت نشد.", runDate);
                            }

                        }
                        else
                        {

                            _logCollector.AddToPALog(pAOrganizationEntry.PAId, pAOrganizationEntry.Id, pAOrganizationEntryCategoryPI.PAOrganizationEntryCategoryId, pAOrganizationEntryCategoryPI.Id, pAResultPI.Id, Gp_PAErrorType.Warning, $"ایدی کارمند خالی است نباید  نوع ارزیابی کننده همکار در شاخصی با دسته بندی دپارتمان انتخاب شود", runDate);
                        }
                        break;
                    case Gp_AssessorType.Position:
                        var emloyeeIdsPosition = _employeeContractAuthorizesRepository.Where(x => x.PositionId == PAOrganizationEntryCategoryPIAssessors.CustomAssessorId.Value).Select(x => x.EmployeeContract.EmployeeId).ToList();
                        if (emloyeeIdsPosition.Count > 0)
                        {
                            assessors.AddRange(emloyeeIdsPosition);
                        }
                        else
                        {
                            var position = _positionRepository.FirstOrDefault(x => x.Id == PAOrganizationEntryCategoryPIAssessors.CustomAssessorId.Value);

                            _logCollector.AddToPALog(pAOrganizationEntry.PAId, pAOrganizationEntry.Id, pAOrganizationEntryCategoryPI.PAOrganizationEntryCategoryId, pAOrganizationEntryCategoryPI.Id, pAResultPI.Id, Gp_PAErrorType.Warning, $"کارمندی برای سمت {position?.Name} یافت نشد", runDate);

                        }
                        break;
                    case Gp_AssessorType.Employee:
                        assessors.Add(PAOrganizationEntryCategoryPIAssessors.CustomAssessorId.Value);
                        break;
                }
            }

            if (assessors.Count > 0)
            {
                assessorSaved = true;
            }

            var distinctAssessor = assessors.Distinct().ToList();
            foreach (var assessor in distinctAssessor)
            {
                await _pAResultPIAssessors.InsertAsync(new PAResultPIAssessor(pAResultPI.Id, assessor, null, Gp_AssessStatus.Pennding, null, null, null));
            }

            return assessorSaved;
        }

        private async Task<Guid> GetCurrentEmployeeManager(Guid employeeId, DateTime dateTime)
        {
            var currentPosition = await GetCurrentEmployeeMainPosition(employeeId, dateTime);
            if (currentPosition != null && currentPosition.OrganizationEntry.ParentId.HasValue)
            {
                try
                {
                    var salaries = await _employeeContractAuthorizesRepository.GetCurrentSalariesAsync(currentPosition.OrganizationEntry.ParentId.Value, dateTime);
                    if (salaries != null)
                    {
                        return salaries.EmployeeContract.EmployeeId;
                    }
                    else
                    {
                        throw new UserFriendlyException("salaries is null");
                    }
                }
                catch (Exception ex)
                {

                    throw;
                }


            }
            else
            {
                throw new UserFriendlyException("currentPosition is null or ParentId is null");
            }
        }


        private async Task<Guid> GetCurrentPositionManager(Guid positionId, DateTime dateTime)
        {
            var currentPosition = _positionRepository.FirstOrDefault(x => x.Id == positionId);
            if (currentPosition != null && currentPosition.ParentId.HasValue)
            {
                try
                {
                    var salaries = await _employeeContractAuthorizesRepository.GetCurrentSalariesAsync(currentPosition.ParentId.Value, dateTime);
                    if (salaries != null)
                    {
                        return salaries.EmployeeContract.EmployeeId;
                    }
                    else
                    {
                        //SharedPALog.PALogs.Add(new PALogInfo()
                        //{
                        //    Exception = $"قردادی برای سمت {currentPosition.ParentId.Value} یافت نشد",
                        //    PAErrorTypeIndex = Gp_PAErrorType.Warning
                        //});
                        var position = _positionRepository.FirstOrDefault(x => x.Id == currentPosition.ParentId.Value);
                        throw new UserFriendlyException($"قردادی برای سمت {position?.Name} یافت نشد");
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {

                //SharedPALog.PALogs.Add(new PALogInfo()
                //{
                //    Exception = $"PositionId {positionId} doesnt Have Parent",
                //    PAErrorTypeIndex = Gp_PAErrorType.Warning,
                //});
                var position = _positionRepository.FirstOrDefault(x => x.Id == positionId);

                throw new UserFriendlyException($"سمت {position?.Name} پرنتی ندارد");
            }
        }

        private async Task<Position> GetCurrentEmployeeMainPosition(Guid employeeId, DateTime runDate)
        {
            var employeeContract = _employeeContractsRepository.FirstOrDefault(x => x.EmployeeId == employeeId && x.StartDate <= runDate && x.EndDate >= runDate);
            try
            {
                var authorizes = await _employeeContractAuthorizesRepository.GetEmployeeContractAuthorizeAsync(employeeContract.Id);
                var mainAuthorize = authorizes.FirstOrDefault(p => p.IsMain);
                if (mainAuthorize != null)
                {
                    return mainAuthorize.Position;
                }
                else
                {
                    return _positionRepository.Where(z => authorizes.Select(x => x.PositionId.Value).ToList().Contains(z.Id) && z.Position != null).Select(x => x.Position).First();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

    }

    public class PAResultEmployeeInfo
    {
        public Guid EmployeeId { get; set; }
        public Guid PositionId { get; set; }
    }


}
