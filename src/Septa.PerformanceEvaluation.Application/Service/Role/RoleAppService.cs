﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Septa.PerformanceEvaluation.Interface;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Identity;

namespace Septa.PerformanceEvaluation.Service.Role
{

    [Authorize]
    public class RoleAppService : IdentityRoleAppService, IRoleAppService
    {
        private readonly IdentityRoleManager _roleManager;
        private readonly IIdentityRoleRepository _roleRepository;

        public RoleAppService(IdentityRoleManager roleManager, IIdentityRoleRepository roleRepository) : base(roleManager, roleRepository)
        {
            _roleManager = roleManager;
            _roleRepository = roleRepository;
        }

        public override async Task<IdentityRoleDto> CreateAsync(IdentityRoleCreateDto input)
        {
            var role = new Volo.Abp.Identity.IdentityRole(GuidGenerator.Create(), input.Name, CurrentTenant.Id);

            role.IsDefault = input.IsDefault;
            role.IsPublic = input.IsPublic;

            (await _roleManager.CreateAsync(role)).CheckErrors();
            await CurrentUnitOfWork.SaveChangesAsync();

            return ObjectMapper.Map<Volo.Abp.Identity.IdentityRole, IdentityRoleDto>(role);
        }

        public override async Task DeleteAsync(Guid id)
        {
            var role = await _roleManager.FindByIdAsync(id.ToString());
            if (role == null)
            {
                return;
            }

            (await _roleManager.DeleteAsync(role)).CheckErrors();
        }

        public override async Task<IdentityRoleDto> GetAsync(Guid id)
        {
            return ObjectMapper.Map<Volo.Abp.Identity.IdentityRole, IdentityRoleDto>(
                await _roleManager.GetByIdAsync(id));
        }

        public override async Task<PagedResultDto<IdentityRoleDto>> GetListAsync(PagedAndSortedResultRequestDto input)
        {
            var list = await _roleRepository.GetListAsync(input.Sorting, input.MaxResultCount, input.SkipCount);
            var totalCount = await _roleRepository.GetCountAsync();

            return new PagedResultDto<IdentityRoleDto>(
                totalCount,
                ObjectMapper.Map<List<Volo.Abp.Identity.IdentityRole>, List<IdentityRoleDto>>(list)
                );
        }

        public override async Task<IdentityRoleDto> UpdateAsync(Guid id, IdentityRoleUpdateDto input)
        {
            var role = await _roleManager.GetByIdAsync(id);
            role.ConcurrencyStamp = input.ConcurrencyStamp;

            (await _roleManager.SetRoleNameAsync(role, input.Name)).CheckErrors();

            role.IsDefault = input.IsDefault;
            role.IsPublic = input.IsPublic;

            (await _roleManager.UpdateAsync(role)).CheckErrors();
            await CurrentUnitOfWork.SaveChangesAsync();

            return ObjectMapper.Map<Volo.Abp.Identity.IdentityRole, IdentityRoleDto>(role);
        }

    }
}
