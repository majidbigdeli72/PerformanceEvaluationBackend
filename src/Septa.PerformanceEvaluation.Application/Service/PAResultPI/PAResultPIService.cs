﻿using Microsoft.AspNetCore.Authorization;
using Septa.PerformanceEvaluation.Const;
using Septa.PerformanceEvaluation.Enums;
using Septa.PerformanceEvaluation.IRepository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp;
using Volo.Abp.Domain.Repositories;

namespace Septa.PerformanceEvaluation.Service
{
    [Authorize(Roles = Roles.PaAdmin)]
    public class PAResultPIService : PerformanceEvaluationAppService
    {
        private readonly IRepository<Entities.PAResultPI, int> _pAResultPIRepository;
        private readonly IPARepository _pARespository;
        private readonly IPALogRepository _pALogRepository;

        public PAResultPIService(
            IRepository<Entities.PAResultPI, int> pAResultPIRepository,
            IPARepository pARespository,
            IPALogRepository pALogRepository
            )
        {
            _pAResultPIRepository = pAResultPIRepository;
            _pARespository = pARespository;
            _pALogRepository = pALogRepository;
        }

        public async Task Delete(int pAId, DateTime relatedDate)
        {
            relatedDate = relatedDate.Date;
            var pA = await _pARespository.FindAsync(pAId);
            if (relatedDate < pA.StartDate.Date)
            {
                throw new UserFriendlyException("تاریخ وارد شده نمی تواند کمتر از تاریخ شروع ارزیابی باشد");
            }

            if (pA.LastExecution.HasValue && relatedDate.AddDays(-1) > pA.LastExecution)
            {
                throw new UserFriendlyException("تاریخ وارد شده نمی تواند بزرگتر از تاریخ اخرین اجرا باشد");
            }

            if (relatedDate == pA.StartDate.Date)
            {
                pA.UpdateEntity(pA, GP_PaStatus.ReadyToRun, null);
            }
            else
            {
                pA.UpdateEntity(pA, GP_PaStatus.ReadyToRun, relatedDate.AddDays(-1));
            }

            await _pARespository.UpdateAsync(pA, true);

            await _pAResultPIRepository.DeleteAsync(x => x.RelatedDate >= relatedDate && x.PAId == pAId, true);

            await _pALogRepository.DeleteAsync(x => x.RelatedDate >= relatedDate && x.PAId == pAId);
        }
    }
}
