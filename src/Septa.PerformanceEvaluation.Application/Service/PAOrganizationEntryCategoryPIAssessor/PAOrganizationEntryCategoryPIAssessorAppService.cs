﻿using Microsoft.AspNetCore.Authorization;
using Septa.PerformanceEvaluation.Const;
using Septa.PerformanceEvaluation.Dto;
using Septa.PerformanceEvaluation.Interface;
using Septa.PerformanceEvaluation.IRepository;
using Volo.Abp;

namespace Septa.PerformanceEvaluation.Service
{
    [Authorize(Roles = Roles.PaAdmin)]
    public class PAOrganizationEntryCategoryPIAssessorAppService : PerformanceEvaluationCrudAppService<Entities.PAOrganizationEntryCategoryPIAssessor, PAOrganizationEntryCategoryPIAssessorGetDto, int, PagedAndSortedResultDto, PAOrganizationEntryCategoryPIAssessorCreateDto, PAOrganizationEntryCategoryPIAssessorUpdateDto>, IPAOrganizationEntryCategoryPIAssessorAppService
    {
        public PAOrganizationEntryCategoryPIAssessorAppService(IPAOrganizationEntryCategoryPIAssessorRepository repository) : base(repository)
        {
        }
    }
}
