﻿using Microsoft.AspNetCore.Authorization;
using Septa.PerformanceEvaluation.Const;
using Septa.PerformanceEvaluation.Dto;
using Septa.PerformanceEvaluation.Entities;
using Septa.PerformanceEvaluation.Interface.Employee;
using Septa.PerformanceEvaluation.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;

namespace Septa.PerformanceEvaluation.Service
{
    [Authorize(Roles = Roles.PaAdmin)]
    public class EmployeeAppService : PerformanceEvaluationCrudAppService<OrganizationEntry, OrganizationEntryEmployeeGetDto, Guid, PagedAndSortedResultDto, OrganizationEntryEmployeeCreateDto, OrganizationEntryEmployeeUpdateDto>, IEmployeeAppService
    {
        private readonly IEmployeeRepository _repository;

        public EmployeeAppService(IEmployeeRepository repository) : base(repository)
        {
            _repository = repository;
        }

        public override async Task<PagedResultDto<OrganizationEntryEmployeeGetDto>> GetListAsync(PagedAndSortedResultDto input)
        {
            var employess = await _repository.GetEmployeListAsync();
            var employeesDto = ObjectMapper.Map<List<OrganizationEntry>, List<OrganizationEntryEmployeeGetDto>>(employess).OrderBy(x => x.Employee.FullName).ToList();

            return new PagedResultDto<OrganizationEntryEmployeeGetDto>()
            {
                Items = employeesDto,
                TotalCount = employess.Count
            };
        }

        protected override IQueryable<OrganizationEntry> CreateFilteredQuery(PagedAndSortedResultDto input)
        {
            return base.Repository.WithDetails().Where(x => x.OrganizatuinEntryTypeIndex == Enums.Gp_OrganizationEntry.Employee);
        }
    }
}
