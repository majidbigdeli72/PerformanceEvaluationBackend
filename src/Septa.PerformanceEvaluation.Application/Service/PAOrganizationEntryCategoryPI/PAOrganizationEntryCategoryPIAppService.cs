﻿using Microsoft.AspNetCore.Authorization;
using Septa.PerformanceEvaluation.Const;
using Septa.PerformanceEvaluation.Dto;
using Septa.PerformanceEvaluation.Interface;
using Septa.PerformanceEvaluation.IRepository;
using Volo.Abp;

namespace Septa.PerformanceEvaluation.Service
{
    [Authorize(Roles = Roles.PaAdmin)]
    public class PAOrganizationEntryCategoryPIAppService : PerformanceEvaluationCrudAppService<Entities.PAOrganizationEntryCategoryPI, PAOrganizationEntryCategoryPIGetDto, int, PagedAndSortedResultDto, PAOrganizationEntryCategoryPICreateDto, PAOrganizationEntryCategoryPIUpdateDto>, IPAOrganizationEntryCategoryPIAppService
    {
        public PAOrganizationEntryCategoryPIAppService(IPAOrganizationEntryCategoryPIRepository repository) : base(repository)
        {
        }
    }
}
