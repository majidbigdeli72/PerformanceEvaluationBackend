﻿using Microsoft.AspNetCore.Authorization;
using Septa.PerformanceEvaluation.Const;
using Septa.PerformanceEvaluation.Dto;
using Septa.PerformanceEvaluation.Entities;
using Septa.PerformanceEvaluation.Enums;
using Septa.PerformanceEvaluation.Interface;
using Septa.PerformanceEvaluation.IRepository;
using Septa.PerformanceEvaluation.LinqView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Volo.Abp;
using Volo.Abp.Application.Dtos;

namespace Septa.PerformanceEvaluation.Service
{
    [Authorize]
    public class PAResultPIAssessorAppService : PerformanceEvaluationAppService, IPAResultPIAssessorAppService
    {
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IPAResultPIAssessorRepository _pAResultPIAssessorRepository;
        private readonly IPAResultPIRepository _pAResultPIRepository;
        private readonly IPAOrganizationEntryCategoryPIRepository _pAOrganizationEntryCategoryPIRepository;

        public PAResultPIAssessorAppService(
            IEmployeeRepository employeeRepository,
            IPAResultPIAssessorRepository pAResultPIAssessorRepository,
            IPAResultPIRepository pAResultPIRepository,
            IPAOrganizationEntryCategoryPIRepository pAOrganizationEntryCategoryPIRepository)
        {
            _employeeRepository = employeeRepository;
            _pAResultPIAssessorRepository = pAResultPIAssessorRepository;
            _pAResultPIRepository = pAResultPIRepository;
            _pAOrganizationEntryCategoryPIRepository = pAOrganizationEntryCategoryPIRepository;
        }
        public async Task<PagedResultDto<PAResultPIAssessorGetDto>> GetCurentUserPAResultPIAsync()
        {
            var userName = CurrentUser.UserName;
            var assessorId = (await _employeeRepository.GetEmployeeByUserNameAsync(userName))?.Id;
            if (assessorId.HasValue)
            {
                var result = await _pAResultPIAssessorRepository.GetListPAResultPIAssessorByAssessorIdAsync(assessorId.Value);
                var pAResultPIAssessors = ObjectMapper.Map<List<LVwPAResultPIAssesor>, List<PAResultPIAssessorGetDto>>(result);
                return new PagedResultDto<PAResultPIAssessorGetDto>(pAResultPIAssessors.Count, pAResultPIAssessors);
            }

            throw new UserFriendlyException(L["EmployeeNotFound", userName]);
        }


        [Authorize(Roles = Roles.PaAdmin)]
        public async Task<PagedResultDto<PAResultPIAssessorGetDto>> GetAssessorPAResultPIAsync(Guid assessorId)
        {
            var result = await _pAResultPIAssessorRepository.GetListPAResultPIAssessorByAssessorIdAsync(assessorId);
            var pAResultPIAssessors = ObjectMapper.Map<List<LVwPAResultPIAssesor>, List<PAResultPIAssessorGetDto>>(result);
            return new PagedResultDto<PAResultPIAssessorGetDto>(pAResultPIAssessors.Count, pAResultPIAssessors);
        }

        public async Task<bool> SavePAResultPIAssessor(PAResultPIAssessorUpdateDto pAResultPIAssessor)
        {

            var pAOrganizationEntryCategoryPI = await _pAOrganizationEntryCategoryPIRepository.FindAsync(pAResultPIAssessor.PAOrganizationEntryCategoryPIId);
            decimal normalizedValue = 0;
            switch (pAResultPIAssessor.CalculationTypeIndex)
            {
                case Enums.Gp_CalculationType.Additive:
                    normalizedValue = NormalizeAdditive(pAOrganizationEntryCategoryPI, pAResultPIAssessor.Value);

                    break;
                case Enums.Gp_CalculationType.Decrease:
                    normalizedValue = NormalizeDecrease(pAOrganizationEntryCategoryPI, pAResultPIAssessor.Value);

                    break;
                default:
                    break;
            }
            await UpdatePAResultPIAssessor(normalizedValue, pAResultPIAssessor.Value, pAResultPIAssessor.PAResultAssessorId, null);
            return true;
        }

        [Authorize(Roles = Roles.PaAdmin)]
        public async Task<bool> ChangePaResultPIAssessorStatus(int paResultPIAssessorId, Gp_AssessStatus assessStatus)
        {
            var pAResulPIAssessor = await _pAResultPIAssessorRepository.FindAsync(paResultPIAssessorId);
            pAResulPIAssessor.UpdateEntity(assessStatus);
            await _pAResultPIAssessorRepository.UpdateAsync(pAResulPIAssessor, true);
            return true;
        }

        [Authorize(Roles = Roles.PaAdmin)]
        public async Task<bool> ByPassPaResultPIAssessor(int paResultPIAssessorId)
        {
            return await ChangePaResultPIAssessorStatus(paResultPIAssessorId, Gp_AssessStatus.ByPass);
        }

        [Authorize(Roles = Roles.PaAdmin)]
        public async Task<bool> ChangeAssessor(int paResultPIAssessorId,Guid assessedById)
        {
            var pAResulPIAssessor = await _pAResultPIAssessorRepository.FindAsync(paResultPIAssessorId);
            pAResulPIAssessor.UpdateEntity(assessedById);
            await _pAResultPIAssessorRepository.UpdateAsync(pAResulPIAssessor, true);
            return true;
        }

        [Authorize(Roles = Roles.PaAdmin)]
        public async Task<bool> ChangePAResultPIEmployee(int paResultPIId, Guid employeeId)
        {
            var pAResulPI = await _pAResultPIRepository.FindAsync(paResultPIId);
            pAResulPI.UpdateEntity(employeeId);
            await _pAResultPIRepository.UpdateAsync(pAResulPI, true);
            return true;
        }

        private decimal NormalizeAdditive(PAOrganizationEntryCategoryPI pAOrganizationEntryCategoryPI, decimal value)
        {

            var normalizedValue = ((value - pAOrganizationEntryCategoryPI.Min) / (pAOrganizationEntryCategoryPI.Max - pAOrganizationEntryCategoryPI.Min));

            if (normalizedValue > 1)
            {
                normalizedValue = 1;
            }

            if (normalizedValue < 0)
            {
                normalizedValue = 0;
            }

            return normalizedValue;
        }

        private decimal NormalizeDecrease(PAOrganizationEntryCategoryPI pAOrganizationEntryCategoryPI, decimal value)
        {
            var normalizedValue = ((pAOrganizationEntryCategoryPI.Max - value) / (pAOrganizationEntryCategoryPI.Max - pAOrganizationEntryCategoryPI.Min));

            if (normalizedValue > 1)
            {
                normalizedValue = 1;
            }

            if (normalizedValue < 0)
            {
                normalizedValue = 0;
            }

            return normalizedValue;
        }

        private async Task UpdatePAResultPIAssessor(decimal normalizedValue, decimal value, int pAResultPIAssessorId, int? QuestionnaireResultId)
        {
            var pAResultPIAssessor = await _pAResultPIAssessorRepository.FindAsync(pAResultPIAssessorId, true);

            if (pAResultPIAssessor.PAResultPI.IsGroupingEvaluation)
            {
                var pAResultPIs = _pAResultPIRepository.WithDetails().Where(x => x.ParentId == pAResultPIAssessor.PAResultPI.Id || x.Id == pAResultPIAssessor.PAResultPI.Id).ToList();

                foreach (var pAResultPI in pAResultPIs)
                {
                    foreach (var item in pAResultPI.PAResultPIAssessors.Where(x => x.AssessedById == pAResultPIAssessor.AssessedById).ToList())
                    {
                        var paResultPIAssessor = item.UpdateEntity(item, item.PAResultPIId, item.AssessedById, QuestionnaireResultId, Gp_AssessStatus.Evaluated, value, normalizedValue, item.AssessDate ?? DateTime.Now, DateTime.Now);
                        await _pAResultPIAssessorRepository.UpdateAsync(paResultPIAssessor, true);
                    }
                }
            }
            else
            {
                var paResultPIAssessor = pAResultPIAssessor.UpdateEntity(pAResultPIAssessor, pAResultPIAssessor.PAResultPIId, pAResultPIAssessor.AssessedById, QuestionnaireResultId, Gp_AssessStatus.Evaluated, value, normalizedValue, pAResultPIAssessor.AssessDate ?? DateTime.Now, DateTime.Now);
                await _pAResultPIAssessorRepository.UpdateAsync(paResultPIAssessor, true);
            }
        }


    }
}
