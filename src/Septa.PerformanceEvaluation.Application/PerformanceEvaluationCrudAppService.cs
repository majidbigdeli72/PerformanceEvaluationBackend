﻿using Septa.PerformanceEvaluation.Localization;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Entities;
using Volo.Abp.Domain.Repositories;

namespace Septa.PerformanceEvaluation
{
    /* Inherit your application services from this class.
 */
    public abstract class PerformanceEvaluationCrudAppService<TEntity, TEntityDto, TKey, TGetListInput, TCreateInput, TUpdateInput> : CrudAppService<TEntity, TEntityDto, TKey, TGetListInput, TCreateInput, TUpdateInput>
       where TEntity : class, IEntity<TKey>
        where TEntityDto : IEntityDto<TKey>
    {
        // public IAsyncQueryableExecuter AsyncQueryableExecuter { get; set; }

        protected PerformanceEvaluationCrudAppService(IRepository<TEntity, TKey> repository):base(repository)
        {
            LocalizationResource = typeof(PerformanceEvaluationResource);
            //  AsyncQueryableExecuter = DefaultAsyncQueryableExecuter.Instance;
        }

    }


}
