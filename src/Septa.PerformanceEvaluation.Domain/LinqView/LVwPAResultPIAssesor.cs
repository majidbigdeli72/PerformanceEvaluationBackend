﻿using Septa.PerformanceEvaluation.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Septa.PerformanceEvaluation.LinqView
{
    public class LVwPAResultPIAssesor
    {

        public LVwPAResultPIAssesor()
        {

        }

        public LVwPAResultPIAssesor(
            int id,
            Gp_AssessStatus assessStatusIndex,
            string firstName, string lastName,
            int iterationNumber, string membership,
            string pAName, string performanceIndicatorName,
            int? questionnaireId,
            string operationEvaluationCategoryName,
            Gp_EvaluationType evaluationTypeIndex,
            Gp_CalculationType calculationTypeIndex,
            int pAOrganizationEntryCategoryPIId,
            int pAResultPIId,
            DateTime? assessDate,
            int? questionnaireResultId,
            decimal? value)
        {
            Id = id;
            AssessStatusIndex = assessStatusIndex;
            FirstName = firstName;
            LastName = lastName;
            IterationNumber = iterationNumber;
            Membership = membership;
            PAName = pAName;
            PerformanceIndicatorName = performanceIndicatorName;
            QuestionnaireId = questionnaireId;
            OperationEvaluationCategoryName = operationEvaluationCategoryName;
            EvaluationTypeIndex = evaluationTypeIndex;
            CalculationTypeIndex = calculationTypeIndex;
            PAOrganizationEntryCategoryPIId = pAOrganizationEntryCategoryPIId;
            PAResultPIId = pAResultPIId;
            AssessDate = assessDate;
            QuestionnaireResultId = questionnaireResultId;
            Value = value;
        }

        public int Id { get; private set; }
        public Gp_AssessStatus AssessStatusIndex { get; private set; }
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public int IterationNumber { get; private set; }
        public string Membership { get; private set; }
        public string PAName { get; private set; }
        public string PerformanceIndicatorName { get; private set; }
        public int? QuestionnaireId { get; private set; }
        public string OperationEvaluationCategoryName { get; private set; }
        public Gp_EvaluationType EvaluationTypeIndex { get; private set; }
        public Gp_CalculationType CalculationTypeIndex { get; private set; }
        public int PAOrganizationEntryCategoryPIId { get;private set; }
        public int PAResultPIId { get;private set; }
        public DateTime? AssessDate { get; private set; }
        public int? QuestionnaireResultId { get; private set; }
        public decimal? Value { get; private set; }


    }

}
