﻿namespace Septa.PerformanceEvaluation.LinqView
{
    public class LVwMinMaxQuestionItem
    {

        public LVwMinMaxQuestionItem()
        {

        }

        public LVwMinMaxQuestionItem(decimal min, decimal max, int questionId)
        {
            Min = min;
            Max = max;
            QuestionId = questionId;
        }

        public decimal Min { get;private set; }
        public decimal Max { get;private set; }
        public int QuestionId { get;private set; }
    }

}
