﻿using Septa.PerformanceEvaluation.Enums;
using System;

namespace Septa.PerformanceEvaluation.LinqView
{
    public class LVwPaLog
    {
        public LVwPaLog()
        {

        }
        public LVwPaLog(string exception, Gp_PAErrorType pAErrorTypeIndex, DateTime relatedDate, string pAName, string pIName, string organizationEntryName)
        {
            Exception = exception;
            PAErrorTypeIndex = pAErrorTypeIndex;
            RelatedDate = relatedDate;
            PAName = pAName;
            PIName = pIName;
            OrganizationEntryName = organizationEntryName;
        }

        public string Exception { get;private set; }
        public Gp_PAErrorType PAErrorTypeIndex { get;private set; }
        public DateTime RelatedDate { get;private set; }
        public string PAName { get;private set; }
        public string PIName { get; set; }
        public string OrganizationEntryName { get;private set; }
    }

}
