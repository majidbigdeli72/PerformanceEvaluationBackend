﻿namespace Septa.PerformanceEvaluation.LinqView
{
    public class LVwQuestionResult
    {
        public LVwQuestionResult()
        {

        }
        public LVwQuestionResult(int questionId, int selectedQuestionItemId, int questionnaireResultId)
        {
            QuestionId = questionId;
            SelectedQuestionItemId = selectedQuestionItemId;
            QuestionnaireResultId = questionnaireResultId;
        }

        public int QuestionId { get; private set; }
        public int SelectedQuestionItemId { get; private set; }
        public int QuestionnaireResultId { get; private set; }
    }

}
