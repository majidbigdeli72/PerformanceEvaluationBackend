﻿using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;

namespace Septa.PerformanceEvaluation.Data
{
    /* This is used if database provider does't define
     * IPerformanceEvaluationDbSchemaMigrator implementation.
     */
    public class NullPerformanceEvaluationDbSchemaMigrator : IPerformanceEvaluationDbSchemaMigrator, ITransientDependency
    {
        public Task MigrateAsync()
        {
            return Task.CompletedTask;
        }
    }
}