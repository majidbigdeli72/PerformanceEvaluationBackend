﻿using System.Threading.Tasks;

namespace Septa.PerformanceEvaluation.Data
{
    public interface IPerformanceEvaluationDbSchemaMigrator
    {
        Task MigrateAsync();
    }
}
