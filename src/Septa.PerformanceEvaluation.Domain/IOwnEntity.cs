﻿using Volo.Abp.Domain.Entities;

namespace Septa.PerformanceEvaluation
{
    public interface IOwnEntity<T>
    {
        void UpdateEntity(T entity);
    }
}
