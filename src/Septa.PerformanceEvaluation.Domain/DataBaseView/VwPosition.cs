﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Septa.PerformanceEvaluation.DataBaseView
{
    public class VwPosition
    {
        public Guid OrganizationEntryId { get; set; }
        public Guid DepartmentId { get; set; }
        public int JobLevelIndex { get; set; }
        public Guid? D_OrganizationEntryId { get; set; }
        public Guid? D_DepartmentId { get; set; }
        public string D_JobLevelIndex { get; set; }
        public string DepartmentName { get; set; }
        public int? PositionLevel { get; set; }
        public string Path2Root { get; set; }
    }
}
