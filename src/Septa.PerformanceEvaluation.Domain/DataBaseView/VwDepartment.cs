﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Septa.PerformanceEvaluation.DataBaseView
{
    public class VwDepartment
    {
        public Guid OrganizationEntryId { get; set; }
        public Guid D_OrganizationEntryId { get; set; }
        public string Path2Root { get; set; }
    }
}
