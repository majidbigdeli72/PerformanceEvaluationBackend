﻿using Volo.Abp.Settings;

namespace Septa.PerformanceEvaluation.Settings
{
    public class PerformanceEvaluationSettingDefinitionProvider : SettingDefinitionProvider
    {
        public override void Define(ISettingDefinitionContext context)
        {
            //Define your own settings here. Example:
            //context.Add(new SettingDefinition(PerformanceEvaluationSettings.MySetting1));
        }
    }
}
