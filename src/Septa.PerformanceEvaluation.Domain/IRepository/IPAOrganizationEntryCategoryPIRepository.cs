﻿using Septa.PerformanceEvaluation.Entities;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace Septa.PerformanceEvaluation.IRepository
{
    public interface IPAOrganizationEntryCategoryPIRepository : IRepository<PAOrganizationEntryCategoryPI, int>
    {
    }

}
