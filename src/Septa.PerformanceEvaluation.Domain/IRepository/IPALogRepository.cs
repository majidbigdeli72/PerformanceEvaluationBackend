﻿using Septa.PerformanceEvaluation.Entities;
using Septa.PerformanceEvaluation.LinqView;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace Septa.PerformanceEvaluation.IRepository
{
    public interface IPALogRepository : IRepository<PALog, int>
    {
        Task<List<LVwPaLog>> GetPALogsAsync();
    }
}
