﻿using Septa.PerformanceEvaluation.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace Septa.PerformanceEvaluation.IRepository
{
    public interface IPARepository : IRepository<PA, int>
    {
        Task<List<PA>> GetPAs(DateTime dateTime);
    }
}
