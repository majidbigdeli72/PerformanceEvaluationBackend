﻿using Septa.PerformanceEvaluation.Entities;
using Volo.Abp.Domain.Repositories;

namespace Septa.PerformanceEvaluation.IRepository
{
    public interface IQuestionnaireResultRepository : IRepository<QuestionnaireResult, int>
    {
    }
}
