﻿using Septa.PerformanceEvaluation.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace Septa.PerformanceEvaluation.IRepository
{
    public interface IPAResultPIRepository : IRepository<PAResultPI, int>
    {

    }
}
