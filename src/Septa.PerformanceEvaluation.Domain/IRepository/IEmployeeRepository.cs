﻿using Septa.PerformanceEvaluation.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace Septa.PerformanceEvaluation.IRepository
{
    public interface IEmployeeRepository : IRepository<OrganizationEntry, Guid>
    {
        Task<Employee> GetEmployeeByUserNameAsync(string userName);
        Task<List<OrganizationEntry>> GetEmployeListAsync();
    }
}
