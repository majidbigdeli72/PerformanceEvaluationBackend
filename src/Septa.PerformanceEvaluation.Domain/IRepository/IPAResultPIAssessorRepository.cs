﻿using Septa.PerformanceEvaluation.Entities;
using Septa.PerformanceEvaluation.LinqView;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace Septa.PerformanceEvaluation.IRepository
{
    public interface IPAResultPIAssessorRepository : IRepository<PAResultPIAssessor, int>
    {
        Task<List<LVwPAResultPIAssesor>> GetListPAResultPIAssessorByAssessorIdAsync(Guid assessorId);

    }



}
