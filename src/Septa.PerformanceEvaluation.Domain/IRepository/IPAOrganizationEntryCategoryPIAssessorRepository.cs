﻿using Septa.PerformanceEvaluation.Entities;
using Volo.Abp.Domain.Repositories;

namespace Septa.PerformanceEvaluation.IRepository
{
    public interface IPAOrganizationEntryCategoryPIAssessorRepository : IRepository<PAOrganizationEntryCategoryPIAssessor, int>
    {
    }

}
