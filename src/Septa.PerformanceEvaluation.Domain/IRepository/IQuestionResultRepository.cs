﻿using Septa.PerformanceEvaluation.Entities;
using Septa.PerformanceEvaluation.LinqView;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace Septa.PerformanceEvaluation.IRepository
{
    public interface IQuestionResultRepository : IRepository<QuestionResult, int>
    {
        Task<List<LVwMinMaxQuestionItem>> GetMinMaxQuestionItemsAsync(int questionnaireId);
        Task<List<LVwQuestionResult>> GetQuestionResultAsync(int questionnaireResultId);

    }
}
