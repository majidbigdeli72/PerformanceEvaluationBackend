﻿using Septa.PerformanceEvaluation.Entities;
using Septa.PerformanceEvaluation.LinqView;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace Septa.PerformanceEvaluation.IRepository
{
    public interface IEmployeeContractAuthorizeRepository : IRepository<EmployeeContractAuthorize, long>
    {
        Task<List<EmployeeContractAuthorize>> GetEmployeeContractAuthorizeAsync(long employeeContractId);
        Task<EmployeeContractAuthorize> GetCurrentSalariesAsync(Guid positionId,DateTime dateTime);
    }
}
