﻿using Septa.PerformanceEvaluation.Entities;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace Septa.PerformanceEvaluation.IRepository
{
    public interface IQuestionnaireRepository : IRepository<Questionnaire, int>
    {
        Task<Questionnaire> FindQuestionnaireAsync(int questionnaireId);
        Task<int> InsertQuestionnaireAsync(Questionnaire questionnaire);
        Task UpdateQuestionnaireAsync(Questionnaire questionnaire);
    }
}
