﻿using Septa.PerformanceEvaluation.DataBaseView;
using Septa.PerformanceEvaluation.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace Septa.PerformanceEvaluation.IRepository
{
    public interface IDepartmentRepository : IRepository<OrganizationEntry, Guid>
    {
       Task<List<VwDepartment>> GetDepartmentPathAsync();
    }
}
