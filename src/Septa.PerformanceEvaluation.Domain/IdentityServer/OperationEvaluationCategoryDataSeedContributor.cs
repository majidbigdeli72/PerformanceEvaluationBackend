﻿using System.Threading.Tasks;
using Septa.PerformanceEvaluation.Entities;
using Septa.PerformanceEvaluation.IRepository;
using Volo.Abp.Data;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Uow;

namespace Septa.PerformanceEvaluation.IdentityServer
{
    public class OperationEvaluationCategoryDataSeedContributor : IDataSeedContributor, ITransientDependency
    {
        private readonly IOperationEvaluationCategoryRepository _operationEvaluationCategoryRepository;

        public OperationEvaluationCategoryDataSeedContributor(IOperationEvaluationCategoryRepository operationEvaluationCategoryRepository)
        {
            _operationEvaluationCategoryRepository = operationEvaluationCategoryRepository;
        }

        [UnitOfWork]
        public virtual async Task SeedAsync(DataSeedContext context)
        {
            await CreateOperationEvaluationCategory();
        }


        private async Task CreateOperationEvaluationCategory()
        {
            await _operationEvaluationCategoryRepository.InsertAsync(new OperationEvaluationCategory("Funcional", Enums.Gp_OperationEvaluationCategory.Funcional), true);
            await _operationEvaluationCategoryRepository.InsertAsync(new OperationEvaluationCategory("Corporate", Enums.Gp_OperationEvaluationCategory.Corporate),true);
            await _operationEvaluationCategoryRepository.InsertAsync(new OperationEvaluationCategory("Individual", Enums.Gp_OperationEvaluationCategory.Individual),true);
            await _operationEvaluationCategoryRepository.InsertAsync(new OperationEvaluationCategory("Team", Enums.Gp_OperationEvaluationCategory.Team),true);
        }


    }
}
