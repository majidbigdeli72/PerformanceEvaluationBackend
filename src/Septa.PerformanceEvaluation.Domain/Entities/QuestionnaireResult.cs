﻿using System;
using System.Collections.Generic;
using Volo.Abp.Domain.Entities;

namespace Septa.PerformanceEvaluation.Entities
{
    public class QuestionnaireResult : Entity<int>, IOwnEntity<QuestionnaireResult>
    {
        public QuestionnaireResult()
        {

        }
        public QuestionnaireResult(int questionnaireId, DateTime date, Guid filledBy, decimal finalValue)
        {
            QuestionnaireId = questionnaireId;
            Date = date;
            FilledBy = filledBy;
            FinalValue = finalValue;
        }

        public int QuestionnaireId { get; private set; }
        public DateTime Date { get; private set; }
        public Guid FilledBy { get; private set; }
        public decimal FinalValue { get; private set; }
        public virtual Questionnaire Questionnaire { get; private set; }
        public virtual ICollection<QuestionResult> QuestionResults { get; private set; }
        public virtual ICollection<PAResultPIAssessor> PAResultPIAssessors { get; private set; }

        public void UpdateEntity(QuestionnaireResult entity)
        {
            throw new NotImplementedException();
        }
    }
}
