﻿using Volo.Abp.Domain.Entities;

namespace Septa.PerformanceEvaluation.Entities
{
    public class QuestionResult : Entity<int>, IOwnEntity<QuestionResult>
    {
        public QuestionResult()
        {

        }
        public QuestionResult(int questionId, int selectedQuestionItemId, int questionnaireResultId)
        {
            QuestionId = questionId;
            SelectedQuestionItemId = selectedQuestionItemId;
            QuestionnaireResultId = questionnaireResultId;
        }

        public int QuestionId { get; private set; }
        public int SelectedQuestionItemId { get; private set; }
        public int QuestionnaireResultId { get; private set; }
        public virtual Question Question { get; private set; }
        public virtual QuestionItem QuestionItem { get; private set; }
        public virtual QuestionnaireResult QuestionnaireResult { get; private set; }

        public void UpdateEntity(QuestionResult entity)
        {
            throw new System.NotImplementedException();
        }
    }
}
