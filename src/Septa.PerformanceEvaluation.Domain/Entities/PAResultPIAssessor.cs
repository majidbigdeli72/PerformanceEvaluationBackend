﻿using Septa.PerformanceEvaluation.Enums;
using System;
using Volo.Abp.Domain.Entities;

namespace Septa.PerformanceEvaluation.Entities
{
    public class PAResultPIAssessor : Entity<int>, IOwnEntity<PAResultPIAssessor>
    {
        public PAResultPIAssessor()
        {
        }

        public PAResultPIAssessor(int pAResultPIId, Guid assessedById, int? questionnaireResultId, Gp_AssessStatus assessStatusIndex, decimal? value, decimal? normalizedValue, DateTime? assessDate)
        {
            PAResultPIId = pAResultPIId;
            AssessedById = assessedById;
            QuestionnaireResultId = questionnaireResultId;
            AssessStatusIndex = assessStatusIndex;
            Value = value;
            NormalizedValue = normalizedValue;
            AssessDate = assessDate;
        }

        public int PAResultPIId { get; private set; }
        public Guid AssessedById { get; private set; }
        public int? QuestionnaireResultId { get; private set; }
        public Gp_AssessStatus AssessStatusIndex { get; private set; }
        public decimal? Value { get; private set; }
        public decimal? NormalizedValue { get; set; }
        public DateTime? AssessDate { get; private set; }
        public DateTime? UpdateAssessDate { get; set; }
        public virtual PAResultPI PAResultPI { get; private set; }
        public virtual Employee Employee { get; private set; }
        public virtual QuestionnaireResult QuestionnaireResult { get; private set; }


        public void UpdateEntity(PAResultPIAssessor entity)
        {
            throw new NotImplementedException();
        }

        public void UpdateEntity(Gp_AssessStatus assessStatus)
        {
            AssessStatusIndex = assessStatus;
        }

        public void UpdateEntity(Guid assessedById)
        {
            AssessedById = assessedById;
        }


        public PAResultPIAssessor UpdateEntity(PAResultPIAssessor item, int pAResultPIId, Guid assessedById, int? questionnaireResultId, Gp_AssessStatus assessStatusIndex, decimal? value, decimal? normalizeValue, DateTime? assessDate, DateTime? updateAssessDate)
        {
            item.PAResultPIId = pAResultPIId;
            item.AssessedById = assessedById;
            item.QuestionnaireResultId = questionnaireResultId;
            item.AssessStatusIndex = assessStatusIndex;
            item.Value = value;
            item.NormalizedValue = normalizeValue;
            item.AssessDate = assessDate;
            item.UpdateAssessDate = updateAssessDate;
            return item;
        }

    }

}
