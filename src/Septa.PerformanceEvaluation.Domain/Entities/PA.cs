﻿using Septa.PerformanceEvaluation.Enums;
using System;
using System.Collections.Generic;
using Volo.Abp.Domain.Entities;

namespace Septa.PerformanceEvaluation.Entities
{
    public class PA : Entity<int>, IOwnEntity<PA>
    {

        public string Name { get; private set; }
        public DateTime StartDate { get; private set; }
        public DateTime EndDate { get; private set; }
        public GP_PaStatus PaStatusIndex { get; private set; }
        public DateTime? LastExecution { get; private set; }
        public int DaysBeforeDueDateWeekly { get; private set; }
        public int DaysBeforeDueDateMonthly { get; private set; }
        public int DaysBeforeDueDateQuarterly { get; private set; }
        public int DaysBeforeDueDateHalfly { get; private set; }
        public int DaysBeforeDueDateYearly { get; private set; }

        public virtual ICollection<PAOrganizationEntry> PAOrganizationEntrys { get;private set; }

        public void UpdateEntity(PA entity)
        {
            throw new NotImplementedException();
        }

        public PA UpdateEntity(PA entity,GP_PaStatus paStatus,DateTime? lastExecution)
        {
            entity.PaStatusIndex = paStatus;
            entity.LastExecution = lastExecution;
            return entity;
        }
    }
}
