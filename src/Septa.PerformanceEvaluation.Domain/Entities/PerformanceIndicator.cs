﻿using Septa.PerformanceEvaluation.Enums;
using System;
using System.Collections.Generic;
using Volo.Abp.Domain.Entities;

namespace Septa.PerformanceEvaluation.Entities
{
    public class PerformanceIndicator : Entity<int>, IOwnEntity<PerformanceIndicator>
    {
        public string Name { get; private set; }
        public string Description { get;private set; }
        public virtual ICollection<PAOrganizationEntryCategoryPI> PAOrganizationEntryCategoryPIs { get;private set; }
  //      public ICollection<PAResultPI> PAResultPIs { get;private set; }

        public void UpdateEntity(PerformanceIndicator entity)
        {
            throw new System.NotImplementedException();
        }
    }
}
