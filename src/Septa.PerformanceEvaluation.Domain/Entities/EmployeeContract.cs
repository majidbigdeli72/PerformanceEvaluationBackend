﻿using System;
using System.Collections.Generic;
using Volo.Abp.Domain.Entities;

namespace Septa.PerformanceEvaluation.Entities
{
    public class EmployeeContract : Entity<long>, IOwnEntity<EmployeeContract>
    {
        public Guid EmployeeId { get;private set; }
        public int SalaryTypeIndex { get; private set; }
        public DateTime StartDate { get; private set; }
        public DateTime EndDate { get; private set; }
        public int BaseSalary { get; private set; }
        public int? NewYearBaseSalary { get; private set; }
        public int? ExtraSalary { get; private set; }
        public int AcquireSalary { get; private set; }
        public int BonSalary { get; private set; }
        public int MaskanSalary { get; private set; }
        public int ChildSalary { get; private set; }
        public int AccordSalary { get; private set; }
        public int ManagementSalary { get; private set; }
        public int LaunchSalary { get; private set; }
        public int RaftAmadSalary { get; private set; }
        public int? TotalSalary { get; private set; }
        public int DefaultOnTheGoDays { get; private set; }
        public DateTime? InsuranceStartDate { get; private set; }
        public int InsuranceCalculationTypeIndex { get; private set; }
        public int ExtraWorkCalculationTypeIndex { get; private set; }
        public bool DontCareAboutEnterance { get; private set; }
        public string Description { get; private set; }
        public DateTime? LastEditDate { get; private set; }
        public Guid? EditedBy { get; private set; }
        public bool Signed { get; private set; }
        public byte[] SignedFile { get; private set; }
        public int ContractTypeIndex { get; private set; }
        public int WorkingStyleId { get; private set; }
        public virtual Employee Employee { get; private set; }
        public virtual Employee Editor { get; private set; }
        public virtual ICollection<EmployeeContractAuthorize> EmployeeContractAuthorizes { get;private set; }


        public void UpdateEntity(EmployeeContract entity)
        {
            throw new NotImplementedException();
        }
    }


}
