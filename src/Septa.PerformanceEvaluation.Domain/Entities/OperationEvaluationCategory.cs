﻿using Septa.PerformanceEvaluation.Enums;
using System.Collections.Generic;
using Volo.Abp.Domain.Entities;

namespace Septa.PerformanceEvaluation.Entities
{
    public class OperationEvaluationCategory : Entity<int>, IOwnEntity<OperationEvaluationCategory>
    {
        public OperationEvaluationCategory()
        {

        }
        public OperationEvaluationCategory(string name, Gp_OperationEvaluationCategory operationEvaluationCategoryIndex)
        {
            Name = name;
            OperationEvaluationCategoryIndex = operationEvaluationCategoryIndex;
        }

        public string Name { get; private set; }
        public Gp_OperationEvaluationCategory OperationEvaluationCategoryIndex { get; private set; }
        public virtual ICollection<PAOrganizationEntryCategory> PAOrganizationEntryCategories { get; private set; }

        public virtual ICollection<PAResultPI> PAResultPIs { get;private set; }

        public void UpdateEntity(OperationEvaluationCategory entity)
        {
            throw new System.NotImplementedException();
        }
    }
}
