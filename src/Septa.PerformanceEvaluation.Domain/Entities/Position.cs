﻿using System;
using System.Collections.Generic;
using Volo.Abp.Domain.Entities;

namespace Septa.PerformanceEvaluation.Entities
{
    public class Position : Entity<Guid>, IOwnEntity<Position>
    {
        public Guid DepartmentId { get; private set; }
        public int JobLevelIndex { get; private set; }
        public virtual Department Department { get; private set; }
        public virtual OrganizationEntry OrganizationEntry { get; private set; }
        public virtual ICollection<EmployeeContractAuthorize> EmployeeContractAuthorizes { get; private set; }
        public virtual ICollection<PAResultPI> PAResultPIs { get; private set; }


        public void UpdateEntity(Position entity)
        {
            throw new NotImplementedException();
        }
    }
}
