﻿using System;
using System.Collections.Generic;
using Volo.Abp.Domain.Entities;

namespace Septa.PerformanceEvaluation.Entities
{
    public class PAOrganizationEntry : Entity<int>, IOwnEntity<PAOrganizationEntry>
    {
        public Guid OrganizationEntryId { get; private set; }
        public int PAId { get; private set; }
        public virtual OrganizationEntry OrganizationEntry { get; private set; }
        public virtual PA PA { get; private set; }
        public virtual ICollection<PAOrganizationEntryCategory> PAOrganizationEntryCategories { get; private set; }

        public void UpdateEntity(PAOrganizationEntry entity)
        {
            throw new NotImplementedException();
        }
    }




}
