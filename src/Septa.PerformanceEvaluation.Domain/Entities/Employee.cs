﻿using System;
using System.Collections.Generic;
using Volo.Abp.Domain.Entities;

namespace Septa.PerformanceEvaluation.Entities
{
    public class Employee : Entity<Guid>, IOwnEntity<Employee>
    {
        public int SbuId { get; private set; }
        public string EmployeeCode { get; private set; }
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public int Sex { get; private set; }
        public DateTime? BirthDate { get; private set; }
        public string NationalCode { get; private set; }
        public string Tel { get; private set; }
        public string Address { get; private set; }
        public string UserName { get; private set; }
        public string ADUsername { get; private set; }
        public string Email { get; private set; }
        public int EmployeeStatusIndex { get; private set; }
        public int? InsuranceNo { get; private set; }
        public double? OldAvailableLeaveDay { get; private set; }
        public Guid? PicFileId { get; private set; }

        public virtual OrganizationEntry OrganizationEntry { get; private set; }
        public virtual ICollection<EmployeeContract> EmployeeContracts { get; private set; }
        public virtual ICollection<EmployeeContract> EditedEmployeeContracts { get; private set; }
        public virtual ICollection<PAResultPI> PAResultPIs { get; private set; }
        public virtual ICollection<PAResultPIAssessor> PAResultPIAssessors { get; private set; }

        public void UpdateEntity(Employee entity)
        {
            throw new NotImplementedException();
        }
    }
}
