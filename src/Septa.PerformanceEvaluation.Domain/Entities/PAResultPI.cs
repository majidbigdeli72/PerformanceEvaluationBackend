﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Volo.Abp.Domain.Entities;

namespace Septa.PerformanceEvaluation.Entities
{
    public class PAResultPI : Entity<int>, IOwnEntity<PAResultPI>
    {

        public PAResultPI()
        {

        }

        public PAResultPI(int pAId,int? parentId, int pAOrganizationEntryCategoryPIId, DateTime createDate, int iterationNumber, bool isGroupingEvaluation, Guid? employeeId, int? operationEvaluationCategoryId, decimal? operationEvaluationCategoryWeight, Guid? positionId,DateTime relateDate)
        {
            PAId = pAId;
            ParentId = parentId;
            PAOrganizationEntryCategoryPIId = pAOrganizationEntryCategoryPIId;
            CreateDate = createDate;
            IterationNumber = iterationNumber;
            IsGroupingEvaluation = isGroupingEvaluation;
            EmployeeId = employeeId;
            OperationEvaluationCategoryId = operationEvaluationCategoryId;
            OperationEvaluationCategoryWeight = operationEvaluationCategoryWeight;
            PositionId = positionId;
            RelatedDate = relateDate;
        }

        public int PAId { get;private set; }
        public int? ParentId { get;private set; }
        public int PAOrganizationEntryCategoryPIId { get;private set; }
        public DateTime CreateDate { get; private set; }
        public int IterationNumber { get; private set; }
        public bool IsGroupingEvaluation { get;private set; }
        public Guid? EmployeeId { get; private set; }
        public int? OperationEvaluationCategoryId { get;private set; }
        public decimal? OperationEvaluationCategoryWeight { get;private set; }
        public Guid? PositionId { get;private set; }
        public DateTime RelatedDate { get; set; }
        public virtual Employee Employee { get; private set; }
        public virtual OperationEvaluationCategory OperationEvaluationCategory { get;private set; }
        public virtual Position Position { get;private set; }
        public virtual PAOrganizationEntryCategoryPI PAOrganizationEntryCategoryPI { get; set; }
        public virtual ICollection<PAResultPIAssessor> PAResultPIAssessors { get; private set; }


        public void UpdateEntity(PAResultPI entity)
        {
            throw new NotImplementedException();
        }

        public void UpdateEntity(Guid employeeId)
        {
            EmployeeId = employeeId;
        }

    }
}
