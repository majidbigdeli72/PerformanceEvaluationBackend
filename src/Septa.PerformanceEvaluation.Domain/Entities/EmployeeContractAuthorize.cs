﻿using System;
using Volo.Abp.Domain.Entities;

namespace Septa.PerformanceEvaluation.Entities
{
    public class EmployeeContractAuthorize : Entity<long>, IOwnEntity<EmployeeContractAuthorize>
    {
        public long EmployeeContractId { get; private set; }
        public Guid DepartmentId { get;private set; }
        public Guid? PositionId { get;private set; }
        public bool IsMain { get; set; }
        public virtual EmployeeContract EmployeeContract { get;private set; }
        public virtual Department Department { get;private set; }
        public virtual Position Position { get;private set; }

        public void UpdateEntity(EmployeeContractAuthorize entity)
        {
            throw new NotImplementedException();
        }
    }


}
