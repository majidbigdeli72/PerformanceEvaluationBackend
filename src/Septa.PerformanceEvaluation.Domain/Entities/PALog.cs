﻿using Septa.PerformanceEvaluation.Enums;
using System;
using Volo.Abp.Domain.Entities;

namespace Septa.PerformanceEvaluation.Entities
{
    public class PALog : Entity<int>, IOwnEntity<PALog>
    {
        public PALog()
        {

        }
        public PALog(int pAId, int? pAOrganizationEntryId, int? pAOrganizationEntryCategoryId, int? pAOrganizationEntryCategoryPIId, int? pAResultPIId, Gp_PAErrorType pAErrorTypeIndex, string exception ,DateTime relatedDate)
        {
            PAId = pAId;
            PAOrganizationEntryId = pAOrganizationEntryId;
            PAOrganizationEntryCategoryId = pAOrganizationEntryCategoryId;
            PAOrganizationEntryCategoryPIId = pAOrganizationEntryCategoryPIId;
            PAResultPIId = pAResultPIId;
            PAErrorTypeIndex = pAErrorTypeIndex;
            Exception = exception;
            RelatedDate = relatedDate;
        }

        public int PAId { get;private set; }
        public int? PAOrganizationEntryId { get;private set; }
        public int? PAOrganizationEntryCategoryId { get;private set; }
        public int? PAOrganizationEntryCategoryPIId { get;private set; }
        public int? PAResultPIId { get;private set; }
        public Gp_PAErrorType PAErrorTypeIndex { get;private set; }
        public string Exception { get;private set; }
        public DateTime RelatedDate { get; private set; }

        public void UpdateEntity(PALog entity)
        {
            throw new NotImplementedException();
        }
    }
}
