﻿using Septa.PerformanceEvaluation.Enums;
using System;
using System.Collections.Generic;
using Volo.Abp.Domain.Entities;

namespace Septa.PerformanceEvaluation.Entities
{
    public class OrganizationEntry : Entity<Guid>, IOwnEntity<OrganizationEntry>
    {
        public Guid? ParentId { get; private set; }
        public Gp_OrganizationEntry OrganizatuinEntryTypeIndex { get; private set; }
        public string Name { get; private set; }
        public string Description { get; private set; }
        public virtual Department Department { get; private set; }
        public virtual Position Position { get; private set; }
        public virtual Employee Employee { get; private set; }
        public virtual ICollection<PAOrganizationEntryCategoryPIAssessor> PAOrganizationEntryCategoryPIAssessors { get; private set; }

        public virtual ICollection<PAOrganizationEntry> PAOrganizationEntrys { get; private set; }
      //  public ICollection<PAResultPI> PAResultPIs { get; private set; }

        public virtual OrganizationEntry Parent { get; private set; }
        public virtual ICollection<OrganizationEntry> Childrens { get; private set; }

        public void UpdateEntity(OrganizationEntry entity)
        {
            throw new NotImplementedException();
        }
    }
}
