﻿using System.Collections.Generic;
using Volo.Abp.Domain.Entities;

namespace Septa.PerformanceEvaluation.Entities
{
    public class Questionnaire : Entity<int>, IOwnEntity<Questionnaire>
    {
        public Questionnaire()
        {

        }
        public Questionnaire(int id)
        {
            Id = id;
        }
        public string Name { get; set; }
        public string Description { get; set; }
        public virtual ICollection<Question> Questions { get; set; }
        public virtual ICollection<QuestionnaireResult> QuestionnaireResults { get; set; }
        public virtual ICollection<PAOrganizationEntryCategoryPI> PAOrganizationEntryCategoryPIs { get;set; }

        public void UpdateEntity(Questionnaire questionnaire)
        {
            Name = questionnaire.Name;
            Description = questionnaire.Description;
            Questions = questionnaire.Questions;
        }

        public Questionnaire UpdateEntity(Questionnaire questionnaire,string name , string description, List<Question> questions)
        {
            questionnaire.Name = name;
            questionnaire.Description = description;
            questionnaire.Questions = questions;
            return questionnaire;
        }

    }
}
