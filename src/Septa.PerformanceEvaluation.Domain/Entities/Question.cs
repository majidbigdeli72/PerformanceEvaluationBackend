﻿using System.Collections.Generic;
using Volo.Abp.Domain.Entities;

namespace Septa.PerformanceEvaluation.Entities
{
    public class Question : Entity<int>, IOwnEntity<Question>
    {
        public Question()
        {

        }
        public Question(int id)
        {
            Id = id;
        }
        public int QuestionnaireId { get;  set; }
        public string Content { get;  set; }
        public decimal Weight { get;  set; }
        public virtual Questionnaire Questionnaire { get;  set; }
        public virtual ICollection<QuestionItem> QuestionItems { get;  set; }
        public virtual ICollection<QuestionResult> QuestionResults { get;  set; }

        public void UpdateEntity(Question entity)
        {
            throw new System.NotImplementedException();
        }
    }
}
