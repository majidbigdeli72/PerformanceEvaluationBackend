﻿using Septa.PerformanceEvaluation.Enums;
using System;
using Volo.Abp.Domain.Entities;

namespace Septa.PerformanceEvaluation.Entities
{
    public class PAOrganizationEntryCategoryPIAssessor : Entity<int>, IOwnEntity<PAOrganizationEntryCategoryPIAssessor>
    {
        public int PAOrganizationEntryCategoryPIId { get; private set; }
        public Gp_AssessorType AssessorTypeIndex { get; private set; }
        public Guid? CustomAssessorId { get; private set; }
        public virtual OrganizationEntry OrganizationEntry { get; private set; }
        public virtual PAOrganizationEntryCategoryPI PAOrganizationEntryCategoryPI { get; private set; }

        public void UpdateEntity(PAOrganizationEntryCategoryPIAssessor entity)
        {
            throw new NotImplementedException();
        }
    }
}
