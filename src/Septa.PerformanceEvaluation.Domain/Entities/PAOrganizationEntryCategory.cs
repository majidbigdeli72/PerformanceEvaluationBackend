﻿using System;
using System.Collections;
using System.Collections.Generic;
using Volo.Abp.Domain.Entities;

namespace Septa.PerformanceEvaluation.Entities
{
    public class PAOrganizationEntryCategory : Entity<int>, IOwnEntity<PAOrganizationEntryCategory>
    {
        public int PAOrganizationEntryId { get; private set; }

        public int OperationEvaluationCategoryId { get; private set; }

        public decimal Weight { get; private set; }

        public virtual PAOrganizationEntry PAOrganizationEntry { get; private set; }

        public virtual OperationEvaluationCategory OperationEvaluationCategory { get; private set; }

        public virtual ICollection<PAOrganizationEntryCategoryPI> PAOrganizationEntryCategoryPIs { get;private set; }

        public void UpdateEntity(PAOrganizationEntryCategory entity)
        {
            throw new NotImplementedException();
        }
    }




}
