﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Domain.Entities;

namespace Septa.PerformanceEvaluation.Entities
{
    public class Department : Entity<Guid>, IOwnEntity<Department>
    {
        public virtual OrganizationEntry OrganizationEntry { get; private set; }
        public virtual ICollection<Position> Positions { get; private set; }
        public virtual ICollection<EmployeeContractAuthorize> EmployeeContractAuthorizes { get; private set; }



        public void UpdateEntity(Department entity)
        {
            throw new NotImplementedException();
        }
    }




}
