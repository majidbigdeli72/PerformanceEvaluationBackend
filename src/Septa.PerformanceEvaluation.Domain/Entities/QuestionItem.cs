﻿using System.Collections.Generic;
using Volo.Abp.Domain.Entities;

namespace Septa.PerformanceEvaluation.Entities
{
    public class QuestionItem : Entity<int>, IOwnEntity<QuestionItem>
    {
        public QuestionItem()
        {

        }
        public QuestionItem(int id)
        {
            Id = id;
        }
        public int QuestionId { get;  set; }
        public string Name { get;  set; }
        public decimal Value { get;  set; }
        public virtual Question Question { get; set; }
        public virtual ICollection<QuestionResult> QuestionResults { get; set; }

        public void UpdateEntity(QuestionItem entity)
        {
            throw new System.NotImplementedException();
        }
    }
}
