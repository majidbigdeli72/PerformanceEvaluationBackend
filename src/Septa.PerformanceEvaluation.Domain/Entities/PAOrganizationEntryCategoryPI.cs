﻿using Septa.PerformanceEvaluation.Enums;
using System;
using System.Collections.Generic;
using Volo.Abp.Domain.Entities;

namespace Septa.PerformanceEvaluation.Entities
{
    public class PAOrganizationEntryCategoryPI : Entity<int>, IOwnEntity<PAOrganizationEntryCategoryPI>
    {
        public int PAOrganizationEntryCategoryId { get; private set; }
        public int PerformanceIndicatorId { get; private set; }
        public int? QuestionnaireId { get; private set; }
        public decimal Weight { get; private set; }
        public Gp_EvaluationPeriod EvaluationPeriodIndex { get; private set; }
        public Gp_EvaluationType EvaluationTypeIndex { get; private set; }
        public Gp_CalculationType CalculationTypeIndex { get;private set; }
        public decimal Min { get;private set; }
        public decimal Max { get;private set; }
        public bool IsGroupingEvaluation { get;private set; }
        public virtual PAOrganizationEntryCategory PAOrganizationEntryCategory { get; private set; }
        public virtual PerformanceIndicator PerformanceIndicator { get; private set; }
        public virtual Questionnaire Questionnaire { get; private set; }
        public virtual ICollection<PAOrganizationEntryCategoryPIAssessor> PAOrganizationEntryCategoryPIAssessors { get;private set; }
        public virtual ICollection<PAResultPI> PAResultPIs { get; set; }
        public void UpdateEntity(PAOrganizationEntryCategoryPI entity)
        {
            throw new NotImplementedException();
        }
    }




}
