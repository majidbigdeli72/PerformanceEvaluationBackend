﻿using Microsoft.EntityFrameworkCore;
using Septa.PerformanceEvaluation.Entities;
using Septa.PerformanceEvaluation.EntityFrameworkCore;
using Septa.PerformanceEvaluation.Enums;
using Septa.PerformanceEvaluation.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;

namespace Septa.PerformanceEvaluation.Repository
{
    public class PARepository : EfCoreRepository<PerformanceEvaluationDbContext, PA, int>, IPARepository
    {
        public PARepository(IDbContextProvider<PerformanceEvaluationDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }
        public async Task<List<PA>> GetPAs(DateTime dateTime)
        {
            return await DbContext.Set<PA>()
                .Include(x => x.PAOrganizationEntrys)
                    .ThenInclude(x => x.OrganizationEntry)
                    .ThenInclude(x => x.Position)
                    .ThenInclude(x => x.EmployeeContractAuthorizes)
                    .ThenInclude(x => x.EmployeeContract)
                    .ThenInclude(x => x.Employee)
                .Include(x => x.PAOrganizationEntrys)
                    .ThenInclude(x => x.OrganizationEntry)
                    .ThenInclude(x => x.Department)
                    .ThenInclude(x => x.EmployeeContractAuthorizes)
                    .ThenInclude(x => x.EmployeeContract)
                .Include(x => x.PAOrganizationEntrys)
                    .ThenInclude(x => x.PAOrganizationEntryCategories)
                    .ThenInclude(x => x.PAOrganizationEntryCategoryPIs)
                    .ThenInclude(x => x.PAOrganizationEntryCategoryPIAssessors)
                .Where(x => x.StartDate <= dateTime.Date && x.EndDate >= dateTime.Date && (x.PaStatusIndex == GP_PaStatus.ReadyToRun || x.PaStatusIndex == GP_PaStatus.InProgress))
                .ToListAsync();
        }
    }
}
