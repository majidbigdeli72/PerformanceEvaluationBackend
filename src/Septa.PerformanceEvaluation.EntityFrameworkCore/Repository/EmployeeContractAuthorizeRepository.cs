﻿using Microsoft.EntityFrameworkCore;
using Septa.PerformanceEvaluation.Entities;
using Septa.PerformanceEvaluation.EntityFrameworkCore;
using Septa.PerformanceEvaluation.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;

namespace Septa.PerformanceEvaluation.Repository
{
    public class EmployeeContractAuthorizeRepository : EfCoreRepository<PerformanceEvaluationDbContext, EmployeeContractAuthorize, long>, IEmployeeContractAuthorizeRepository
    {
        public EmployeeContractAuthorizeRepository(IDbContextProvider<PerformanceEvaluationDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }

        public async Task<List<EmployeeContractAuthorize>> GetEmployeeContractAuthorizeAsync(long employeeContractId)
        {
            return await DbContext.Set<EmployeeContractAuthorize>().Include(x => x.EmployeeContract).Include(x => x.Department).Include(x => x.Position)
                  .Where(x => x.EmployeeContractId == employeeContractId && x.Position != null).ToListAsync();
        }

        public async Task<EmployeeContractAuthorize> GetCurrentSalariesAsync(Guid positionId, DateTime dateTime)
        {
            return await DbContext.Set<EmployeeContractAuthorize>().Include(x => x.EmployeeContract)
                .FirstOrDefaultAsync(x => x.PositionId == positionId && x.EmployeeContract.StartDate <= dateTime && x.EmployeeContract.EndDate >= dateTime);
        }
    }
}
