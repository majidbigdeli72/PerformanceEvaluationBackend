﻿using Microsoft.EntityFrameworkCore;
using Septa.PerformanceEvaluation.Entities;
using Septa.PerformanceEvaluation.EntityFrameworkCore;
using Septa.PerformanceEvaluation.IRepository;
using Septa.PerformanceEvaluation.LinqView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;

namespace Septa.PerformanceEvaluation.Repository
{
    public class PAResultPIAssessorRepository : EfCoreRepository<PerformanceEvaluationDbContext, PAResultPIAssessor, int>, IPAResultPIAssessorRepository
    {
        public PAResultPIAssessorRepository(IDbContextProvider<PerformanceEvaluationDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }
        public async Task<List<LVwPAResultPIAssesor>> GetListPAResultPIAssessorByAssessorIdAsync(Guid assessorId)
        {

            return await (from pAResultPIAssessor in DbContext.PAResultPIAssessor
                          join pAResultPI in DbContext.PAResultPI on pAResultPIAssessor.PAResultPIId equals pAResultPI.Id into p
                          from pAResultPI in p.DefaultIfEmpty()
                          join employee in DbContext.Employee on pAResultPI.EmployeeId equals employee.Id into e
                          from employee in e.DefaultIfEmpty()
                          join pAOrganizationEntryCategoryPI in DbContext.PAOrganizationEntryCategoryPI on pAResultPI.PAOrganizationEntryCategoryPIId equals pAOrganizationEntryCategoryPI.Id into y
                          from pAOrganizationEntryCategoryPI in y.DefaultIfEmpty()
                          join performanceIndicator in DbContext.PerformanceIndicator on pAOrganizationEntryCategoryPI.PerformanceIndicatorId equals performanceIndicator.Id into k
                          from performanceIndicator in k.DefaultIfEmpty()
                          join questionnaire in DbContext.Questionnaire on pAOrganizationEntryCategoryPI.QuestionnaireId equals questionnaire.Id into j
                          from questionnaire in j.DefaultIfEmpty()
                          join pAOrganizationEntryCategory in DbContext.PAOrganizationEntryCategory on pAOrganizationEntryCategoryPI.PAOrganizationEntryCategoryId equals pAOrganizationEntryCategory.Id into o
                          from pAOrganizationEntryCategory in o.DefaultIfEmpty()
                          join operationEvaluationCategory in DbContext.OperationEvaluationCategory on pAOrganizationEntryCategory.OperationEvaluationCategoryId equals operationEvaluationCategory.Id into i
                          from operationEvaluationCategory in i.DefaultIfEmpty()
                          join pAOrganizationEntry in DbContext.PAOrganizationEntry on pAOrganizationEntryCategory.PAOrganizationEntryId equals pAOrganizationEntry.Id into t
                          from pAOrganizationEntry in t.DefaultIfEmpty()
                          join pA in DbContext.PA on pAOrganizationEntry.PAId equals pA.Id into z
                          from pA in z.DefaultIfEmpty()
                          join organizationEntry in DbContext.OrganizationEntry on pAOrganizationEntry.OrganizationEntryId equals organizationEntry.Id into l
                          from organizationEntry in l.DefaultIfEmpty()
                          where pAResultPIAssessor.AssessedById == assessorId && pAResultPI.ParentId == null
                          select new LVwPAResultPIAssesor(
                              pAResultPIAssessor.Id,
                              pAResultPIAssessor.AssessStatusIndex,
                              employee.FirstName,
                              employee.LastName,
                              pAResultPI.IterationNumber,
                              organizationEntry.Name,
                              pA.Name,
                              performanceIndicator.Name,
                              questionnaire.Id,
                              operationEvaluationCategory.Name,
                              pAOrganizationEntryCategoryPI.EvaluationTypeIndex,
                              pAOrganizationEntryCategoryPI.CalculationTypeIndex,
                              pAOrganizationEntryCategoryPI.Id,
                              pAResultPI.Id,
                              pAResultPIAssessor.AssessDate,
                              pAResultPIAssessor.QuestionnaireResultId,
                              pAResultPIAssessor.Value)
                          ).ToListAsync();


        }
    }
}
