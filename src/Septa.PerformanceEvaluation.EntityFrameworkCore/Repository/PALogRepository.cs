﻿using Septa.PerformanceEvaluation.Entities;
using Septa.PerformanceEvaluation.EntityFrameworkCore;
using Septa.PerformanceEvaluation.IRepository;
using Septa.PerformanceEvaluation.LinqView;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System.Linq;


namespace Septa.PerformanceEvaluation.Repository
{
    public class PALogRepository : EfCoreRepository<PerformanceEvaluationDbContext, PALog, int>, IPALogRepository
    {
        public PALogRepository(IDbContextProvider<PerformanceEvaluationDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }

        public async Task<List<LVwPaLog>> GetPALogsAsync() => await (from pALog in DbContext.PALog
                                                                join pAOrganizationEntryCategoryPI in DbContext.PAOrganizationEntryCategoryPI on pALog.PAOrganizationEntryCategoryPIId equals pAOrganizationEntryCategoryPI.Id into j
                                                                from pAOrganizationEntryCategoryPI in j.DefaultIfEmpty()
                                                                join performanceIndicator in DbContext.PerformanceIndicator on pAOrganizationEntryCategoryPI.PerformanceIndicatorId equals performanceIndicator.Id into f
                                                                from performanceIndicator in f.DefaultIfEmpty()
                                                                join pAOrganizationEntryCategory in DbContext.PAOrganizationEntryCategory on pAOrganizationEntryCategoryPI.PAOrganizationEntryCategoryId equals pAOrganizationEntryCategory.Id into t
                                                                from pAOrganizationEntryCategory in t.DefaultIfEmpty()
                                                                join pAOrganizationEntry in DbContext.PAOrganizationEntry on pAOrganizationEntryCategory.PAOrganizationEntryId equals pAOrganizationEntry.Id into r
                                                                from pAOrganizationEntry in r.DefaultIfEmpty()
                                                                join pA in DbContext.PA on pAOrganizationEntry.PAId equals pA.Id into h
                                                                from pA in h.DefaultIfEmpty()
                                                                join organizationEntry in DbContext.OrganizationEntry on pAOrganizationEntry.OrganizationEntryId equals organizationEntry.Id into y
                                                                from organizationEntry in y.DefaultIfEmpty()
                                                                orderby pALog.Id descending
                                                                select new LVwPaLog(pALog.Exception, pALog.PAErrorTypeIndex, pALog.RelatedDate, pA.Name, performanceIndicator.Name, organizationEntry.Name)
                                                                ).ToListAsync();
    }

}
