﻿using Microsoft.EntityFrameworkCore;
using Septa.PerformanceEvaluation.Entities;
using Septa.PerformanceEvaluation.EntityFrameworkCore;
using Septa.PerformanceEvaluation.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;

namespace Septa.PerformanceEvaluation.Repository
{
    public class EmployeeRepository : EfCoreRepository<PerformanceEvaluationDbContext, OrganizationEntry, Guid>, IEmployeeRepository
    {
        public EmployeeRepository(IDbContextProvider<PerformanceEvaluationDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }

        public async Task<Employee> GetEmployeeByUserNameAsync(string userName)
        {
           return await DbContext.Set<Employee>().AsQueryable().FirstOrDefaultAsync(x => x.UserName.Equals(userName));
        }

        public async Task<List<OrganizationEntry>> GetEmployeListAsync()
        {
            return await DbContext.Set<OrganizationEntry>().Include(x => x.Employee).Where(x => x.OrganizatuinEntryTypeIndex == Enums.Gp_OrganizationEntry.Employee).ToListAsync();
        }
    }
}
