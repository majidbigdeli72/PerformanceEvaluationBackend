﻿using Septa.PerformanceEvaluation.Entities;
using Septa.PerformanceEvaluation.EntityFrameworkCore;
using Septa.PerformanceEvaluation.IRepository;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;

namespace Septa.PerformanceEvaluation.Repository
{
    public class OperationEvaluationCategoryRepository : EfCoreRepository<PerformanceEvaluationDbContext, OperationEvaluationCategory, int>, IOperationEvaluationCategoryRepository
    {
        public OperationEvaluationCategoryRepository(IDbContextProvider<PerformanceEvaluationDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }
    }

}
