﻿using Microsoft.EntityFrameworkCore;
using Septa.PerformanceEvaluation.DataBaseView;
using Septa.PerformanceEvaluation.Entities;
using Septa.PerformanceEvaluation.EntityFrameworkCore;
using Septa.PerformanceEvaluation.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;

namespace Septa.PerformanceEvaluation.Repository
{
    public class PositionRepository : EfCoreRepository<PerformanceEvaluationDbContext, OrganizationEntry, Guid>, IPositionRepository
    {
        public PositionRepository(IDbContextProvider<PerformanceEvaluationDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }

        public async Task<List<VwPosition>> GetPositionAsync()
        {
            return await DbContext.Set<VwPosition>().OrderBy(x => x.Path2Root).ToListAsync();
        }
    }
}
