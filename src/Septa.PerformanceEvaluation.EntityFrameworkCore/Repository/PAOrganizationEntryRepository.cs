﻿using Septa.PerformanceEvaluation.Entities;
using Septa.PerformanceEvaluation.EntityFrameworkCore;
using Septa.PerformanceEvaluation.IRepository;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;

namespace Septa.PerformanceEvaluation.Repository
{
    public class PAOrganizationEntryRepository : EfCoreRepository<PerformanceEvaluationDbContext, PAOrganizationEntry, int>, IPAOrganizationEntryRepository
    {
        public PAOrganizationEntryRepository(IDbContextProvider<PerformanceEvaluationDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }
    }
   
}
