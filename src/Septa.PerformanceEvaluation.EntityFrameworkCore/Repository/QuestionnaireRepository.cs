﻿using Microsoft.EntityFrameworkCore;
using Septa.PerformanceEvaluation.Entities;
using Septa.PerformanceEvaluation.EntityFrameworkCore;
using Septa.PerformanceEvaluation.IRepository;
using System.Threading;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;

namespace Septa.PerformanceEvaluation.Repository
{
    public class QuestionnaireRepository : EfCoreRepository<PerformanceEvaluationDbContext, Questionnaire, int>, IQuestionnaireRepository
    {
        public QuestionnaireRepository(IDbContextProvider<PerformanceEvaluationDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }

        public override async Task<Questionnaire> FindAsync(int id, bool includeDetails = true, CancellationToken cancellationToken = default)
        {
            if (includeDetails)
            {
                return await DbContext.Set<Questionnaire>().Include(x => x.Questions).ThenInclude(x => x.QuestionItems).FirstOrDefaultAsync(x => x.Id == id);
            }

            return await base.FindAsync(id, includeDetails, cancellationToken);

        }

        public async Task<Questionnaire> FindQuestionnaireAsync(int questionnaireId)
        {
            return await DbContext.Set<Questionnaire>().Include(x => x.Questions).ThenInclude(x => x.QuestionItems).FirstOrDefaultAsync(x => x.Id == questionnaireId);
        }

        public async Task<int> InsertQuestionnaireAsync(Questionnaire questionnaire)
        {
            DbContext.Set<Questionnaire>().Add(questionnaire);
            await DbContext.SaveChangesAsync();
            return questionnaire.Id;
        }

        public async Task UpdateQuestionnaireAsync(Questionnaire model)
        {
            DbContext.Set<Questionnaire>().Update(model);
            await DbContext.SaveChangesAsync();
        }
    }

}
