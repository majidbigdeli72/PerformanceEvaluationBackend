﻿using Microsoft.EntityFrameworkCore;
using Septa.PerformanceEvaluation.Entities;
using Septa.PerformanceEvaluation.EntityFrameworkCore;
using Septa.PerformanceEvaluation.IRepository;
using Septa.PerformanceEvaluation.LinqView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;

namespace Septa.PerformanceEvaluation.Repository
{
    public class QuestionResultRepository : EfCoreRepository<PerformanceEvaluationDbContext, QuestionResult, int>, IQuestionResultRepository
    {
        public QuestionResultRepository(IDbContextProvider<PerformanceEvaluationDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }

        public async Task<List<LVwMinMaxQuestionItem>> GetMinMaxQuestionItemsAsync(int questionnaireId)
        {
            return await (from questionnaire in DbContext.Questionnaire
                          join question in DbContext.Question on questionnaire.Id equals question.QuestionnaireId
                          join questionItem in DbContext.QuestionItem on question.Id equals questionItem.QuestionId
                          where questionnaire.Id == questionnaireId
                          group questionItem by questionItem.QuestionId into g
                          select new LVwMinMaxQuestionItem(g.Min(q => q.Value), g.Max(q => q.Value), g.Key)
                      ).ToListAsync();

        }

        public async Task<List<LVwQuestionResult>> GetQuestionResultAsync(int questionnaireResultId)
        {
            return await (from questionResult in DbContext.QuestionResult
                          where questionResult.QuestionnaireResultId == questionnaireResultId
                          select new LVwQuestionResult(questionResult.QuestionId, questionResult.SelectedQuestionItemId, questionResult.QuestionnaireResultId)
                         ).ToListAsync();

        }
    }
}
