﻿using Microsoft.EntityFrameworkCore;
using Septa.PerformanceEvaluation.DataBaseView;
using Septa.PerformanceEvaluation.Entities;
using System;
using Volo.Abp.Data;
using Volo.Abp.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore.Modeling;
using Volo.Abp.Users.EntityFrameworkCore;

namespace Septa.PerformanceEvaluation.EntityFrameworkCore
{
    /* This is your actual DbContext used on runtime.
     * It includes only your entities.
     * It does not include entities of the used modules, because each module has already
     * its own DbContext class. If you want to share some database tables with the used modules,
     * just create a structure like done for AppUser.
     *
     * Don't use this DbContext for database migrations since it does not contain tables of the
     * used modules (as explained above). See PerformanceEvaluationMigrationsDbContext for migrations.
     */
    [ConnectionStringName("Default")]
    public class PerformanceEvaluationDbContext : AbpDbContext<PerformanceEvaluationDbContext>
    {
        //public DbSet<AppUser> Users { get; set; }

        /* Add DbSet properties for your Aggregate Roots / Entities here.
         * Also map them inside PerformanceEvaluationDbContextModelCreatingExtensions.ConfigurePerformanceEvaluation
         */

        public DbSet<AppUser> AppUsers { get; set; }
        public DbSet<Department> Department { get; set; }
        public DbSet<Employee> Employee { get; set; }
        public DbSet<EmployeeContract> EmployeeContract { get; set; }
        public DbSet<EmployeeContractAuthorize> EmployeeContractAuthorize { get; set; }
        public DbSet<PAOrganizationEntry> PAOrganizationEntry { get; set; }
        public DbSet<PAOrganizationEntryCategory> PAOrganizationEntryCategory { get; set; }
        public DbSet<PAOrganizationEntryCategoryPI> PAOrganizationEntryCategoryPI { get; set; }
        public DbSet<PAOrganizationEntryCategoryPIAssessor> PAOrganizationEntryCategoryPIAssessor { get; set; }
        public DbSet<PAResultPI> PAResultPI { get; set; }
        public DbSet<PAResultPIAssessor> PAResultPIAssessor { get; set; }
        public DbSet<OperationEvaluationCategory> OperationEvaluationCategory { get; set; }
        public DbSet<PA> PA { get; set; }
        public DbSet<PALog> PALog { get; set; }
        public DbSet<OrganizationEntry> OrganizationEntry { get; set; }
        public DbSet<PerformanceIndicator> PerformanceIndicator { get; set; }
        public DbSet<Position> Position { get; set; }
        public DbSet<Question> Question { get; set; }
        public DbSet<QuestionItem> QuestionItem { get; set; }
        public DbSet<Questionnaire> Questionnaire { get; set; }
        public DbSet<QuestionnaireResult> QuestionnaireResult { get; set; }
        public DbSet<QuestionResult> QuestionResult { get; set; }

        //View
        public DbSet<VwDepartment> VwDepartment { get; set; }
        public DbSet<VwPosition> VwPosition { get; set; }        

        public PerformanceEvaluationDbContext(DbContextOptions<PerformanceEvaluationDbContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<AppUser>(b =>
            {
                b.ToTable(PerformanceEvaluationConsts.DbTablePrefix + "Users"); //Sharing the same table "AbpUsers" with the IdentityUser
                b.ConfigureByConvention();
                b.ConfigureAbpUser();

                //Moved customization to a method so we can share it with the jjjMigrationsDbContext class
                b.ConfigureCustomUserProperties();
                b.ConfigureExtraProperties();

            });

            builder.ConfigurePerformanceEvaluation();
        }
    }
}
