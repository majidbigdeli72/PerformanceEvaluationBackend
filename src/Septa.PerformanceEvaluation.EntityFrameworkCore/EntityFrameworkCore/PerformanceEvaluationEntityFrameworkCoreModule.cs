﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Septa.PerformanceEvaluation.Entities;
using System.Linq;
using Volo.Abp.AuditLogging.EntityFrameworkCore;
using Volo.Abp.BackgroundJobs.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore.SqlServer;
using Volo.Abp.FeatureManagement.EntityFrameworkCore;
using Volo.Abp.Identity.EntityFrameworkCore;
using Volo.Abp.IdentityServer.EntityFrameworkCore;
using Volo.Abp.Modularity;
using Volo.Abp.PermissionManagement.EntityFrameworkCore;
using Volo.Abp.SettingManagement.EntityFrameworkCore;
using Volo.Abp.TenantManagement.EntityFrameworkCore;

namespace Septa.PerformanceEvaluation.EntityFrameworkCore
{
    [DependsOn(
        typeof(PerformanceEvaluationDomainModule),
        typeof(AbpIdentityEntityFrameworkCoreModule),
        typeof(AbpIdentityServerEntityFrameworkCoreModule),
        typeof(AbpPermissionManagementEntityFrameworkCoreModule),
        typeof(AbpSettingManagementEntityFrameworkCoreModule),
        typeof(AbpEntityFrameworkCoreSqlServerModule),
        typeof(AbpBackgroundJobsEntityFrameworkCoreModule),
        typeof(AbpAuditLoggingEntityFrameworkCoreModule),
        typeof(AbpTenantManagementEntityFrameworkCoreModule),
        typeof(AbpFeatureManagementEntityFrameworkCoreModule)
        )]
    public class PerformanceEvaluationEntityFrameworkCoreModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddAbpDbContext<PerformanceEvaluationDbContext>(options =>
            {
                /* Remove "includeAllEntities: true" to create
                 * default repositories only for aggregate roots */
                options.AddDefaultRepositories(includeAllEntities: true);

                options.Entity<Questionnaire>(opt =>
                {
                    opt.DefaultWithDetailsFunc = q => q.Include(x => x.Questions).ThenInclude(x => x.QuestionItems);
                });

                options.Entity<OrganizationEntry>(opt =>
                {
                    opt.DefaultWithDetailsFunc = q => q.Include(x => x.Department).Include(x => x.Position).Include(x => x.Employee);
                });

                options.Entity<PA>(opt =>
                {
                    opt.DefaultWithDetailsFunc = q => q.Include(x => x.PAOrganizationEntrys).ThenInclude(x => x.OrganizationEntry);
                });

                options.Entity<PAOrganizationEntry>(opt =>
                {
                    opt.DefaultWithDetailsFunc = q => q
                    .Include(x => x.PAOrganizationEntryCategories)
                    .ThenInclude(x => x.OperationEvaluationCategory)
                    .Include(x => x.PAOrganizationEntryCategories)
                    .ThenInclude(x => x.PAOrganizationEntryCategoryPIs)
                    .ThenInclude(x => x.Questionnaire)
                    .Include(x => x.PAOrganizationEntryCategories)
                    .ThenInclude(x => x.OperationEvaluationCategory)
                    .Include(x => x.PAOrganizationEntryCategories)
                    .ThenInclude(x => x.PAOrganizationEntryCategoryPIs)
                    .ThenInclude(x => x.PerformanceIndicator);
                });

                options.Entity<PAOrganizationEntryCategory>(opt =>
                {
                    opt.DefaultWithDetailsFunc = q => q
                    .Include(x => x.OperationEvaluationCategory)
                    .Include(x => x.PAOrganizationEntryCategoryPIs).ThenInclude(x => x.Questionnaire)
                    .Include(x => x.PAOrganizationEntryCategoryPIs).ThenInclude(x => x.PerformanceIndicator);
                });

                options.Entity<PAOrganizationEntryCategoryPI>(opt =>
                {
                    opt.DefaultWithDetailsFunc = q => q
                    .Include(x => x.PAOrganizationEntryCategoryPIAssessors)
                    .ThenInclude(x => x.OrganizationEntry)
                    .Include(x => x.PerformanceIndicator)
                    .Include(x => x.Questionnaire);
                });

                options.Entity<EmployeeContractAuthorize>(opt =>
                {
                    opt.DefaultWithDetailsFunc = q => q.Include(x => x.Position).Include(x => x.Department).Include(x => x.EmployeeContract);
                });

                options.Entity<PAResultPIAssessor>(opt =>
                {
                    opt.DefaultWithDetailsFunc = q => q.Include(x => x.PAResultPI);
                });

                options.Entity<PAResultPI>(opt =>
                {
                    opt.DefaultWithDetailsFunc = q => q.Include(x => x.PAResultPIAssessors);
                });

            });

            Configure<AbpDbContextOptions>(options =>
            {
                /* The main point to change your DBMS.
                 * See also PerformanceEvaluationMigrationsDbContextFactory for EF Core tooling. */
                options.UseSqlServer();
            });
        }
    }
}
