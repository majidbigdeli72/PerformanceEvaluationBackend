﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Septa.PerformanceEvaluation.DataBaseView;
using Septa.PerformanceEvaluation.Entities;
using Septa.PerformanceEvaluation.Enums;
using System;
using Volo.Abp;
using Volo.Abp.Users;

namespace Septa.PerformanceEvaluation.EntityFrameworkCore
{
    public static class PerformanceEvaluationDbContextModelCreatingExtensions
    {
        public static void ConfigurePerformanceEvaluation(this ModelBuilder builder)
        {
            Check.NotNull(builder, nameof(builder));

            builder.Entity<Department>(c =>
            {
                c.HasKey(d => d.Id);
                c.Property(d => d.Id).HasColumnName("OrganizationEntryId").ValueGeneratedNever();
                c.HasMany(d => d.Positions).WithOne(s => s.Department).HasForeignKey(f => f.DepartmentId).OnDelete(DeleteBehavior.Restrict);
                c.HasMany(d => d.EmployeeContractAuthorizes).WithOne(s => s.Department).HasForeignKey(f => f.DepartmentId).HasConstraintName("FK_EmployeeContractAuthorize_Department").OnDelete(DeleteBehavior.Restrict);

            });

            builder.Entity<Employee>(c =>
            {
                c.HasKey(d => d.Id);
                c.Property(d => d.Id).HasColumnName("OrganizationEntryId").ValueGeneratedNever();
                c.Property(d => d.BirthDate).HasColumnType("datetime");
                c.HasMany(d => d.EmployeeContracts).WithOne(s => s.Employee).HasForeignKey(f => f.EmployeeId).HasConstraintName("FK_EmployeeSalaryInfo_Employee").OnDelete(DeleteBehavior.Restrict);
                c.HasMany(d => d.EditedEmployeeContracts).WithOne(s => s.Editor).HasForeignKey(f => f.EditedBy).HasConstraintName("FK_EmployeeSalaryInfo_Employee1").OnDelete(DeleteBehavior.Restrict);
                c.HasMany(d => d.PAResultPIs).WithOne(s => s.Employee).HasForeignKey(f => f.EmployeeId).OnDelete(DeleteBehavior.Restrict);
                c.HasMany(d => d.PAResultPIAssessors).WithOne(s => s.Employee).HasForeignKey(f => f.AssessedById).OnDelete(DeleteBehavior.Restrict);
            });

            builder.Entity<EmployeeContract>(c =>
            {
                c.HasKey(d => d.Id);
                c.Property(d => d.StartDate).HasColumnType("datetime");
                c.Property(d => d.EndDate).HasColumnType("datetime");
                c.Property(d => d.InsuranceStartDate).HasColumnType("datetime");
                c.Property(d => d.LastEditDate).HasColumnType("datetime");
                c.HasMany(d => d.EmployeeContractAuthorizes).WithOne(s => s.EmployeeContract).HasForeignKey(f => f.EmployeeContractId).HasConstraintName("FK_EmployeeContractAuthorize_EmployeeContract").OnDelete(DeleteBehavior.Restrict);
            });

            builder.Entity<PAOrganizationEntry>(c =>
            {
                c.HasKey(d => d.Id);
                c.HasMany(d => d.PAOrganizationEntryCategories).WithOne(s => s.PAOrganizationEntry).HasForeignKey(f => f.PAOrganizationEntryId).OnDelete(DeleteBehavior.Restrict);
            });

            builder.Entity<PAOrganizationEntryCategory>(c =>
            {
                c.HasKey(d => d.Id);
                c.HasMany(d => d.PAOrganizationEntryCategoryPIs).WithOne(s => s.PAOrganizationEntryCategory).HasForeignKey(f => f.PAOrganizationEntryCategoryId).OnDelete(DeleteBehavior.Restrict);
            });

            builder.Entity<PAOrganizationEntryCategoryPI>(c =>
            {
                c.HasKey(d => d.Id);
                c.Property(d => d.EvaluationPeriodIndex).HasConversion(new EnumToNumberConverter<Gp_EvaluationPeriod, int>());
                c.Property(d => d.EvaluationTypeIndex).HasConversion(new EnumToNumberConverter<Gp_EvaluationType, int>());
                c.Property(d => d.CalculationTypeIndex).HasConversion(new EnumToNumberConverter<Gp_CalculationType, int>());
                c.HasMany(d => d.PAOrganizationEntryCategoryPIAssessors).WithOne(s => s.PAOrganizationEntryCategoryPI).HasForeignKey(f => f.PAOrganizationEntryCategoryPIId);
                c.HasMany(d => d.PAResultPIs).WithOne(s => s.PAOrganizationEntryCategoryPI).HasForeignKey(f => f.PAOrganizationEntryCategoryPIId);
            });

            builder.Entity<PAOrganizationEntryCategoryPIAssessor>(c =>
            {
                c.HasKey(d => d.Id);
                c.Property(d => d.AssessorTypeIndex).HasConversion(new EnumToNumberConverter<Gp_AssessorType, int>());
            });

            builder.Entity<PAResultPI>(c =>
            {
                c.HasKey(d => d.Id);
                c.HasMany(d => d.PAResultPIAssessors).WithOne(s => s.PAResultPI).HasForeignKey(f => f.PAResultPIId);
            });

            builder.Entity<PAResultPIAssessor>(c =>
            {
                c.HasKey(d => d.Id);
                c.Property(d => d.AssessStatusIndex).HasConversion(new EnumToNumberConverter<Gp_AssessStatus, int>());
            });

            builder.Entity<OperationEvaluationCategory>(c =>
            {
                c.HasKey(d => d.Id);
                c.Property(d => d.OperationEvaluationCategoryIndex).HasConversion(new EnumToNumberConverter<Gp_OperationEvaluationCategory, int>());
                c.HasMany(d => d.PAOrganizationEntryCategories).WithOne(s => s.OperationEvaluationCategory).HasForeignKey(f => f.OperationEvaluationCategoryId);
                c.HasMany(d => d.PAResultPIs).WithOne(s => s.OperationEvaluationCategory).HasForeignKey(f => f.OperationEvaluationCategoryId);
            });

            builder.Entity<PA>(c =>
            {
                c.HasKey(d => d.Id);
                c.Property(d => d.PaStatusIndex).HasConversion(new EnumToNumberConverter<GP_PaStatus, int>());
                c.HasMany(d => d.PAOrganizationEntrys).WithOne(s => s.PA).HasForeignKey(f => f.PAId).OnDelete(DeleteBehavior.Restrict);
            });

            builder.Entity<PALog>(c =>
            {
                c.HasKey(d => d.Id);
                c.Property(d => d.PAErrorTypeIndex).HasConversion(new EnumToNumberConverter<Gp_PAErrorType, int>());
            });

            builder.Entity<OrganizationEntry>(c =>
            {
                c.HasKey(d => d.Id);
                c.HasOne(d => d.Employee).WithOne(s => s.OrganizationEntry).HasForeignKey<Employee>(f => f.Id);
                c.HasOne(d => d.Department).WithOne(s => s.OrganizationEntry).HasForeignKey<Department>(f => f.Id);
                c.HasOne(d => d.Position).WithOne(s => s.OrganizationEntry).HasForeignKey<Position>(f => f.Id);
                c.HasMany(d => d.PAOrganizationEntryCategoryPIAssessors).WithOne(s => s.OrganizationEntry).HasForeignKey(f => f.CustomAssessorId);
                c.HasMany(d => d.Childrens).WithOne(s => s.Parent).HasForeignKey(f => f.ParentId);
                c.HasMany(d => d.PAOrganizationEntrys).WithOne(s => s.OrganizationEntry).HasForeignKey(f => f.OrganizationEntryId).OnDelete(DeleteBehavior.Restrict);
            });

            builder.Entity<PerformanceIndicator>(c =>
            {
                c.HasKey(d => d.Id);
                c.HasMany(d => d.PAOrganizationEntryCategoryPIs).WithOne(s => s.PerformanceIndicator).HasForeignKey(f => f.PerformanceIndicatorId).OnDelete(DeleteBehavior.Restrict);
            });

            builder.Entity<Position>(c =>
            {
                c.HasKey(d => d.Id);
                c.Property(d => d.Id).HasColumnName("OrganizationEntryId").ValueGeneratedNever();
                c.HasMany(d => d.EmployeeContractAuthorizes).WithOne(s => s.Position).HasForeignKey(f => f.PositionId).HasConstraintName("FK_EmployeeContractAuthorize_Position").OnDelete(DeleteBehavior.Restrict);
                c.HasMany(d => d.PAResultPIs).WithOne(s => s.Position).HasForeignKey(f => f.PositionId);
            });

            builder.Entity<Question>(c =>
            {
                c.HasKey(d => d.Id);
                c.HasMany(d => d.QuestionItems).WithOne(s => s.Question).HasForeignKey(f => f.QuestionId);
                c.HasMany(d => d.QuestionResults).WithOne(s => s.Question).HasForeignKey(f => f.QuestionId);
            });

            builder.Entity<QuestionItem>(c =>
            {
                c.HasKey(d => d.Id);
                c.HasMany(d => d.QuestionResults).WithOne(s => s.QuestionItem).HasForeignKey(f => f.SelectedQuestionItemId).OnDelete(DeleteBehavior.Restrict);
            });

            builder.Entity<Questionnaire>(c =>
            {
                c.HasKey(d => d.Id);
                c.HasMany(d => d.Questions).WithOne(s => s.Questionnaire).HasForeignKey(f => f.QuestionnaireId);
                c.HasMany(d => d.QuestionnaireResults).WithOne(s => s.Questionnaire).HasForeignKey(f => f.QuestionnaireId);
                c.HasMany(d => d.PAOrganizationEntryCategoryPIs).WithOne(s => s.Questionnaire).HasForeignKey(f => f.QuestionnaireId);
            });

            builder.Entity<QuestionnaireResult>(c =>
            {
                c.HasKey(d => d.Id);
                c.HasMany(d => d.QuestionResults).WithOne(s => s.QuestionnaireResult).HasForeignKey(f => f.QuestionnaireResultId).OnDelete(DeleteBehavior.Restrict);
                c.HasMany(d => d.PAResultPIAssessors).WithOne(s => s.QuestionnaireResult).HasForeignKey(f => f.QuestionnaireResultId).OnDelete(DeleteBehavior.Restrict); ;
            });

            builder.Entity<QuestionResult>(c =>
            {
                c.HasKey(d => d.Id);
            });

            //View
            builder.Entity<VwDepartment>(c =>
            {
                c.HasNoKey();
                c.ToView("VwDepartment");
            });

            builder.Entity<VwPosition>(c =>
            {
                c.HasNoKey();
                c.ToView("VwPosition");
            });
        }

        public static void ConfigureCustomUserProperties<TUser>(this EntityTypeBuilder<TUser> b)
            where TUser : class, IUser
        {
            b.Property<DateTime?>(nameof(AppUser.ExpirePasswordDate));
            
            //b.Property<decimal?>(nameof(AppUser.IQ));
            //b.Property<DateTime?>(nameof(AppUser.BirthDate));
        }
    }
}