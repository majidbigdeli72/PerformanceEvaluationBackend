﻿using Volo.Abp.Localization;

namespace Septa.PerformanceEvaluation.Localization
{
    [LocalizationResourceName("PerformanceEvaluation")]
    public class PerformanceEvaluationResource
    {

    }
}