﻿namespace Septa.PerformanceEvaluation.Enums
{
    public enum GP_PaStatus
    {
        Scheduled,
        ReadyToRun,
        InProgress,
        Finished
    }
}