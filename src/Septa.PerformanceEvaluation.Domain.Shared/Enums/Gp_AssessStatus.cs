﻿namespace Septa.PerformanceEvaluation.Enums
{
    public enum Gp_AssessStatus
    {
        Pennding,
        Evaluated,
        ByPass
    }

    public enum Gp_LoginResultType : byte
    {
        Success = 1,

        InvalidUserNameOrPassword = 2,

        NotAllowed = 3,

        LockedOut = 4,

        RequiresTwoFactor = 5,

        PasswordExpierd = 6
    }
}