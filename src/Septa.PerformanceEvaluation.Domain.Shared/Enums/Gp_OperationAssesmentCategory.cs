﻿namespace Septa.PerformanceEvaluation.Enums
{
    public enum Gp_OperationEvaluationCategory
    {
        Funcional,
        Corporate,
        Individual,
        Team
    }
}