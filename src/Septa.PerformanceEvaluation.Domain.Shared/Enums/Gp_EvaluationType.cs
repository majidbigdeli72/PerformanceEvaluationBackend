﻿namespace Septa.PerformanceEvaluation.Enums
{
    public enum Gp_EvaluationType
    {
        Questionnaire,
        Manual
    }
}