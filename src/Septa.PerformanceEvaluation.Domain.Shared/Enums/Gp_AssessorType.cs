﻿namespace Septa.PerformanceEvaluation.Enums
{
    public enum Gp_AssessorType
    {
        Manager,
        CoWorker,
        Position,
        Employee
    }
}