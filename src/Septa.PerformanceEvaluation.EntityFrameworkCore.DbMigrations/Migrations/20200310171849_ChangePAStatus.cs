﻿using Microsoft.EntityFrameworkCore.Migrations;
using Septa.PerformanceEvaluation.Enums;

namespace Septa.PerformanceEvaluation.Migrations
{
    public partial class ChangePAStatus : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ExcutionStrategyIndex",
                table: "PA");

            migrationBuilder.AddColumn<int>(
                name: "PaStatusIndex",
                table: "PA",
                nullable: false,
                defaultValue: (int)GP_PaStatus.Scheduled);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PaStatusIndex",
                table: "PA");

            migrationBuilder.AddColumn<int>(
                name: "ExcutionStrategyIndex",
                table: "PA",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
