﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Septa.PerformanceEvaluation.Migrations
{
    public partial class AddPALog : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PALog",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PAId = table.Column<int>(nullable: false),
                    PAOrganizationEntryId = table.Column<int>(nullable: true),
                    PAOrganizationEntryCategoryId = table.Column<int>(nullable: true),
                    PAOrganizationEntryCategoryPIId = table.Column<int>(nullable: true),
                    PAErrorTypeIndex = table.Column<int>(nullable: false),
                    Exception = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PALog", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PALog");
        }
    }
}
