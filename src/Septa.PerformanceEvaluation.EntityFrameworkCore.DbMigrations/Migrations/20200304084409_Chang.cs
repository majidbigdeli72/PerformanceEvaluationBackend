﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Septa.PerformanceEvaluation.Migrations
{
    public partial class Chang : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PAResultPI_Position_PositionId",
                table: "PAResultPI");

            migrationBuilder.DropTable(
                name: "PAPositionCategoryPIAssessor");

            migrationBuilder.DropTable(
                name: "PAPositionCategoryPI");

            migrationBuilder.DropTable(
                name: "PAPositionCategory");

            migrationBuilder.DropTable(
                name: "PAPosition");

            migrationBuilder.DropIndex(
                name: "IX_PAResultPI_PositionId",
                table: "PAResultPI");

            migrationBuilder.DropColumn(
                name: "PositionId",
                table: "PAResultPI");

            migrationBuilder.AddColumn<Guid>(
                name: "OrganizationEntryId",
                table: "PAResultPI",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateTable(
                name: "PAOrganizationEntry",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    OrganizationEntryId = table.Column<Guid>(nullable: false),
                    PAId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PAOrganizationEntry", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PAOrganizationEntry_OrganizationEntry_OrganizationEntryId",
                        column: x => x.OrganizationEntryId,
                        principalTable: "OrganizationEntry",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PAOrganizationEntry_PA_PAId",
                        column: x => x.PAId,
                        principalTable: "PA",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PAOrganizationEntryCategory",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PAOrganizationEntryId = table.Column<int>(nullable: false),
                    OperationEvaluationCategoryId = table.Column<int>(nullable: false),
                    Weight = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PAOrganizationEntryCategory", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PAOrganizationEntryCategory_OperationEvaluationCategory_OperationEvaluationCategoryId",
                        column: x => x.OperationEvaluationCategoryId,
                        principalTable: "OperationEvaluationCategory",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PAOrganizationEntryCategory_PAOrganizationEntry_PAOrganizationEntryId",
                        column: x => x.PAOrganizationEntryId,
                        principalTable: "PAOrganizationEntry",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PAOrganizationEntryCategoryPI",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PAOrganizationEntryCategoryId = table.Column<int>(nullable: false),
                    PerformanceIndicatorId = table.Column<int>(nullable: false),
                    QuestionnaireId = table.Column<int>(nullable: true),
                    Weight = table.Column<decimal>(nullable: false),
                    EvaluationPeriodIndex = table.Column<int>(nullable: false),
                    EvaluationTypeIndex = table.Column<int>(nullable: false),
                    CalculationTypeIndex = table.Column<int>(nullable: false),
                    Min = table.Column<decimal>(nullable: false),
                    Max = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PAOrganizationEntryCategoryPI", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PAOrganizationEntryCategoryPI_PAOrganizationEntryCategory_PAOrganizationEntryCategoryId",
                        column: x => x.PAOrganizationEntryCategoryId,
                        principalTable: "PAOrganizationEntryCategory",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PAOrganizationEntryCategoryPI_PerformanceIndicator_PerformanceIndicatorId",
                        column: x => x.PerformanceIndicatorId,
                        principalTable: "PerformanceIndicator",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PAOrganizationEntryCategoryPI_Questionnaire_QuestionnaireId",
                        column: x => x.QuestionnaireId,
                        principalTable: "Questionnaire",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PAOrganizationEntryCategoryPIAssessor",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PAOrganizationEntryCategoryPIId = table.Column<int>(nullable: false),
                    AssessorTypeIndex = table.Column<int>(nullable: false),
                    CustomAssessorId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PAOrganizationEntryCategoryPIAssessor", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PAOrganizationEntryCategoryPIAssessor_OrganizationEntry_CustomAssessorId",
                        column: x => x.CustomAssessorId,
                        principalTable: "OrganizationEntry",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PAOrganizationEntryCategoryPIAssessor_PAOrganizationEntryCategoryPI_PAOrganizationEntryCategoryPIId",
                        column: x => x.PAOrganizationEntryCategoryPIId,
                        principalTable: "PAOrganizationEntryCategoryPI",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PAResultPI_OrganizationEntryId",
                table: "PAResultPI",
                column: "OrganizationEntryId");

            migrationBuilder.CreateIndex(
                name: "IX_PAOrganizationEntry_OrganizationEntryId",
                table: "PAOrganizationEntry",
                column: "OrganizationEntryId");

            migrationBuilder.CreateIndex(
                name: "IX_PAOrganizationEntry_PAId",
                table: "PAOrganizationEntry",
                column: "PAId");

            migrationBuilder.CreateIndex(
                name: "IX_PAOrganizationEntryCategory_OperationEvaluationCategoryId",
                table: "PAOrganizationEntryCategory",
                column: "OperationEvaluationCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_PAOrganizationEntryCategory_PAOrganizationEntryId",
                table: "PAOrganizationEntryCategory",
                column: "PAOrganizationEntryId");

            migrationBuilder.CreateIndex(
                name: "IX_PAOrganizationEntryCategoryPI_PAOrganizationEntryCategoryId",
                table: "PAOrganizationEntryCategoryPI",
                column: "PAOrganizationEntryCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_PAOrganizationEntryCategoryPI_PerformanceIndicatorId",
                table: "PAOrganizationEntryCategoryPI",
                column: "PerformanceIndicatorId");

            migrationBuilder.CreateIndex(
                name: "IX_PAOrganizationEntryCategoryPI_QuestionnaireId",
                table: "PAOrganizationEntryCategoryPI",
                column: "QuestionnaireId");

            migrationBuilder.CreateIndex(
                name: "IX_PAOrganizationEntryCategoryPIAssessor_CustomAssessorId",
                table: "PAOrganizationEntryCategoryPIAssessor",
                column: "CustomAssessorId");

            migrationBuilder.CreateIndex(
                name: "IX_PAOrganizationEntryCategoryPIAssessor_PAOrganizationEntryCategoryPIId",
                table: "PAOrganizationEntryCategoryPIAssessor",
                column: "PAOrganizationEntryCategoryPIId");

            migrationBuilder.AddForeignKey(
                name: "FK_PAResultPI_OrganizationEntry_OrganizationEntryId",
                table: "PAResultPI",
                column: "OrganizationEntryId",
                principalTable: "OrganizationEntry",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PAResultPI_OrganizationEntry_OrganizationEntryId",
                table: "PAResultPI");

            migrationBuilder.DropTable(
                name: "PAOrganizationEntryCategoryPIAssessor");

            migrationBuilder.DropTable(
                name: "PAOrganizationEntryCategoryPI");

            migrationBuilder.DropTable(
                name: "PAOrganizationEntryCategory");

            migrationBuilder.DropTable(
                name: "PAOrganizationEntry");

            migrationBuilder.DropIndex(
                name: "IX_PAResultPI_OrganizationEntryId",
                table: "PAResultPI");

            migrationBuilder.DropColumn(
                name: "OrganizationEntryId",
                table: "PAResultPI");

            migrationBuilder.AddColumn<Guid>(
                name: "PositionId",
                table: "PAResultPI",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateTable(
                name: "PAPosition",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PAId = table.Column<int>(type: "int", nullable: false),
                    PositionId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PAPosition", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PAPosition_PA_PAId",
                        column: x => x.PAId,
                        principalTable: "PA",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PAPosition_Position_PositionId",
                        column: x => x.PositionId,
                        principalTable: "Position",
                        principalColumn: "OrganizationEntryId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PAPositionCategory",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    OperationEvaluationCategoryId = table.Column<int>(type: "int", nullable: false),
                    PAPositionId = table.Column<int>(type: "int", nullable: false),
                    Weight = table.Column<decimal>(type: "decimal(18,2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PAPositionCategory", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PAPositionCategory_OperationEvaluationCategory_OperationEvaluationCategoryId",
                        column: x => x.OperationEvaluationCategoryId,
                        principalTable: "OperationEvaluationCategory",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PAPositionCategory_PAPosition_PAPositionId",
                        column: x => x.PAPositionId,
                        principalTable: "PAPosition",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PAPositionCategoryPI",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CalculationTypeIndex = table.Column<int>(type: "int", nullable: false),
                    EvaluationPeriodIndex = table.Column<int>(type: "int", nullable: false),
                    EvaluationTypeIndex = table.Column<int>(type: "int", nullable: false),
                    Max = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Min = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    PAPositionCategoryId = table.Column<int>(type: "int", nullable: false),
                    PerformanceIndicatorId = table.Column<int>(type: "int", nullable: false),
                    QuestionnaireId = table.Column<int>(type: "int", nullable: true),
                    Weight = table.Column<decimal>(type: "decimal(18,2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PAPositionCategoryPI", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PAPositionCategoryPI_PAPositionCategory_PAPositionCategoryId",
                        column: x => x.PAPositionCategoryId,
                        principalTable: "PAPositionCategory",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PAPositionCategoryPI_PerformanceIndicator_PerformanceIndicatorId",
                        column: x => x.PerformanceIndicatorId,
                        principalTable: "PerformanceIndicator",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PAPositionCategoryPI_Questionnaire_QuestionnaireId",
                        column: x => x.QuestionnaireId,
                        principalTable: "Questionnaire",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PAPositionCategoryPIAssessor",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AssessorTypeIndex = table.Column<int>(type: "int", nullable: false),
                    CustomAssessorId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    PAPositionCategoryPIId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PAPositionCategoryPIAssessor", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PAPositionCategoryPIAssessor_OrganizationEntry_CustomAssessorId",
                        column: x => x.CustomAssessorId,
                        principalTable: "OrganizationEntry",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PAPositionCategoryPIAssessor_PAPositionCategoryPI_PAPositionCategoryPIId",
                        column: x => x.PAPositionCategoryPIId,
                        principalTable: "PAPositionCategoryPI",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PAResultPI_PositionId",
                table: "PAResultPI",
                column: "PositionId");

            migrationBuilder.CreateIndex(
                name: "IX_PAPosition_PAId",
                table: "PAPosition",
                column: "PAId");

            migrationBuilder.CreateIndex(
                name: "IX_PAPosition_PositionId",
                table: "PAPosition",
                column: "PositionId");

            migrationBuilder.CreateIndex(
                name: "IX_PAPositionCategory_OperationEvaluationCategoryId",
                table: "PAPositionCategory",
                column: "OperationEvaluationCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_PAPositionCategory_PAPositionId",
                table: "PAPositionCategory",
                column: "PAPositionId");

            migrationBuilder.CreateIndex(
                name: "IX_PAPositionCategoryPI_PAPositionCategoryId",
                table: "PAPositionCategoryPI",
                column: "PAPositionCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_PAPositionCategoryPI_PerformanceIndicatorId",
                table: "PAPositionCategoryPI",
                column: "PerformanceIndicatorId");

            migrationBuilder.CreateIndex(
                name: "IX_PAPositionCategoryPI_QuestionnaireId",
                table: "PAPositionCategoryPI",
                column: "QuestionnaireId");

            migrationBuilder.CreateIndex(
                name: "IX_PAPositionCategoryPIAssessor_CustomAssessorId",
                table: "PAPositionCategoryPIAssessor",
                column: "CustomAssessorId");

            migrationBuilder.CreateIndex(
                name: "IX_PAPositionCategoryPIAssessor_PAPositionCategoryPIId",
                table: "PAPositionCategoryPIAssessor",
                column: "PAPositionCategoryPIId");

            migrationBuilder.AddForeignKey(
                name: "FK_PAResultPI_Position_PositionId",
                table: "PAResultPI",
                column: "PositionId",
                principalTable: "Position",
                principalColumn: "OrganizationEntryId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
