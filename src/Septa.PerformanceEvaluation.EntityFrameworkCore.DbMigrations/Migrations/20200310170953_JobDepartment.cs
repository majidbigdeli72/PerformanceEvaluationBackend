﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Septa.PerformanceEvaluation.Migrations
{
    public partial class JobDepartment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<Guid>(
                name: "EmployeeId",
                table: "PAResultPI",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier");

            migrationBuilder.AddColumn<bool>(
                name: "IsGroupingEvaluation",
                table: "PAResultPI",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "OperationEvaluationCategoryId",
                table: "PAResultPI",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "OperationEvaluationCategoryWeight",
                table: "PAResultPI",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "PositionId",
                table: "PAResultPI",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_PAResultPI_OperationEvaluationCategoryId",
                table: "PAResultPI",
                column: "OperationEvaluationCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_PAResultPI_PositionId",
                table: "PAResultPI",
                column: "PositionId");

            migrationBuilder.AddForeignKey(
                name: "FK_PAResultPI_OperationEvaluationCategory_OperationEvaluationCategoryId",
                table: "PAResultPI",
                column: "OperationEvaluationCategoryId",
                principalTable: "OperationEvaluationCategory",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PAResultPI_Position_PositionId",
                table: "PAResultPI",
                column: "PositionId",
                principalTable: "Position",
                principalColumn: "OrganizationEntryId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PAResultPI_OperationEvaluationCategory_OperationEvaluationCategoryId",
                table: "PAResultPI");

            migrationBuilder.DropForeignKey(
                name: "FK_PAResultPI_Position_PositionId",
                table: "PAResultPI");

            migrationBuilder.DropIndex(
                name: "IX_PAResultPI_OperationEvaluationCategoryId",
                table: "PAResultPI");

            migrationBuilder.DropIndex(
                name: "IX_PAResultPI_PositionId",
                table: "PAResultPI");

            migrationBuilder.DropColumn(
                name: "IsGroupingEvaluation",
                table: "PAResultPI");

            migrationBuilder.DropColumn(
                name: "OperationEvaluationCategoryId",
                table: "PAResultPI");

            migrationBuilder.DropColumn(
                name: "OperationEvaluationCategoryWeight",
                table: "PAResultPI");

            migrationBuilder.DropColumn(
                name: "PositionId",
                table: "PAResultPI");

            migrationBuilder.AlterColumn<Guid>(
                name: "EmployeeId",
                table: "PAResultPI",
                type: "uniqueidentifier",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);
        }
    }
}
