﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Septa.PerformanceEvaluation.Migrations
{
    public partial class AddRestrict : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PAOrganizationEntry_PA_PAId",
                table: "PAOrganizationEntry");

            migrationBuilder.DropForeignKey(
                name: "FK_PAOrganizationEntryCategory_PAOrganizationEntry_PAOrganizationEntryId",
                table: "PAOrganizationEntryCategory");

            migrationBuilder.DropForeignKey(
                name: "FK_PAOrganizationEntryCategoryPI_PAOrganizationEntryCategory_PAOrganizationEntryCategoryId",
                table: "PAOrganizationEntryCategoryPI");

            migrationBuilder.AddForeignKey(
                name: "FK_PAOrganizationEntry_PA_PAId",
                table: "PAOrganizationEntry",
                column: "PAId",
                principalTable: "PA",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PAOrganizationEntryCategory_PAOrganizationEntry_PAOrganizationEntryId",
                table: "PAOrganizationEntryCategory",
                column: "PAOrganizationEntryId",
                principalTable: "PAOrganizationEntry",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PAOrganizationEntryCategoryPI_PAOrganizationEntryCategory_PAOrganizationEntryCategoryId",
                table: "PAOrganizationEntryCategoryPI",
                column: "PAOrganizationEntryCategoryId",
                principalTable: "PAOrganizationEntryCategory",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PAOrganizationEntry_PA_PAId",
                table: "PAOrganizationEntry");

            migrationBuilder.DropForeignKey(
                name: "FK_PAOrganizationEntryCategory_PAOrganizationEntry_PAOrganizationEntryId",
                table: "PAOrganizationEntryCategory");

            migrationBuilder.DropForeignKey(
                name: "FK_PAOrganizationEntryCategoryPI_PAOrganizationEntryCategory_PAOrganizationEntryCategoryId",
                table: "PAOrganizationEntryCategoryPI");

            migrationBuilder.AddForeignKey(
                name: "FK_PAOrganizationEntry_PA_PAId",
                table: "PAOrganizationEntry",
                column: "PAId",
                principalTable: "PA",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PAOrganizationEntryCategory_PAOrganizationEntry_PAOrganizationEntryId",
                table: "PAOrganizationEntryCategory",
                column: "PAOrganizationEntryId",
                principalTable: "PAOrganizationEntry",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PAOrganizationEntryCategoryPI_PAOrganizationEntryCategory_PAOrganizationEntryCategoryId",
                table: "PAOrganizationEntryCategoryPI",
                column: "PAOrganizationEntryCategoryId",
                principalTable: "PAOrganizationEntryCategory",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
