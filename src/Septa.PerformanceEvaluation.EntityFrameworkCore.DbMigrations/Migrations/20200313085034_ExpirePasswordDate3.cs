﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Septa.PerformanceEvaluation.Migrations
{
    public partial class ExpirePasswordDate3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "ExpirePasswordDate",
                table: "AppUsers",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ExpirePasswordDate",
                table: "AppUsers");
        }
    }
}
