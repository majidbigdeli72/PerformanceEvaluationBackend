﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Septa.PerformanceEvaluation.Migrations
{
    public partial class UpdatePAResultPI : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PAResultPI_OrganizationEntry_OrganizationEntryId",
                table: "PAResultPI");

            migrationBuilder.DropForeignKey(
                name: "FK_PAResultPI_PA_PAId",
                table: "PAResultPI");

            migrationBuilder.DropForeignKey(
                name: "FK_PAResultPI_PerformanceIndicator_PerformanceIndicatorId",
                table: "PAResultPI");

            migrationBuilder.DropIndex(
                name: "IX_PAResultPI_OrganizationEntryId",
                table: "PAResultPI");

            migrationBuilder.DropIndex(
                name: "IX_PAResultPI_PerformanceIndicatorId",
                table: "PAResultPI");

            migrationBuilder.DropColumn(
                name: "OrganizationEntryId",
                table: "PAResultPI");

            migrationBuilder.DropColumn(
                name: "PerformanceIndicatorId",
                table: "PAResultPI");

            migrationBuilder.AlterColumn<int>(
                name: "PAId",
                table: "PAResultPI",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<int>(
                name: "PAOrganizationEntryCategoryPIId",
                table: "PAResultPI",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ParentId",
                table: "PAResultPI",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_PAResultPI_PAOrganizationEntryCategoryPIId",
                table: "PAResultPI",
                column: "PAOrganizationEntryCategoryPIId");

            migrationBuilder.AddForeignKey(
                name: "FK_PAResultPI_PA_PAId",
                table: "PAResultPI",
                column: "PAId",
                principalTable: "PA",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PAResultPI_PAOrganizationEntryCategoryPI_PAOrganizationEntryCategoryPIId",
                table: "PAResultPI",
                column: "PAOrganizationEntryCategoryPIId",
                principalTable: "PAOrganizationEntryCategoryPI",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PAResultPI_PA_PAId",
                table: "PAResultPI");

            migrationBuilder.DropForeignKey(
                name: "FK_PAResultPI_PAOrganizationEntryCategoryPI_PAOrganizationEntryCategoryPIId",
                table: "PAResultPI");

            migrationBuilder.DropIndex(
                name: "IX_PAResultPI_PAOrganizationEntryCategoryPIId",
                table: "PAResultPI");

            migrationBuilder.DropColumn(
                name: "PAOrganizationEntryCategoryPIId",
                table: "PAResultPI");

            migrationBuilder.DropColumn(
                name: "ParentId",
                table: "PAResultPI");

            migrationBuilder.AlterColumn<int>(
                name: "PAId",
                table: "PAResultPI",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "OrganizationEntryId",
                table: "PAResultPI",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<int>(
                name: "PerformanceIndicatorId",
                table: "PAResultPI",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_PAResultPI_OrganizationEntryId",
                table: "PAResultPI",
                column: "OrganizationEntryId");

            migrationBuilder.CreateIndex(
                name: "IX_PAResultPI_PerformanceIndicatorId",
                table: "PAResultPI",
                column: "PerformanceIndicatorId");

            migrationBuilder.AddForeignKey(
                name: "FK_PAResultPI_OrganizationEntry_OrganizationEntryId",
                table: "PAResultPI",
                column: "OrganizationEntryId",
                principalTable: "OrganizationEntry",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PAResultPI_PA_PAId",
                table: "PAResultPI",
                column: "PAId",
                principalTable: "PA",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PAResultPI_PerformanceIndicator_PerformanceIndicatorId",
                table: "PAResultPI",
                column: "PerformanceIndicatorId",
                principalTable: "PerformanceIndicator",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
