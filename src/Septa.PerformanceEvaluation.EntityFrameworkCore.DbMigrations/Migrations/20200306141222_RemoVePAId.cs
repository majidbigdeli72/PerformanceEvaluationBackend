﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Septa.PerformanceEvaluation.Migrations
{
    public partial class RemoVePAId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PAResultPI_PA_PAId",
                table: "PAResultPI");

            migrationBuilder.DropIndex(
                name: "IX_PAResultPI_PAId",
                table: "PAResultPI");

            migrationBuilder.DropColumn(
                name: "PAId",
                table: "PAResultPI");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "PAId",
                table: "PAResultPI",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_PAResultPI_PAId",
                table: "PAResultPI",
                column: "PAId");

            migrationBuilder.AddForeignKey(
                name: "FK_PAResultPI_PA_PAId",
                table: "PAResultPI",
                column: "PAId",
                principalTable: "PA",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
