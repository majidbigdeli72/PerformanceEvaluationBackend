﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Modularity;

namespace Septa.PerformanceEvaluation.EntityFrameworkCore
{
    [DependsOn(
        typeof(PerformanceEvaluationEntityFrameworkCoreModule)
        )]
    public class PerformanceEvaluationEntityFrameworkCoreDbMigrationsModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddAbpDbContext<PerformanceEvaluationMigrationsDbContext>();
        }
    }
}
