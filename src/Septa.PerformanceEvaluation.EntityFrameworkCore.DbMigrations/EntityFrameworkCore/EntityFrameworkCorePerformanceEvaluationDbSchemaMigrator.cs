﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Septa.PerformanceEvaluation.Data;
using Volo.Abp.DependencyInjection;

namespace Septa.PerformanceEvaluation.EntityFrameworkCore
{
    [Dependency(ReplaceServices = true)]
    public class EntityFrameworkCorePerformanceEvaluationDbSchemaMigrator 
        : IPerformanceEvaluationDbSchemaMigrator, ITransientDependency
    {
        private readonly PerformanceEvaluationMigrationsDbContext _dbContext;

        public EntityFrameworkCorePerformanceEvaluationDbSchemaMigrator(PerformanceEvaluationMigrationsDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task MigrateAsync()
        {
            await _dbContext.Database.MigrateAsync();
        }
    }
}