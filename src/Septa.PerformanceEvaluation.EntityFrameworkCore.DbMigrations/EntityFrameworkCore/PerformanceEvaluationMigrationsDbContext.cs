﻿using Microsoft.EntityFrameworkCore;
using System;
using Volo.Abp.AuditLogging.EntityFrameworkCore;
using Volo.Abp.BackgroundJobs.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore.Modeling;
using Volo.Abp.FeatureManagement.EntityFrameworkCore;
using Volo.Abp.Identity;
using Volo.Abp.Identity.EntityFrameworkCore;
using Volo.Abp.IdentityServer.EntityFrameworkCore;
using Volo.Abp.PermissionManagement.EntityFrameworkCore;
using Volo.Abp.SettingManagement.EntityFrameworkCore;
using Volo.Abp.TenantManagement.EntityFrameworkCore;

namespace Septa.PerformanceEvaluation.EntityFrameworkCore
{
    /* This DbContext is only used for database migrations.
     * It is not used on runtime. See PerformanceEvaluationDbContext for the runtime DbContext.
     * It is a unified model that includes configuration for
     * all used modules and your application.
     */
    public class PerformanceEvaluationMigrationsDbContext : AbpDbContext<PerformanceEvaluationMigrationsDbContext>
    {
        public PerformanceEvaluationMigrationsDbContext(DbContextOptions<PerformanceEvaluationMigrationsDbContext> options) 
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            /* Include modules to your migration db context */

            builder.ConfigurePermissionManagement(x => x.TablePrefix = PerformanceEvaluationConsts.DbTablePrefix);
            builder.ConfigureSettingManagement(x => x.TablePrefix = PerformanceEvaluationConsts.DbTablePrefix);
            builder.ConfigureBackgroundJobs(x => x.TablePrefix = PerformanceEvaluationConsts.DbTablePrefix);
            builder.ConfigureAuditLogging(x => x.TablePrefix = PerformanceEvaluationConsts.DbTablePrefix);
            builder.ConfigureIdentity(x => x.TablePrefix = PerformanceEvaluationConsts.DbTablePrefix);
            builder.ConfigureIdentityServer(x => x.TablePrefix = PerformanceEvaluationConsts.DbTablePrefix);
            builder.ConfigureFeatureManagement(x => x.TablePrefix = PerformanceEvaluationConsts.DbTablePrefix);
            builder.ConfigureTenantManagement(x => x.TablePrefix = PerformanceEvaluationConsts.DbTablePrefix);

            /* Configure customizations for entities from the modules included  */

            builder.Entity<IdentityUser>(b =>
            {
                b.ConfigureCustomUserProperties();
                b.ConfigureExtraProperties();


            });

            /* Configure your own tables/entities inside the ConfigurePerformanceEvaluation method */

            builder.ConfigurePerformanceEvaluation();
        }
    }
}