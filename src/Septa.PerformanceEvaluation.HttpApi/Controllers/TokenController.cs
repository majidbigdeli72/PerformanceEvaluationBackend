﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Septa.PerformanceEvaluation.Models.Test;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Septa.PerformanceEvaluation.Controllers
{
    [Authorize]
    [Route("api/Token")]
    public class TokenController : PerformanceEvaluationController
    {
        public TokenController()
        {
            
        }

        [HttpPost]
        [Route("ExpireToken")]
        public async Task<bool> ExpireToken()
        {

            await HttpContext.SignOutAsync("idsrv");
            await HttpContext.SignOutAsync();
            HttpContext.Response.Cookies.Delete("token");
            return true;

        }

        [HttpGet]
        public Task Test()
        {
            return Task.CompletedTask;
        }
    }
}
