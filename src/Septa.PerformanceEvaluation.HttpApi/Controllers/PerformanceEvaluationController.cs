﻿using Septa.PerformanceEvaluation.Localization;
using Volo.Abp.AspNetCore.Mvc;

namespace Septa.PerformanceEvaluation.Controllers
{
    /* Inherit your controllers from this class.
     */
    public abstract class PerformanceEvaluationController : AbpController
    {
        protected PerformanceEvaluationController()
        {
            LocalizationResource = typeof(PerformanceEvaluationResource);
        }
    }
}