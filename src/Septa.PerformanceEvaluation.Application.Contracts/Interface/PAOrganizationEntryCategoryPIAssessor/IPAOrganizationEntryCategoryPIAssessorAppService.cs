﻿using Septa.PerformanceEvaluation.Dto;
using Volo.Abp.Application.Services;

namespace Septa.PerformanceEvaluation.Interface
{
    public interface IPAOrganizationEntryCategoryPIAssessorAppService : ICrudAppService<PAOrganizationEntryCategoryPIAssessorGetDto, int, PagedAndSortedResultDto, PAOrganizationEntryCategoryPIAssessorCreateDto, PAOrganizationEntryCategoryPIAssessorUpdateDto>
    {
    }
}
