﻿using System;
using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;

namespace Septa.PerformanceEvaluation.Interface
{
    public interface IPersianCalendarManager : ITransientDependency
    {
        DateTime GetWeekLastDay(DateTime dateTime);
        DateTime GetMonthFirstDay(DateTime dateTime);
        DateTime GetMonthLastDay(DateTime dateTime);
        DateTime GetQuarterLastDay(DateTime dateTime);
        DateTime GetYearLastDay(DateTime dateTime);
    }
}
