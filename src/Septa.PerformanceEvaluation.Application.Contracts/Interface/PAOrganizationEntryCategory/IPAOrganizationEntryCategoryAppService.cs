﻿using Septa.PerformanceEvaluation.Dto;
using Volo.Abp.Application.Services;

namespace Septa.PerformanceEvaluation.Interface
{
    public interface IPAOrganizationEntryCategoryAppService : ICrudAppService<PAOrganizationEntryCategoryGetDto, int, PagedAndSortedResultDto, PAOrganizationEntryCategoryCreateDto, PAOrganizationEntryCategoryUpdateDto>
    {
    }
}
