﻿using Septa.PerformanceEvaluation.Dto.Account;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace Septa.PerformanceEvaluation.Interface.Account
{
    public interface IAccountAppService : IApplicationService
    {
        Task<LoginResult> Login(UserLoginDto login);
    }
}
