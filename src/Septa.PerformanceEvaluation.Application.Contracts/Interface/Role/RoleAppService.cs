﻿using Volo.Abp.Identity;

namespace Septa.PerformanceEvaluation.Interface
{
    public interface IRoleAppService : IIdentityRoleAppService
    {
    }
}
