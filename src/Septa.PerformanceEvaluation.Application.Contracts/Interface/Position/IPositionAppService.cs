﻿using Septa.PerformanceEvaluation.Dto;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace Septa.PerformanceEvaluation.Interface
{
    public interface IPositionAppService : ICrudAppService<OrganizationEntryPositionGetDto, Guid, PagedAndSortedResultDto, OrganizationEntryPositionCreateDto, OrganizationEntryPositionUpdateDto>
    {
        Task<List<VwPositionDto>> GetPositionPathAsync();
    }
}
