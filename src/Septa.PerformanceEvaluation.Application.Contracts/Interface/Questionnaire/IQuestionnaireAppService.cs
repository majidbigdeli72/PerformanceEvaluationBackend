﻿using Septa.PerformanceEvaluation.Dto;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace Septa.PerformanceEvaluation.Interface
{
    public interface IQuestionnaireAppService : ICrudAppService<QuestionnaireGetDto, int, PagedAndSortedResultDto, QuestionnaireCreateDto, QuestionnaireUpdateDto>
    {
        //Task<ReturnSuccessDto> CreateQuestionnaireAsync(QuestionnaireCreateDto questionnaireCreateDto);
        //Task<ReturnSuccessDto> GetQuestionnaireListAsync(PagedAndSortedResultRequestDto pagedAndSortedResultRequestDto);
        //Task<ReturnSuccessDto> GetQuestionnaireAsync(int questionnaireId);
        //Task<ReturnSuccessDto> UpdateQuestionnaireAsync(QuestionnaireUpdateDto questionnaireUpdateDto);


    }
}
