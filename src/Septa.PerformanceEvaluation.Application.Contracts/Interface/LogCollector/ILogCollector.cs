﻿using Septa.PerformanceEvaluation.Enums;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;

namespace Septa.PerformanceEvaluation.Interface
{
    public interface ILogCollector : ITransientDependency
    {
        void AddToPALog(int pAId, int? pAOrganizationEntryId, int? pAOrganizationEntryCategoryId, int? pAOrganizationEntryCategoryPIId, int? pAResultPIId, Gp_PAErrorType pAErrorTypeIndex, string exception, DateTime relatedDate);
        Task PersistPALogAsync();

        void ClearPALog(); 
   
    }
}
