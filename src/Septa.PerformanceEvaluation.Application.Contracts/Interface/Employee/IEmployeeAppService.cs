﻿using Septa.PerformanceEvaluation.Dto;
using System;
using Volo.Abp.Application.Services;

namespace Septa.PerformanceEvaluation.Interface.Employee
{
    public interface IEmployeeAppService : ICrudAppService<OrganizationEntryEmployeeGetDto, Guid, PagedAndSortedResultDto, OrganizationEntryEmployeeCreateDto, OrganizationEntryEmployeeUpdateDto>
    {
    }
}
