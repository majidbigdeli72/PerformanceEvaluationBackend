﻿using Septa.PerformanceEvaluation.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;
using Volo.Abp.DependencyInjection;

namespace Septa.PerformanceEvaluation.Interface
{
    public interface ISendPIToAssessorAppService : IApplicationService
    {
        Task ExecutePA(DateTime dateTime);
    }
}
