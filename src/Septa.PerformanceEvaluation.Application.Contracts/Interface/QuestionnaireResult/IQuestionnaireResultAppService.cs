﻿using Septa.PerformanceEvaluation.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace Septa.PerformanceEvaluation.Interface
{
    public interface IQuestionnaireResultAppService :IApplicationService
    {
        Task<QuestionnaireResultGetDto> CreateQuestionnaireResult(List<QuestionnaireResultCreateDto> questionnaireResult);
    }
}
