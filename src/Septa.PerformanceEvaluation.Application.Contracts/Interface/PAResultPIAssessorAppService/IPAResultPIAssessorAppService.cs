﻿using Microsoft.AspNetCore.Authorization;
using Septa.PerformanceEvaluation.Dto;
using Septa.PerformanceEvaluation.Enums;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace Septa.PerformanceEvaluation.Interface
{
    public interface IPAResultPIAssessorAppService : IApplicationService
    {
        Task<bool> ByPassPaResultPIAssessor(int paResultPIAssessorId);
        Task<bool> ChangeAssessor(int paResultPIAssessorId, Guid assessedById);
        Task<bool> ChangePaResultPIAssessorStatus(int paResultPIAssessorId, Gp_AssessStatus assessStatus);
        [Authorize(Roles = "paadmin")]
        Task<bool> ChangePAResultPIEmployee(int paResultPIId, Guid employeeId);
        Task<PagedResultDto<PAResultPIAssessorGetDto>> GetAssessorPAResultPIAsync(Guid assessorId);
        Task<PagedResultDto<PAResultPIAssessorGetDto>> GetCurentUserPAResultPIAsync();
    }
}
