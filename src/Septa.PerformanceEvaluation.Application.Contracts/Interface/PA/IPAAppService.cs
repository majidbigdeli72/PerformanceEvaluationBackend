﻿using Septa.PerformanceEvaluation.Dto;
using Volo.Abp.Application.Services;

namespace Septa.PerformanceEvaluation.Interface
{
    public interface IPAAppService : ICrudAppService<PAGetDto, int, PagedAndSortedResultDto, PACreateDto, PAUpdateDto>
    {
    }
}
