﻿using Septa.PerformanceEvaluation.Dto;
using Volo.Abp.Application.Services;

namespace Septa.PerformanceEvaluation.Interface
{
    public interface IPerformanceIndicatorAppService : ICrudAppService<PerformanceIndicatorGetDto, int, PagedAndSortedResultDto, PerformanceIndicatorCreateDto, PerformanceIndicatorUpdateDto>
    {
    }
}
