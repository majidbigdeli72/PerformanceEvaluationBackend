﻿using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace Septa.PerformanceEvaluation.Interface
{
    public interface ITokenAppService : IApplicationService
    {
        string ValidateToken();
    }
}
