﻿using Septa.PerformanceEvaluation.Dto;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace Septa.PerformanceEvaluation.Interface
{
    public interface IDepartmentAppService : ICrudAppService<OrganizationEntryDepartmentGetDto, Guid, PagedAndSortedResultDto, OrganizationEntryDepartmentCreateDto, OrganizationEntryDepartmentUpdateDto>
    {
        Task<List<VwDepartmentDto>> GetDepartmentPathAsync();
    }
}
