﻿using Septa.PerformanceEvaluation.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace Septa.PerformanceEvaluation.Interface
{
    public interface IQuestionResultAppService : IApplicationService
    {
        Task<List<QuestionResultGetDto>> GetQuestionResultAsync(int questionnaireResultId);
    }
}
