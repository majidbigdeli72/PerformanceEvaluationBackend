﻿using Septa.PerformanceEvaluation.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace Septa.PerformanceEvaluation.Interface
{
    public interface IPALogAppService : IApplicationService
    {
        Task<PagedResultDto<PALogGetDto>> GetPALogs();        
    }
}
