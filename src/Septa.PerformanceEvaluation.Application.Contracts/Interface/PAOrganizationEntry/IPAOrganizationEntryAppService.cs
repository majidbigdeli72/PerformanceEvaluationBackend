﻿using Septa.PerformanceEvaluation.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Services;

namespace Septa.PerformanceEvaluation.Interface
{
    public interface IPAOrganizationEntryAppService : ICrudAppService<PAOrganizationEntryGetDto, int, PagedAndSortedResultDto, PAOrganizationEntryCreateDto, PAOrganizationEntryUpdateDto>
    {
    }
}
