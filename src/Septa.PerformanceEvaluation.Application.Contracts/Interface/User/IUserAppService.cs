﻿using System.Threading.Tasks;
using Volo.Abp.Identity;

namespace Septa.PerformanceEvaluation.Interface
{
    public interface IUserAppService : IIdentityUserAppService
    {
        Task ChangeUserPasswordAsync(string userName ,string newPassword);
        Task<bool> HasRoleAsync(string roleName);
    }
}
