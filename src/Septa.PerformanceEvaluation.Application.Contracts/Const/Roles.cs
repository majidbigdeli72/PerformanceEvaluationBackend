﻿namespace Septa.PerformanceEvaluation.Const
{
    public static class Roles
    {
        public const string PaAdmin = "paadmin";
        public const string SystemAdmin = "systemadmin";
        public const string Secretary = "secretary";
        public const string FinancialOperator = "financialoperator";
        public const string FinancialAdmin = "financialadmin";
        public const string HROperator = "hroperator";
        public const string HRAdmin = "hradmin";
        public const string Manager = "manager";
        public const string User = "user";

    }
}
