﻿using Septa.PerformanceEvaluation.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Septa.PerformanceEvaluation.Dto
{
    public class PALogGetDto
    {
        public string Exception { get; set; }
        public Gp_PAErrorType PAErrorTypeIndex { get; set; }
        public DateTime RelatedDate { get; set; }
        public string PAName { get; set; }
        public string PIName { get; set; }
        public string OrganizationEntryName { get; set; }
    }
}
