﻿namespace Septa.PerformanceEvaluation.Dto
{
    public class ReturnSuccessDto
    {
        public object Data { get; set; }
        public string Message { get; set; }

    }
}
