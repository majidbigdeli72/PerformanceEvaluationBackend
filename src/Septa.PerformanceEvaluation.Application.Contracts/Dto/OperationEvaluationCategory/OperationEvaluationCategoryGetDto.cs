﻿using Septa.PerformanceEvaluation.Enums;
using Volo.Abp.Application.Dtos;

namespace Septa.PerformanceEvaluation.Dto
{
    public class OperationEvaluationCategoryGetDto : EntityDto<int>
    {
        public string Name { get; set; }
        public Gp_OperationEvaluationCategory OperationEvaluationCategoryIndex { get; set; }
    }

}
