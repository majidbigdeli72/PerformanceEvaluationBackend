﻿namespace Septa.PerformanceEvaluation.Dto
{
    public class PerformanceIndicatorCreateDto 
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
