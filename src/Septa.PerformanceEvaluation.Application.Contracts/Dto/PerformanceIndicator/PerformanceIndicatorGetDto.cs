﻿using Volo.Abp.Application.Dtos;

namespace Septa.PerformanceEvaluation.Dto
{
    public class PerformanceIndicatorGetDto :EntityDto<int>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        //     public List<PAOrganizationEntryCategoryPIGetDto> PAOrganizationEntryCategoryPIs { get; set; }
    }
}
