﻿namespace Septa.PerformanceEvaluation.Dto
{
    public class PerformanceIndicatorUpdateDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
