﻿namespace Septa.PerformanceEvaluation.Dto
{
    public class OrganizationEntryDepartmentCreateDto : OrganizationEntryCreateDto
    {
        public DepartmentCreateDto Department { get; set; }
    }
}
