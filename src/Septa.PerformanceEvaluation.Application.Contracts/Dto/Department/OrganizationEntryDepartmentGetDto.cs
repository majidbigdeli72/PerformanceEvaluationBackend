﻿namespace Septa.PerformanceEvaluation.Dto
{
    public class OrganizationEntryDepartmentGetDto : OrganizationEntryGetDto
    {
        public DepartmentGetDto Department { get; set; }
    }




}
