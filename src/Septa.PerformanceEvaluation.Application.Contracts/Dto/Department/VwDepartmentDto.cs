﻿using System;


namespace Septa.PerformanceEvaluation.Dto
{
    public class VwDepartmentDto
    {
        public Guid OrganizationEntryId { get; set; }
        public Guid D_OrganizationEntryId { get; set; }
        public string Path2Root { get; set; }
    }

}
