﻿using Septa.PerformanceEvaluation.Enums;
using System;
using Volo.Abp.Application.Dtos;

namespace Septa.PerformanceEvaluation.Dto
{
    public class PAResultPIAssessorGetDto : EntityDto<int>
    {
        public Gp_AssessStatus AssessStatusIndex { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get { return GetFullName(); } }
        public int IterationNumber { get; set; }
        public string Membership { get; set; }
        public string PAName { get; set; }
        public string PerformanceIndicatorName { get; set; }
        public int? QuestionnaireId { get; set; }
        public string OperationEvaluationCategoryName { get; set; }
        public Gp_EvaluationType EvaluationTypeIndex { get; set; }
        public Gp_CalculationType CalculationTypeIndex { get; set; }
        public int PAOrganizationEntryCategoryPIId { get; set; }
        public int PAResultPIId { get; set; }
        public DateTime? AssessDate { get; set; }
        public int? QuestionnaireResultId { get; set; }
        public decimal? Value { get; set; }
        public bool IsEditAble { get { return CheckEditAble(); } }
        private bool CheckEditAble()
        {
            if (!AssessDate.HasValue)
            {
                return true;
            }

           return AssessDate.Value.Date.AddDays(7) >= DateTime.Today;
        }
        private string GetFullName()
        {
            return FirstName + " " + LastName;
        }
    }


}
