﻿using Septa.PerformanceEvaluation.Enums;

namespace Septa.PerformanceEvaluation.Dto
{
    public class PAResultPIAssessorUpdateDto
    {
        public int PAResultAssessorId { get; set; }
        public Gp_CalculationType CalculationTypeIndex { get; set; }
        public decimal Value { get; set; }
        public int PAOrganizationEntryCategoryPIId { get; set; }
    }


}
