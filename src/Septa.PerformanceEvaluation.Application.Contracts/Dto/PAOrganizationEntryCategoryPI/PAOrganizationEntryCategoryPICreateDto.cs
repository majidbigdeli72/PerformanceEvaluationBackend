﻿using Septa.PerformanceEvaluation.Enums;

namespace Septa.PerformanceEvaluation.Dto
{
    public class PAOrganizationEntryCategoryPICreateDto
    {
        public int PAOrganizationEntryCategoryId { get; set; }
        public int PerformanceIndicatorId { get; set; }
        public int? QuestionnaireId { get; set; }
        public decimal Weight { get; set; }
        public bool IsGroupingEvaluation { get; set; }
        public Gp_EvaluationPeriod EvaluationPeriodIndex { get; set; }
        public Gp_EvaluationType EvaluationTypeIndex { get; set; }
        public Gp_CalculationType CalculationTypeIndex { get; set; }
        public decimal Min { get; set; }
        public decimal Max { get; set; }

    }
}
