﻿using Septa.PerformanceEvaluation.Enums;
using System.Collections.Generic;
using Volo.Abp.Application.Dtos;

namespace Septa.PerformanceEvaluation.Dto
{
    public class PAOrganizationEntryCategoryPIGetDto : EntityDto<int>
    {
        public int PAOrganizationEntryCategoryId { get; set; }
        public int PerformanceIndicatorId { get; set; }
        public int? QuestionnaireId { get; set; }
        public decimal Weight { get; set; }
        public Gp_EvaluationPeriod EvaluationPeriodIndex { get; set; }
        public Gp_EvaluationType EvaluationTypeIndex { get; set; }
        public Gp_CalculationType CalculationTypeIndex { get; set; }
        public decimal Min { get; set; }
        public decimal Max { get; set; }
        public bool IsGroupingEvaluation { get; set; }
        public virtual PerformanceIndicatorGetDto PerformanceIndicator { get; set; }
    //    public virtual PAOrganizationEntryCategoryGetDto PAOrganizationEntryCategory { get; set; }
        public virtual QuestionnaireGetDto Questionnaire { get; set; }
        public List<PAOrganizationEntryCategoryPIAssessorGetDto> PAOrganizationEntryCategoryPIAssessors { get; set; }

    }
}
