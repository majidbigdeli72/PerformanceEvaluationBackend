﻿using System;

namespace Septa.PerformanceEvaluation.Dto
{
    public class PAOrganizationEntryCreateDto
    {
        public Guid OrganizationEntryId { get; set; }
        public int PAId { get; set; }
    }
}
