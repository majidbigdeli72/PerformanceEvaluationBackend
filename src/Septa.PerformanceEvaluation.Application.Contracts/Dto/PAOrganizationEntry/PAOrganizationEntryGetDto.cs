﻿using System;
using System.Collections.Generic;
using Volo.Abp.Application.Dtos;

namespace Septa.PerformanceEvaluation.Dto
{
    public class PAOrganizationEntryGetDto : EntityDto<int>
    {
        public Guid OrganizationEntryId { get; set; }
        public int PAId { get; set; }
        public OrganizationEntryGetDto OrganizationEntry { get; set; }
        public List<PAOrganizationEntryCategoryGetDto> PAOrganizationEntryCategories { get; set; }

    }
}
