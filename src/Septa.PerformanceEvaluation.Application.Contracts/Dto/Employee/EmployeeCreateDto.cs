﻿using System;

namespace Septa.PerformanceEvaluation.Dto
{
    public class EmployeeCreateDto
    {
        public int SbuId { get; set; }
        public string EmployeeCode { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Sex { get; set; }
        public DateTime? BirthDate { get; set; }
        public string NationalCode { get; set; }
        public string Tel { get; set; }
        public string Address { get; set; }
        public string UserName { get; set; }
        public string ADUsername { get; set; }
        public string Email { get; set; }
        public int EmployeeStatusIndex { get; set; }
        public int? InsuranceNo { get; set; }
        public double? OldAvailableLeaveDay { get; set; }
        public Guid? PicFileId { get; set; }
    }
}
