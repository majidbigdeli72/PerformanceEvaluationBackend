﻿namespace Septa.PerformanceEvaluation.Dto
{
    public class QuestionResultGetDto
    {
        public int QuestionId { get; private set; }
        public int SelectedQuestionItemId { get; private set; }
        public int QuestionnaireResultId { get; private set; }
    }
}
