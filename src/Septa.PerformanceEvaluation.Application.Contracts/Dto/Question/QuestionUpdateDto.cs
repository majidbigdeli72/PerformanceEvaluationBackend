﻿using System.Collections.Generic;

namespace Septa.PerformanceEvaluation.Dto
{
    public class QuestionUpdateDto
    {
        public int Id { get; set; }
        public int QuestionnaireId { get; set; }
        public string Content { get; set; }
        public decimal Weight { get; set; }
        public List<QuestionItemUpdateDto> QuestionItems { get; set; }

    }
}
