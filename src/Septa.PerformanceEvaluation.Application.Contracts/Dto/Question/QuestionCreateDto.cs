﻿using System.Collections.Generic;

namespace Septa.PerformanceEvaluation.Dto
{
    public class QuestionCreateDto
    {
        public string Content { get; set; }
        public decimal Weight { get; set; }
        public List<QuestionItemCreateDto> QuestionItems { get; set; }
    }
}
