﻿using System;
using Volo.Abp.Application.Dtos;


namespace Septa.PerformanceEvaluation.Dto
{
    public class PositionOrganizationEntryGetDto : EntityDto<Guid>
    {
        public Guid DepartmentId { get; set; }
        public int JobLevelIndex { get; set; }
        public OrganizationEntryGetDto OrganizationEntry { get; set; }
    }

}
