﻿using System;


namespace Septa.PerformanceEvaluation.Dto
{
    public class PositionCreateDto
    {
        public Guid DepartmentId { get; set; }
        public int JobLevelIndex { get; set; }
    }

}
