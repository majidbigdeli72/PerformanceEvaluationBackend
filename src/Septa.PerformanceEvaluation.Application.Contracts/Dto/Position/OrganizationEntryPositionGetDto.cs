﻿using System;


namespace Septa.PerformanceEvaluation.Dto
{
    public class OrganizationEntryPositionGetDto : OrganizationEntryGetDto
    {
        public PositionGetDto Position { get; set; }
    }

}
