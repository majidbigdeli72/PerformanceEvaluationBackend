﻿using System;
using Volo.Abp.Application.Dtos;


namespace Septa.PerformanceEvaluation.Dto
{
    public class PositionGetDto : EntityDto<Guid>
    {
        public Guid DepartmentId { get; set; }
        public int JobLevelIndex { get; set; }

    }

}
