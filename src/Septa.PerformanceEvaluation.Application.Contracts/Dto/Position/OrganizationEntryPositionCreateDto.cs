﻿using System;


namespace Septa.PerformanceEvaluation.Dto
{
    public class OrganizationEntryPositionCreateDto : OrganizationEntryCreateDto
    {
        public PositionCreateDto Position { get; set; }
    }

}
