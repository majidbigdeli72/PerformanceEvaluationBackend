﻿using Septa.PerformanceEvaluation.Enums;
using System;

namespace Septa.PerformanceEvaluation.Dto
{
    public class PAOrganizationEntryCategoryPIAssessorCreateDto
    {
        public int PAOrganizationEntryCategoryPIId { get; set; }
        public Gp_AssessorType AssessorTypeIndex { get; set; }
        public Guid? CustomAssessorId { get; set; }
    }
}
