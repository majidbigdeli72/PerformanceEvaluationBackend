﻿using Septa.PerformanceEvaluation.Enums;
using System;
using Volo.Abp.Application.Dtos;

namespace Septa.PerformanceEvaluation.Dto
{
    public class PAOrganizationEntryCategoryPIAssessorGetDto : EntityDto<int>
    {
        public int PAOrganizationEntryCategoryPIId { get; set; }
        public Gp_AssessorType AssessorTypeIndex { get; set; }
        public Guid? CustomAssessorId { get; set; }
        public OrganizationEntryGetDto OrganizationEntry { get; set; }


    }
}
