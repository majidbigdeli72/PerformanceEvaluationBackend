﻿using Septa.PerformanceEvaluation.Enums;
using System;

namespace Septa.PerformanceEvaluation.Dto
{
    public class LogCollectorDto
    {
        public int PAId { get; set; }
        public int? PAOrganizationEntryId { get; set; }
        public int? PAOrganizationEntryCategoryId { get; set; }
        public int? PAOrganizationEntryCategoryPIId { get; set; }
        public int? PAResultPIId { get; set; }
        public Gp_PAErrorType PAErrorTypeIndex { get; set; }
        public string Exception { get; set; }
        public DateTime RelatedDate { get; set; }

    }
}
