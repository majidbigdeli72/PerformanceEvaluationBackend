﻿using Septa.PerformanceEvaluation.Enums;

namespace Septa.PerformanceEvaluation.Dto
{
    public class QuestionnaireResultCreateDto
    {
        public int PAResultAssessorId { get; set; }
        public decimal Value { get; set; }
        public int QuestionId { get; set; }
        public int SelectedItemId { get; set; }
        public Gp_CalculationType CalculationTypeIndex { get; set; }
        public int QuestionnaireId { get; set; }
        public decimal Weight { get; set; }

    }




}
