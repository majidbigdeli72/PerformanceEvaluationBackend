﻿using System;
using Volo.Abp.Application.Dtos;

namespace Septa.PerformanceEvaluation.Dto
{
    public class QuestionnaireResultGetDto :EntityDto<int>
    {
        public int QuestionnaireId { get; private set; }
        public DateTime Date { get; private set; }
        public Guid FilledBy { get; private set; }
        public decimal FinalValue { get; private set; }

    }




}
