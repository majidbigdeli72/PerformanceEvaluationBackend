﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Septa.PerformanceEvaluation.Dto
{
    public class PAOrganizationEntryCategoryCreateDto
    {
        public int PAOrganizationEntryId { get; set; }
        public int OperationEvaluationCategoryId { get; set; }
        public decimal Weight { get; set; }

    }


}
