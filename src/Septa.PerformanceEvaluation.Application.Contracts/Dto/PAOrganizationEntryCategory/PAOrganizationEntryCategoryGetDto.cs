﻿using System.Collections.Generic;
using Volo.Abp.Application.Dtos;

namespace Septa.PerformanceEvaluation.Dto
{
    public class PAOrganizationEntryCategoryGetDto : EntityDto<int>
    {
        public int PAOrganizationEntryId { get; set; }
        public int OperationEvaluationCategoryId { get; set; }
        public decimal Weight { get; set; }
        public OperationEvaluationCategoryGetDto OperationEvaluationCategory { get; set; }
        public List<PAOrganizationEntryCategoryPIGetDto> PAOrganizationEntryCategoryPIs { get; set; }
    }
}
