﻿namespace Septa.PerformanceEvaluation.Dto
{
    public class PAOrganizationEntryCategoryUpdateDto
    {
        public int PAOrganizationEntryId { get; set; }
        public int OperationEvaluationCategoryId { get; set; }
        public decimal Weight { get; set; }

    }


}
