﻿using System;
using System.Collections.Generic;
using Volo.Abp.Application.Dtos;

namespace Septa.PerformanceEvaluation.Dto
{
    public class PAGetDto : EntityDto<int>
    {
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime? LastExecution { get; set; }
        public int DaysBeforeDueDateWeekly { get; set; }
        public int DaysBeforeDueDateMonthly { get; set; }
        public int DaysBeforeDueDateQuarterly { get; set; }
        public int DaysBeforeDueDateHalfly { get; set; }
        public int DaysBeforeDueDateYearly { get; set; }
        public List<PAOrganizationEntryGetDto> paOrganizationEntrys { get; set; }
    }

}
