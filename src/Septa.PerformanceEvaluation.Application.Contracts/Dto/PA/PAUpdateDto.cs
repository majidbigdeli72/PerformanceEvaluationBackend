﻿using System;

namespace Septa.PerformanceEvaluation.Dto
{
    public class PAUpdateDto 
    {
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int DaysBeforeDueDateWeekly { get; set; }
        public int DaysBeforeDueDateMonthly { get; set; }
        public int DaysBeforeDueDateQuarterly { get; set; }
        public int DaysBeforeDueDateHalfly { get; set; }
        public int DaysBeforeDueDateYearly { get; set; }
    }

}
