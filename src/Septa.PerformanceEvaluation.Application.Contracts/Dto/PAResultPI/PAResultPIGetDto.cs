﻿using System;
using Volo.Abp.Application.Dtos;

namespace Septa.PerformanceEvaluation.Dto
{
    public class PAResultPIGetDto : EntityDto<int>
    {
        public int PAId { get; set; }
        public Guid EmployeeId { get; set; }
        public Guid OrganizationEntryId { get; set; }
        public int PerformanceIndicatorId { get; set; }
        public int IterationNumber { get; set; }
        public DateTime CreateDate { get; set; }
        public PAGetDto  PA { get; set; }
        public EmployeeGetDto Employee { get; set; }
        public OrganizationEntryGetDto OrganizationEntry { get; set; }

        public PerformanceIndicatorGetDto PerformanceIndicator { get; set; }
    }
}
