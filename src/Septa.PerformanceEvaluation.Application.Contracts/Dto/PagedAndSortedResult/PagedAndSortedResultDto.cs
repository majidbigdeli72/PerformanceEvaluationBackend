﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Localization.Resources.AbpDdd;

namespace Septa.PerformanceEvaluation.Dto
{
    public class PagedAndSortedResultDto : PagedAndSortedResultRequestDto
    {
        public new static int DefaultMaxResultCount { get; set; } = 10;
        public new static int MaxMaxResultCount { get; set; } = int.MaxValue;

        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (MaxResultCount > MaxMaxResultCount)
            {
                var localizer = validationContext.GetRequiredService<IStringLocalizer<AbpDddApplicationContractsResource>>();

                yield return new ValidationResult(
                    localizer[
                        "MaxResultCountExceededExceptionMessage",
                        nameof(MaxResultCount),
                        MaxMaxResultCount,
                        typeof(LimitedResultRequestDto).FullName,
                        nameof(MaxMaxResultCount)
                    ],
                    new[] { nameof(MaxResultCount) });
            }
        }


    }
}
