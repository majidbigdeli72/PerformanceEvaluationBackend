﻿using System.ComponentModel.DataAnnotations;
using Volo.Abp.Auditing;

namespace Septa.PerformanceEvaluation.Dto.Account
{
    public class UserLoginDto
    {
        [Required]
        [StringLength(255)]
        public string UserName { get; set; }

        [Required]
        [StringLength(32)]
        [DataType(DataType.Password)]
        [DisableAuditing]
        public string Password { get; set; }
    }
}
