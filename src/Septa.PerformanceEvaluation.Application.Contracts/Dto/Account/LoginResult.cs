﻿using Septa.PerformanceEvaluation.Enums;

namespace Septa.PerformanceEvaluation.Dto.Account
{
    public class LoginResult
    {
        public LoginResult(Gp_LoginResultType result)
        {
            Result = result;
        }

        public Gp_LoginResultType Result { get; }

        public string Description => Result.ToString();
    }
}
