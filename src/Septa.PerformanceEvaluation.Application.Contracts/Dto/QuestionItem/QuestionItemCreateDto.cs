﻿namespace Septa.PerformanceEvaluation.Dto
{
    public class QuestionItemCreateDto
    {
        public string Name { get; set; }
        public decimal Value { get; set; }
    }
}
