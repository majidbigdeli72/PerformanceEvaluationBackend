﻿namespace Septa.PerformanceEvaluation.Dto
{
    public class QuestionItemUpdateDto
    {
        public int Id { get; set; }
        public int QuestionId { get; set; }
        public string Name { get; set; }
        public decimal Value { get; set; }
    }
}
