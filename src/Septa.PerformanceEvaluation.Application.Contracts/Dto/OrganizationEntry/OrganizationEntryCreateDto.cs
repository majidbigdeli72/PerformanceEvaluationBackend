﻿using System;

namespace Septa.PerformanceEvaluation.Dto
{
    public class OrganizationEntryCreateDto
    {
        public Guid? ParentId { get; set; }
        public int OrganizatuinEntryTypeIndex { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
