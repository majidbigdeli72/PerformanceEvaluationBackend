﻿using System;
using Volo.Abp.Application.Dtos;

namespace Septa.PerformanceEvaluation.Dto
{
    public class OrganizationEntryGetDto : EntityDto<Guid>
    {
        public Guid? ParentId { get; set; }
        public int OrganizatuinEntryTypeIndex { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
