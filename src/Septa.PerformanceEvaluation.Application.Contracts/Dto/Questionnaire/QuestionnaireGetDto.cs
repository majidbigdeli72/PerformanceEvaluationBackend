﻿using System.Collections.Generic;
using Volo.Abp.Application.Dtos;

namespace Septa.PerformanceEvaluation.Dto
{
    public class QuestionnaireGetDto: EntityDto<int>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public List<QuestionGetDto> Questions { get; set; }
    }
}
