﻿using System.Collections.Generic;

namespace Septa.PerformanceEvaluation.Dto
{
    public class QuestionnaireUpdateDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public List<QuestionUpdateDto> Questions { get; set; }

    }
}
