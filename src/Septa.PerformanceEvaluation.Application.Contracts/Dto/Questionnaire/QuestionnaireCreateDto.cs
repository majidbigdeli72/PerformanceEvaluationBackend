﻿using System.Collections.Generic;

namespace Septa.PerformanceEvaluation.Dto
{
    public class QuestionnaireCreateDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public List<QuestionCreateDto> Questions { get; set; }

    }
}
