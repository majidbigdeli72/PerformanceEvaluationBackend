﻿namespace Septa.PerformanceEvaluation.Permissions
{
    public static class PerformanceEvaluationPermissions
    {
        public const string GroupName = "PerformanceEvaluation";

        //Add your own permission names. Example:
        //public const string MyPermission1 = GroupName + ".MyPermission1";
    }
}