﻿using Septa.PerformanceEvaluation.Localization;
using Volo.Abp.Authorization.Permissions;
using Volo.Abp.Localization;

namespace Septa.PerformanceEvaluation.Permissions
{
    public class PerformanceEvaluationPermissionDefinitionProvider : PermissionDefinitionProvider
    {
        public override void Define(IPermissionDefinitionContext context)
        {
            var myGroup = context.AddGroup(PerformanceEvaluationPermissions.GroupName);

            //Define your own permissions here. Example:
            //myGroup.AddPermission(PerformanceEvaluationPermissions.MyPermission1, L("Permission:MyPermission1"));
        }

        private static LocalizableString L(string name)
        {
            return LocalizableString.Create<PerformanceEvaluationResource>(name);
        }
    }
}
