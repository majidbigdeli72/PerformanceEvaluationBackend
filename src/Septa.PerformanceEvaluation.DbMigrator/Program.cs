﻿using System.IO;
using Microsoft.Extensions.DependencyInjection;
using Septa.PerformanceEvaluation.Data;
using Serilog;
using Serilog.Events;
using Volo.Abp;
using Volo.Abp.Data;
using Volo.Abp.Identity;
using Volo.Abp.IdentityServer;
using Volo.Abp.Threading;

namespace Septa.PerformanceEvaluation.DbMigrator
{
    class Program
    {
        static void Main(string[] args)
        {

            AbpCommonDbProperties.DbTablePrefix = PerformanceEvaluationConsts.DbTablePrefix;
           // AbpIdentityDbProperties.DbTablePrefix = "Septa";
            AbpIdentityServerDbProperties.DbTablePrefix = PerformanceEvaluationConsts.DbTablePrefix;

            ConfigureLogging();

            using (var application = AbpApplicationFactory.Create<PerformanceEvaluationDbMigratorModule>(options =>
            {
                options.UseAutofac();
                options.Services.AddLogging(c => c.AddSerilog());
            }))
            {
                application.Initialize();

                AsyncHelper.RunSync(
                    () => application
                        .ServiceProvider
                        .GetRequiredService<PerformanceEvaluationDbMigrationService>()
                        .MigrateAsync()
                );

                application.Shutdown();
            }
        }

        private static void ConfigureLogging()
        {
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Information()
                .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
                .MinimumLevel.Override("Volo.Abp", LogEventLevel.Warning)
#if DEBUG
                .MinimumLevel.Override("Septa.PerformanceEvaluation", LogEventLevel.Debug)
#else
                .MinimumLevel.Override("Septa.PerformanceEvaluation", LogEventLevel.Information)
#endif
                .Enrich.FromLogContext()
                .WriteTo.File(Path.Combine(Directory.GetCurrentDirectory(), "Logs/logs.txt"))
                .WriteTo.Console()
                .CreateLogger();
        }
    }
}
