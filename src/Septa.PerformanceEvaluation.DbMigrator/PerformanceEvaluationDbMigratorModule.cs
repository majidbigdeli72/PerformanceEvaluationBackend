﻿using Septa.PerformanceEvaluation.EntityFrameworkCore;
using Volo.Abp.Autofac;
using Volo.Abp.BackgroundJobs;
using Volo.Abp.Modularity;

namespace Septa.PerformanceEvaluation.DbMigrator
{
    [DependsOn(
        typeof(AbpAutofacModule),
        typeof(PerformanceEvaluationEntityFrameworkCoreDbMigrationsModule),
        typeof(PerformanceEvaluationApplicationContractsModule)
        )]
    public class PerformanceEvaluationDbMigratorModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            Configure<AbpBackgroundJobOptions>(options => options.IsJobExecutionEnabled = false);
        }
    }
}
